from rest_framework import serializers

from core.users.models import User
from v2.ngo.models import NGOFocusArea, NGOLocation, NGOPartner, NGOPointOfContact, NGOSubFocusArea, \
    NGOSubTargetSegment, NGOTargetSegment
from v2.project.models import Project


"""
    This class will serialize NGOPartner model.
"""
class NGOPartnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = NGOPartner
        fields = '__all__'


"""
    This class will serialize NGOPartner model and return fields id, ngo_name.
"""
class MiniNGOPartnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = NGOPartner
        fields = fields = ['id', 'ngo_name']


"""
    This class will serialize NGOPartner model for read operation.
"""
class NGOPartnerReadSerializer(serializers.ModelSerializer):
    location = serializers.SerializerMethodField()
    focus_area = serializers.SerializerMethodField()
    sub_focus_area = serializers.SerializerMethodField()
    target_segment = serializers.SerializerMethodField()
    sub_target_segment = serializers.SerializerMethodField()
    point_of_contact = serializers.SerializerMethodField()
    projects = serializers.SerializerMethodField()
    users = serializers.SerializerMethodField()
    clients = serializers.SerializerMethodField()
    cities = serializers.SerializerMethodField()

    @staticmethod
    def get_location(obj):
        locations = NGOLocation.objects.filter(ngo=obj).values_list('location', flat=True)
        return locations

    @staticmethod
    def get_focus_area(obj):
        focus_area = NGOFocusArea.objects.filter(ngo=obj).values_list('focus_area', flat=True)
        return focus_area

    @staticmethod
    def get_sub_focus_area(obj):
        sub_focus_area = NGOSubFocusArea.objects.filter(ngo=obj).values_list('sub_focus_area', flat=True)
        return sub_focus_area

    @staticmethod
    def get_target_segment(obj):
        target_segment = NGOTargetSegment.objects.filter(ngo=obj).values_list('target_segment', flat=True)
        return target_segment

    @staticmethod
    def get_sub_target_segment(obj):
        sub_target_segment = NGOSubTargetSegment.objects.filter(ngo=obj).values_list('sub_target_segment',
                                                                                     flat=True)
        return sub_target_segment

    @staticmethod
    def get_point_of_contact(obj):
        point_of_contacts = NGOPointOfContact.objects.filter(ngo=obj)
        serializer = NGOPointOfContactSerializer(point_of_contacts, many=True)
        return serializer.data

    @staticmethod
    def get_projects(obj):
        from v2.project.serializers import ProjectMiniSerializer
        projects = Project.objects.filter(ngo_partner=obj)
        serializer = ProjectMiniSerializer(projects, many=True)
        return serializer.data

    @staticmethod
    def get_users(obj):
        from v2.user_management.serializers import UserMicroSerializer
        users = User.objects.filter(ngo=obj)
        serializer = UserMicroSerializer(users, many=True)
        return serializer.data

    @staticmethod
    def get_clients(obj):
        clients = obj.clients
        return clients.values_list('display_name', flat=True)

    @staticmethod
    def get_cities(obj):
        cities = NGOLocation.objects.filter(ngo=obj).values_list('city', flat=True)
        return cities

    class Meta:
        model = NGOPartner
        fields = '__all__'


"""
    This class will serialize NGOPointOfContact model.
"""
class NGOPointOfContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = NGOPointOfContact
        fields = '__all__'
