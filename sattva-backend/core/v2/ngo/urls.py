from django.conf.urls import include, url

from v2.ngo.views import NGOPartnerModelViewSet

app_name = "ngo"

urlpatterns = [
    url(r'^$', NGOPartnerModelViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='ngo_partner'),
    url(r'^(?P<pk>\d+)/$', NGOPartnerModelViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='ngo_partner'),
]
