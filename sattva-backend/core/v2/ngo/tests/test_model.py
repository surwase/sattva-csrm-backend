from django.test import TestCase
from .test_setup import TestSetUp
from v2.ngo.models import NGOPartner
from django.core.files.uploadedfile import SimpleUploadedFile

class TestNGOPartner(TestSetUp):

    def setUp(self):
        super().setUp()
        NGOPartner.objects.create(
            ngo_name = "ngo1",
            company_type = "type1",
            website_url = "www.ngo.com",
            about = "hi i am ngo",
            address = "lucknow",
            employee_count = 100,
            annual_budget = 100000,
            experience = 10,
            awards = "award1",
            state = "up",
            logo = SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            key_funders = ['sonika'],
            key_work = ['owner'],

            )

    def test_validation(self):
        with self.assertRaises(ValueError):
            NGOPartner.objects.create(
            ngo_name = 123,
            company_type = "type1",
            website_url = "www.ngo.com",
            about = "hi i am ngo",
            address = "lucknow",
            employee_count = 100,
            annual_budget = "hi",
            experience = 10,
            awards = "award1",
            state = "up",
            logo = SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            key_funders = ['sonika'],
            key_work = ['owner'],

            )

    def test_values(self):
        ngo1=NGOPartner.objects.get( ngo_name = "ngo1")
        self.assertEqual(ngo1.ngo_name , "ngo1")
        self.assertEqual(ngo1.company_type , "type1")
        self.assertEqual(ngo1.website_url , "www.ngo.com")
        self.assertEqual(ngo1.about , "hi i am ngo")
        self.assertEqual(ngo1.address , "lucknow")
        self.assertEqual(ngo1.employee_count , 100)
        self.assertEqual(ngo1.annual_budget , 100000)
        self.assertEqual(ngo1.experience , 10)
        self.assertEqual(ngo1.awards , "award1")
        self.assertEqual(ngo1.state , "up")
        self.assertEqual(ngo1.key_funders , ['sonika'])


    def test__str__(self):
        ngo1=NGOPartner.objects.get( ngo_name = "ngo1")
        self.assertEqual(ngo1.__str__() , "ngo1")
