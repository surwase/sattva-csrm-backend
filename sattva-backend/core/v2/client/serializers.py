from rest_framework import serializers
from rest_framework_bulk import BulkSerializerMixin

from collections import OrderedDict
from operator import itemgetter
from v2.project.models import ProjectTag
from core.users.models import User
from core.utils import check_null_and_replace_to_none
from v2.client.models import Client, ClientComplianceCheckList, ClientComplianceEvaluation, ClientDetail, \
    ClientOverview, ClientUser, ClientImage, ClientDocument, ClientDashboard, ClientComplianceSchedule, CustomFolder, \
    ClientSettings,ClientDatabaseDetail
from v2.plant.models import Plant, PlantUser
from v2.plant.serializers import PlantSerializer, PlantOptionSerializer, PlantProjectStatusSerializer, \
    PlantProjectAggregationSerializer, PlantListSerializer
from v2.project.models import Project, ProjectUser
from v2.project.serializers import ProjectSerializer, ProjectOptionSerializer, ProjectDetailMiniSerializer, \
    ProjectListSerializer, ProjectMiniMicroSerializer
from v2.user_management.serializers import UserSerializer, UserSimpleSerializer, UserMicroSerializer

"""
    This class will serialize client model.
"""
class ClientMicroSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'client_name', 'display_name']


"""
    This class will serialize client model for read operation.
"""
class ClientReadSerializer(serializers.ModelSerializer):
    project = serializers.SerializerMethodField('get_client_projects')
    plant = serializers.SerializerMethodField('get_client_plants')
    client_users = serializers.SerializerMethodField()

    # TODO: Check if we could just send the number of projects and clients in some of the APIs.

    # fetch client projects
    def get_client_projects(self, client):
        projects = Project.objects.filter(client=client)
        serializer = ProjectSerializer(projects, many=True)
        return serializer.data

    @staticmethod
    # fetch client plants
    def get_client_plants(client):
        plants = Plant.objects.filter(client=client)
        serializer = PlantSerializer(plants, many=True)
        return serializer.data

    @staticmethod
    # fetch client users
    def get_client_users(client):
        users = User.objects.filter(id__in=ClientUser.objects.filter(client=client).values_list('user', flat=True))
        serializer = UserSimpleSerializer(users, many=True)
        return serializer.data

    class Meta:
        model = Client
        fields = '__all__'


"""
   Serializer for ClientList used for read operation
"""
class ClientListReadSerializer(serializers.ModelSerializer):
    project = serializers.SerializerMethodField('get_client_projects')
    plant = serializers.SerializerMethodField('get_client_plants')
    client_users = serializers.SerializerMethodField()

    # fetch client projects
    def get_client_projects(self, client):
        user = self.context['request'].user
        if user.application_role.role_name == 'sattva-admin':
            projects = Project.objects.filter(client=client)
        else:
            user_projects = ProjectUser.objects.filter(user=user).values_list('project', flat=True)
            projects = Project.objects.filter(client=client, id__in=user_projects)
        serializer = ProjectListSerializer(projects, many=True)
        return serializer.data

    # fetch client plants
    def get_client_plants(self, client):
        user = self.context['request'].user
        if user.application_role.role_name == 'sattva-admin':
            plants = Plant.objects.filter(client=client)
        else:
            user_plants = PlantUser.objects.filter(user=user).values_list('plant', flat=True)
            plants = Plant.objects.filter(client=client, id__in=user_plants)
        serializer = PlantListSerializer(plants, many=True)
        return serializer.data

    @staticmethod
    def get_client_users(client):
        users = User.objects.filter(id__in=ClientUser.objects.filter(client=client).values_list('user', flat=True))
        serializer = UserSimpleSerializer(users, many=True)
        return serializer.data

    class Meta:
        model = Client
        fields = '__all__'


"""
   Serializer for Client mini List used for read operation
"""
class ClientMiniListReadSerializer(serializers.ModelSerializer):
    project = serializers.SerializerMethodField('get_client_projects')
    plant = serializers.SerializerMethodField('get_client_plants')
    client_users = serializers.SerializerMethodField()

    # fetch client projects
    def get_client_projects(self, client):
        user = self.context['request'].user
        if user.application_role.role_name == 'sattva-admin':
            projects = Project.objects.filter(client=client)
        else:
            user_projects = ProjectUser.objects.filter(user=user).values_list('project', flat=True)
            projects = Project.objects.filter(client=client, id__in=user_projects)
        return projects.count()

    # fetch client plants
    def get_client_plants(self, client):
        user = self.context['request'].user
        if user.application_role.role_name == 'sattva-admin':
            plants = Plant.objects.filter(client=client)
        else:
            user_plants = PlantUser.objects.filter(user=user).values_list('plant', flat=True)
            plants = Plant.objects.filter(client=client, id__in=user_plants)
        return plants.count()

    @staticmethod
    # fetch client users
    def get_client_users(client):
        users = User.objects.filter(id__in=ClientUser.objects.filter(client=client).values_list('user', flat=True))
        serializer = UserSimpleSerializer(users, many=True)
        return serializer.data

    class Meta:
        model = Client
        fields = ['id', 'display_name', 'project', 'plant', 'client_users', 'website_url', 'logo', 'is_kobo_enabled']


"""
    This class will serialize client model for read operations and return fields id and display_name  .
"""
class ClientMicroListReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'display_name']


"""
    This class will serialize Client Image model and return fields id, image_file, image_size, path, client.
"""
class ClientImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientImage
        fields = ['id', 'image_file', 'image_size', 'path', 'client']


"""
    This class will serialize Client model for write operations.
"""
class ClientWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'

#apply the filter for project
def apply_client_details_api_filter(self, obj):
    projects = obj.get_all_projects()
    start_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('start_date'))
    end_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('end_date'))
    location = check_null_and_replace_to_none(self.context.get('request').query_params.get('location'))
    focus_area = check_null_and_replace_to_none(self.context.get('request').query_params.get('focus_area'))
    sub_focus_area = check_null_and_replace_to_none(self.context.get('request').query_params.get('sub_focus_area'))
    tags = check_null_and_replace_to_none(self.context.get('request').query_params.get('project_tag'))
    # if not ((start_date is None) | (end_date is None)):
    #     projects = obj.get_all_projects_list_by_date_range(start_date=start_date, end_date=end_date)
    if start_date and end_date is None:
        projects = projects.filter(end_date__gte=start_date)
    elif end_date and start_date is None:
        projects = projects.filter(start_date__lte=end_date)
    elif start_date and end_date:
        projects = projects.filter(start_date__lte=end_date, end_date__gte=start_date)
    if not location is None:
        projects = projects.filter(location == location)
    if not focus_area is None:
        projects = projects.filter(focus_area == focus_area)
    if not sub_focus_area is None:
        projects = projects.filter(sub_focus_area__in=[sub_focus_area])
    if not tags is None:
        tag_name = ProjectTag.objects.get(id=tags)
        projects = projects.filter(projecttag__tag=tag_name.tag)
    return projects

#apply the filter for plant
def apply_client_details_api_filter_for_plant(self, obj):
    projects = obj.get_all_projects()
    projects = projects.filter(plant__isnull=False)
    start_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('start_date'))
    end_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('end_date'))
    tags = check_null_and_replace_to_none(self.context.get('request').query_params.get('project_tag'))
    # if not ((start_date is None) | (end_date is None)):
    #     plants = obj.get_all_plants_list_by_date_range(start_date=start_date, end_date=end_date)
    if start_date and end_date is None:
        projects = projects.filter(end_date__gte=start_date)
    elif end_date and start_date is None:
        projects = projects.filter(start_date__lte=end_date)
    elif start_date and end_date:
        projects = projects.filter(start_date__lte=end_date, end_date__gte=start_date)
    if not tags is None:
        tag_name = ProjectTag.objects.get(id=tags)
        projects = projects.filter(projecttag__tag=tag_name.tag)
    plants = projects.values_list('plant', flat=True)
    plants = Plant.objects.filter(id__in=plants)
    return plants


"""
    This class will serialize Client details for read operation.
"""
class ClientDetailReadSerializer(serializers.ModelSerializer):
    # images = serializers.SerializerMethodField()
    project_locations = serializers.SerializerMethodField()
    focus_area = serializers.SerializerMethodField()
    sub_focus_area = serializers.SerializerMethodField()
    target_segment = serializers.SerializerMethodField()
    sub_target_segment = serializers.SerializerMethodField()
    budget = serializers.SerializerMethodField()
    disbursement_budget = serializers.SerializerMethodField()
    over_utilization = serializers.SerializerMethodField()
    total_projects = serializers.SerializerMethodField()
    live_projects = serializers.SerializerMethodField()
    project_status = serializers.SerializerMethodField()
    financial_utilization = serializers.SerializerMethodField()
    last_compliance_date = serializers.SerializerMethodField()
    sdg_goals = serializers.SerializerMethodField()
    total_disbursement = serializers.SerializerMethodField()
    planned_disbursement = serializers.SerializerMethodField()

    # fetch images
    def get_images(self, obj):
        data = ClientImageSerializer(ClientImage.objects.filter(client=obj), many=True).data
        return data

    # fetch project locations
    def get_project_locations(self, obj):
        return obj.get_all_project_locations()

    # fetch project focus_area
    def get_focus_area(self, obj):
        return obj.get_project_focus_areas_list()

    # fetch project sub focus_area
    def get_sub_focus_area(self, obj):
        return obj.get_project_sub_focus_areas_list()

    # fetch project target_segment
    def get_target_segment(self, obj):
        return obj.get_project_target_segment_list()

    # fetch project sub target_segment
    def get_sub_target_segment(self, obj):
        return obj.get_project_sub_target_segment_list()

    # fetch budget
    def get_budget(self, obj):
        projects = apply_client_details_api_filter(self, obj)
        return obj.get_total_budget(projects=projects)

    # fetch disbursement budget
    def get_disbursement_budget(self, obj):
        projects = apply_client_details_api_filter(self, obj)
        return obj.get_total_disbursement_amount(projects=projects)

    # fetch over_utilization
    def get_over_utilization(self, obj):
        projects = apply_client_details_api_filter(self, obj)
        return obj.get_project_over_utilization(projects=projects)

    # fetch last compliance_date
    def get_last_compliance_date(self, obj):
        return obj.get_last_compliance_date()

    # fetch project status
    def get_project_status(self, obj):
        projects = apply_client_details_api_filter(self, obj)
        return str(round(obj.get_all_projects_average_status(projects=projects), 2)) + "%"

    # fetch total projects
    def get_total_projects(self, obj):
        projects = apply_client_details_api_filter(self, obj)
        return projects.count()

    # fetch live projects
    def get_live_projects(self, obj):
        projects = apply_client_details_api_filter(self, obj)
        return obj.get_all_live_projects().filter(id__in=projects.values_list('id', flat=True)).count()

    # fetch financial utilization
    def get_financial_utilization(self, obj):
        projects = apply_client_details_api_filter(self, obj)
        total_utilized = obj.get_total_utilized_amount(projects=projects)
        unutilized_amount = obj.get_total_disbursement_amount(projects=projects) - total_utilized
        return [{"Utilised": total_utilized,
                 "Not Utilised": 0 if unutilized_amount < 0 else unutilized_amount}]

    # fetch total disbursement
    def get_total_disbursement(self, obj):
        projects = apply_client_details_api_filter(self, obj)
        return obj.get_total_disbursement_amount(projects=projects)

    # fetch planned disbursement
    def get_planned_disbursement(self, obj):
        projects = apply_client_details_api_filter(self, obj)
        return obj.get_planned_disbursement_amount(projects=projects)

    # fetch sdg goals
    def get_sdg_goals(self, obj):
        start_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('start_date'))
        end_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('end_date'))
        return obj.get_projects_sdg_goals()

    class Meta:
        model = Client
        fields = ['id', 'client_name', 'display_name', 'logo', 'website_url',
                  'project_locations', "budget", "disbursement_budget", "focus_area", "sub_focus_area",
                  "target_segment", "sub_target_segment", "over_utilization", "total_projects",
                  "live_projects", "project_status", "financial_utilization", "last_compliance_date",
                  "sdg_goals", "total_disbursement", "planned_disbursement"]

"""
    This class will serialize Client Risk Status.
"""
class ClientRiskStatusSerializer(serializers.ModelSerializer):
    risk_status = serializers.SerializerMethodField()

    # fetch risk_status
    def get_risk_status(self, obj):
        start_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('start_date'))
        end_date = check_null_and_replace_to_none(self.context.get('request').query_params.get('end_date'))
        projects = apply_client_details_api_filter(self, obj)
        location = check_null_and_replace_to_none(self.context.get('request').query_params.get('location'))
        focus_area = check_null_and_replace_to_none(self.context.get('request').query_params.get('focus_area'))
        sub_focus_area = check_null_and_replace_to_none(self.context.get('request').query_params.get('sub_focus_area'))
        if not ((start_date is None) | (end_date is None)):
            return obj.get_count_of_risk_status(start_date=start_date, end_date=end_date, projects=projects)
        elif (location is not None) | (focus_area is not None) | (sub_focus_area is not None):
            return obj.get_count_of_risk_status(projects=projects)
        else:
            return obj.get_count_of_risk_status()

    class Meta:
        model = Client
        fields = ['id', 'client_name', 'risk_status']


"""
    This class will serialize Client Project Status.
"""
class ClientProjectStatusSerializer(serializers.ModelSerializer):
    project_status_chart = serializers.SerializerMethodField()

    # fetch project_status_chart
    def get_project_status_chart(self, obj):
        projects = apply_client_details_api_filter(self, obj)
        total_projects = projects.count()
        projects_list = projects.values_list('id', flat=True)
        if total_projects > 0:
            overall_status = []
            completed_projects = len([project for project in projects if project.project_status >= 100])
            on_going_projects = len([project for project in projects if project.project_status > 0 and project.project_status < 100])
            open_projects = len([project for project in projects if project.project_status <= 0])
            if completed_projects > 0:
                overall_status.append({"status": "Completed", "count": completed_projects,
                                       "percentage": round((completed_projects / total_projects) * 100, 2)})
            if open_projects > 0:
                overall_status.append({"status": "Not Started", "count": open_projects,
                                       "percentage": round((open_projects / total_projects) * 100, 2)})
            if on_going_projects > 0:
                overall_status.append({"status": "On Going", "count": on_going_projects,
                                       "percentage": round((on_going_projects / total_projects) * 100, 2)})
            return overall_status
        else:
            return []

    class Meta:
        model = Client
        fields = ['id', 'client_name', "project_status_chart"]

"""
    This class will serialize Client project Status.
"""
class ClientProjectStatusSerializer(serializers.ModelSerializer):
    project_status_chart = serializers.SerializerMethodField()

    # fetch project_status_chart
    def get_project_status_chart(self, obj):
        projects = apply_client_details_api_filter(self, obj)
        total_projects = projects.count()
        projects_list = projects.values_list('id', flat=True)
        if total_projects > 0:
            overall_status = []
            completed_projects = obj.get_all_completed_projects().filter(id__in=projects_list).count()
            open_projects = obj.get_all_open_projects().filter(id__in=projects_list).count()
            on_going_projects = obj.get_all_on_going_projects().filter(id__in=projects_list).count()
            if completed_projects > 0:
                overall_status.append({"status": "Completed", "count": completed_projects,
                                       "percentage": round((completed_projects / total_projects) * 100, 2)})
            if open_projects > 0:
                overall_status.append({"status": "Not Started", "count": open_projects,
                                       "percentage": round((open_projects / total_projects) * 100, 2)})
            if on_going_projects > 0:
                overall_status.append({"status": "On Going", "count": on_going_projects,
                                       "percentage": round((on_going_projects / total_projects) * 100, 2)})
            return overall_status
        else:
            return []

    class Meta:
        model = Client
        fields = ['id', 'client_name', "project_status_chart"]

"""
    This class will serialize Client model project list.
"""
class ClientProjectListSerializer(serializers.ModelSerializer):
    projects = serializers.SerializerMethodField()

    def get_projects(self, obj):
        projects = Project.objects.filter(client=obj)
        return ProjectDetailMiniSerializer(projects, many=True).data

    class Meta:
        model = Client
        fields = ['id', 'client_name', 'display_name', 'projects']


"""
    This class will serialize Client Plant List.
"""
class ClientPlantListSerializer(serializers.ModelSerializer):
    plants = serializers.SerializerMethodField()

    def get_plants(self, obj):
        return PlantProjectStatusSerializer(Plant.objects.filter(client=obj), many=True).data

    class Meta:
        model = Client
        fields = ['id', 'client_name', 'display_name', 'plants']


"""
    This class will serialize Client Plant ,project List.
"""
class ClientPlantProjectListSerializer(serializers.ModelSerializer):
    plants = serializers.SerializerMethodField()
    standalone_projects = serializers.SerializerMethodField()

    # fetch plants
    def get_plants(self, obj):
        plants = apply_client_details_api_filter_for_plant(self, obj)
        serializer = PlantProjectAggregationSerializer(plants, many=True)
        serializer._context = self.context
        return serializer.data

    # fetch standalone projects
    def get_standalone_projects(self, obj):
        projects = apply_client_details_api_filter(self, obj)
        projects = projects.filter(plant__isnull=True)
        serializer = ProjectDetailMiniSerializer(projects, many=True)
        serializer._context = self.context
        return serializer.data

    class Meta:
        model = Client
        fields = ['id', 'client_name', 'display_name', 'plants', 'standalone_projects']


"""
    This class will serialize Client User model.
"""
class ClientUserReadSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = ClientUser
        fields = '__all__'


"""
    This class will serialize Client User model.
"""
class MiniClientUserReadSerializer(serializers.ModelSerializer):
    user = UserSimpleSerializer(read_only=True)

    class Meta:
        model = ClientUser
        fields = '__all__'


"""
    This class will serialize Client User model and return feilds id and user.
"""
class ClientUserMiniSerializer(serializers.ModelSerializer):
    user = UserMicroSerializer(read_only=True)

    class Meta:
        model = ClientUser
        fields = ['id', 'user']


"""
    This class will serialize Client User model for write operation.
"""
class ClientUserWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientUser
        fields = '__all__'


"""
    This class will serialize Client Overview model.
"""
class ClientOverviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientOverview
        fields = '__all__'


"""
    This class will serialize Client Details model.
"""
class ClientDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientDetail
        fields = '__all__'


"""
    This class will serialize Client Compliance CheckList model.
"""
class ClientComplianceCheckListSerializer(BulkSerializerMixin, serializers.ModelSerializer):
    class Meta:
        model = ClientComplianceCheckList
        fields = '__all__'


"""
    This class will serialize Client Compliance Evaluation model.
"""
class ClientComplianceEvaluationSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientComplianceEvaluation
        fields = '__all__'


"""
    This class will serialize Client Compliance CheckList for read operation.
"""
class ComplianceCheckListReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientComplianceCheckList
        fields = ['checklist_id', 'area', 'client', 'indicator']


"""
    This class will serialize Client Compliance Evaluation model for read operation.
"""
class ClientComplianceEvaluationReadSerializer(serializers.ModelSerializer):
    client = serializers.SerializerMethodField()
    compliance_checklist = serializers.SerializerMethodField()

    def get_client(self, obj):
        return obj.client.id

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        ret = OrderedDict(filter(lambda x: x[1] is not None, ret.items()))
        return ret

    def get_compliance_checklist(self, obj):
        compliance_checklist = obj.compliance_checklist
        serializer = ComplianceCheckListReadSerializer(compliance_checklist)
        return serializer.data

    class Meta:
        model = ClientComplianceEvaluation
        fields = ['batch_id', 'id', 'client', 'compliance_checklist', 'date_captured', 'value', 'support_document', 'created_by']
        depth = 1


"""
    This class will serialize Client model for read operation and return fields id, display_name, logo, plant, standalone_projects.
"""
class ClientOptionReadSerializer(serializers.ModelSerializer):
    plant = serializers.SerializerMethodField('get_client_plants')
    standalone_projects = serializers.SerializerMethodField()

    @staticmethod
    def get_client_plants(client):
        plants = Plant.objects.filter(client=client)
        serializer = PlantOptionSerializer(plants, many=True)
        return serializer.data

    @staticmethod
    def get_standalone_projects(client):
        projects = Project.objects.filter(client=client, plant=None)
        serializer = ProjectMiniMicroSerializer(projects, many=True)
        return serializer.data

    class Meta:
        model = Client
        fields = ('id', 'display_name', 'logo', 'plant', 'standalone_projects')


"""
    This class will serialize Client Document model.
"""
class ClientFilesCommonDisbursementSerializer(serializers.ModelSerializer):
    file_name = serializers.SerializerMethodField()
    file_path = serializers.SerializerMethodField()
    file_type = serializers.SerializerMethodField()

    def get_file_name(self, obj):
        return obj.document_file.url.split("/")[-1] if obj.document_file else ''

    def get_file_path(self, obj):
        return obj.document_file.url if obj.document_file else ''

    def get_file_type(self, obj):
        file_parts = obj.document_file.name.split(".") if obj.document_file else []
        if len(file_parts) > 1:
            return file_parts[1].lower()
        return ""

    class Meta:
        model = ClientDocument
        fields = ['id', 'file_name', 'file_path', 'file_type', 'created_date']


"""
    This class will serialize Client Image model.
"""
class ClientImagesCommonDisbursementSerializer(serializers.ModelSerializer):
    file_name = serializers.SerializerMethodField()
    file_path = serializers.SerializerMethodField()
    file_type = serializers.SerializerMethodField()

    def get_file_name(self, obj):
        return obj.image_file.url.split("/")[-1] if obj.image_file else ''

    def get_file_path(self, obj):
        return obj.image_file.url if obj.image_file else ''

    def get_file_type(self, obj):
        file_parts = obj.image_file.name.split(".") if obj.image_file else []
        if len(file_parts) > 1:
            return file_parts[1].lower()
        return ""

    class Meta:
        model = ClientImage
        fields = ['id', 'file_name', 'file_path', 'file_type', 'created_date']


"""
    This class will serialize Client Dashboard model.
"""
class ClientDashboardSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientDashboard
        fields = '__all__'


"""
    This class will serialize Custom Folder model.
"""
class CustomFolderSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomFolder
        fields = "__all__"


"""
    This class will serialize Client Dashboard and return fields url, embedded_content, password, dashboard_name, id.
"""
class ClientDashboardMiniSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientDashboard
        fields = ['url', 'embedded_content', 'password', 'dashboard_name', 'id']


"""
    This class will serialize ClientComplianceSchedule model.
"""
class ClientComplianceScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientComplianceSchedule
        fields = '__all__'


"""
    This class will serialize Client Settings model.
"""
class ClientSettingsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientSettings
        fields = '__all__'


"""
    This class will serialize Client Database Detail model.
"""
class ClientDatabaseDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientDatabaseDetail
        fields = ['id', 'username', 'password', 'schema_name', 'client']
