from os import name
from v2.project.models import ProjectLocation, ProjectFocusArea, ProjectSubFocusArea, ProjectTargetSegment ,ProjectSubTargetSegment, ProjectSDGGoals
import datetime
from django.core.files.uploadedfile import SimpleUploadedFile
from core.users.models import User
from django.test import TestCase, client
from unittest.mock import patch
from .test_setup import TestSetUp

from v2.client.models import Client, ClientComplianceCheckList, ClientComplianceEvaluation, \
    ClientComplianceSchedule, ClientDashboard, ClientDetail, ClientDocument, ClientImage, ClientOverview, ClientSettings, ClientUser, CustomFolder, UserNotificationSettings, \
        ClientComplianceEvaluation


class ClientTestCase(TestSetUp):


    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self, mock_send,mock_send2):
        super().setUp()
        Client.objects.create(
            client_name='client 123',
            display_name='client 123',
            website_url='www.company1.com',
            client_cin='FJOF033HFJEF',
            plant_field_name='Test_name',
            license_type='Standard',
            engagement_model='Standalone',
            is_kobo_enabled=True,
            compliance_frequency='Annually',
            csr_user=5,
            csr_admin=5,
            ngo_user=5,
            data_collector=5,
            csr_watcher=5
        )

    def test_validation(self):
        with self.assertRaises(ValueError):
            Client.objects.create(
            client_name=123,
            display_name=123,
            website_url='www.company1.com',
            client_cin='FJOF033HFJEF',
            plant_field_name='Test_name',
            license_type='Standard',
            engagement_model='Standalone',
            is_kobo_enabled=True,
            compliance_frequency='Annually',
            csr_user="hi",
            csr_admin="hi",
            ngo_user="hi",
            data_collector="hi",
            csr_watcher="hi"
        )

    def test_values(self):
        client1 = Client.objects.get(display_name="client 123")
        self.assertEqual(client1.client_name, 'client 123')
        self.assertEqual(client1.display_name, 'client 123')
        self.assertEqual(client1.website_url, 'www.company1.com')
        self.assertEqual(client1.client_cin, 'FJOF033HFJEF')
        self.assertEqual(client1.plant_field_name, 'Test_name')
        self.assertEqual(client1.license_type, 'Standard')
        self.assertEqual(client1.engagement_model, 'Standalone')
        self.assertEqual(client1.compliance_frequency, 'Annually')
        self.assertEqual(client1.csr_user, 5)
        self.assertEqual(client1.csr_admin, 5)
        self.assertEqual(client1.ngo_user, 5)
        self.assertEqual(client1.data_collector, 5)
        self.assertEqual(client1.csr_watcher, 5)

    def test_get_name(self):
        client1 = Client.objects.get(display_name="client 123")
        self.assertEqual(client1.get_name(), "client 123")

    def test_get_all_projects(self):
        from v2.project.models import Project
        client1 = Client.objects.get(display_name="client 123")
        Project.objects.create(project_name = "testing project", client=client1)
        self.assertEqual(client1.get_all_projects().count(), 1)

    def test_get_all_standalone_projects(self):
        from v2.project.models import Project
        client1 = Client.objects.get(display_name="client 123")
        Project.objects.create(project_name = "testing project 1", client=client1)
        Project.objects.create(project_name = "testing project 2", client=client1)
        self.assertEqual(client1.get_all_standalone_projects().count(), 2)

    def test_get_all_plants(self):
        from v2.project.models import Plant
        client1 = Client.objects.get(display_name="client 123")
        Plant.objects.create(plant_name="testing plant 1", client=client1)
        Plant.objects.create(plant_name="testing plant 2", client=client1)
        self.assertEqual(client1.get_all_plants().count(), 2)

    def test_get_all_live_projects(self):
        from v2.project.models import Project
        client1 = Client.objects.get(display_name="client 123")
        Project.objects.create(project_name = "testing project 1", client=client1, project_status=10)
        Project.objects.create(project_name = "testing project 2", client=client1, project_status=101)
        self.assertEqual(client1.get_all_live_projects().count(), 1)


    def test_get_all_open_projects(self):
        from v2.project.models import Project
        client1 = Client.objects.get(display_name="client 123")
        Project.objects.create(project_name = "testing project 1", client=client1, project_status=0)
        Project.objects.create(project_name = "testing project 2", client=client1, project_status=10)
        self.assertEqual(client1.get_all_open_projects().count(), 1)

    def test_get_all_ongoing_projects(self):
        from v2.project.models import Project
        client1 = Client.objects.get(display_name="client 123")
        Project.objects.create(project_name = "testing project 1", client=client1, project_status=0)
        Project.objects.create(project_name = "testing project 2", client=client1, project_status=10)
        self.assertEqual(client1.get_all_on_going_projects().count(), 1)

    def test_get_all_completed_projects(self):
        from v2.project.models import Project
        client1 = Client.objects.get(display_name="client 123")
        Project.objects.create(project_name = "testing project 1", client=client1, project_status=200)
        Project.objects.create(project_name = "testing project 2", client=client1, project_status=102)
        self.assertEqual(client1.get_all_completed_projects().count(), 2)

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def test_get_all_project_average_status(self, mock_send,mock_send2):
        from v2.project.models import Project
        client1 = Client.objects.get(display_name="client 123")
        Project.objects.create(project_name = "testing project 1", client=client1, project_status=200)
        Project.objects.create(project_name = "testing project 2", client=client1, project_status=100)
        self.assertEqual(client1.get_all_projects_average_status(), 150)

    def test_get_total_budget(self):
        from v2.project.models import Project
        client1 = Client.objects.get(display_name="client 123")
        Project.objects.create(project_name = "testing project 1", client=client1, project_status=200, budget=120)
        Project.objects.create(project_name = "testing project 2", client=client1, project_status=100, budget=120)
        self.assertEqual(client1.get_total_budget(), 240)

    def test_total_disbursement_amount(self):
        from v2.project.models import Project
        client1 = Client.objects.get(display_name="client 123")
        Project.objects.create(project_name = "testing project 1", client=client1, project_status=200, budget=120, total_disbursement_amount=600)
        Project.objects.create(project_name = "testing project 2", client=client1, project_status=100, budget=120, total_disbursement_amount=500)
        self.assertEqual(client1.get_total_disbursement_amount(), 1100.0)

    def test_get_planned_disbursement_amount(self):
        from v2.project.models import Project
        client1 = Client.objects.get(display_name="client 123")
        Project.objects.create(project_name = "testing project 1", client=client1, project_status=200, budget=120, planned_disbursement_amount=600)
        Project.objects.create(project_name = "testing project 2", client=client1, project_status=100, budget=120, planned_disbursement_amount=500)
        self.assertEqual(client1.get_planned_disbursement_amount(), 1100.0)

    def test_get_total_utilized_amount(self):
        from v2.project.models import Project
        client1 = Client.objects.get(display_name="client 123")
        Project.objects.create(project_name = "testing project 1", client=client1, project_status=200, budget=120, total_utilised_amount=600)
        Project.objects.create(project_name = "testing project 2", client=client1, project_status=100, budget=120, total_utilised_amount=500)
        self.assertEqual(client1.get_total_utilized_amount(), 1100.0)

    def test_get_all_project_locations(self):
        from v2.project.models import Project
        client1 = Client.objects.get(display_name="client 123")
        proj = Project.objects.create(project_name = "testing project 1", client=client1, project_status=200, budget=120, total_utilised_amount=600)
        ProjectLocation.objects.create(project=proj, area="ABC", city="DEF", state="GH", country="IND")
        self.assertEqual(client1.get_all_project_locations().count(), 1)

    def test_get_project_focus_areas_list(self):
        from v2.project.models import Project
        client1 = Client.objects.get(display_name="client 123")
        proj = Project.objects.create(project_name = "testing project 1", client=client1, project_status=200, budget=120, total_utilised_amount=600)
        ProjectFocusArea.objects.create(project=proj, focus_area="children")
        self.assertEqual(len(client1.get_project_focus_areas_list()), 1)

    def test_get_project_sub_focus_areas_list(self):
        from v2.project.models import Project
        client1 = Client.objects.get(display_name="client 123")
        proj = Project.objects.create(project_name = "testing project 1", client=client1, project_status=200, budget=120, total_utilised_amount=600)
        ProjectSubFocusArea.objects.create(project=proj, sub_focus_area="child Health")
        self.assertEqual(len(client1.get_project_sub_focus_areas_list()), 1)

    def test_get_project_target_segment_list(self):
        from v2.project.models import Project
        client1 = Client.objects.get(display_name="client 123")
        proj = Project.objects.create(project_name = "testing project 1", client=client1, project_status=200, budget=120, total_utilised_amount=600)
        ProjectTargetSegment.objects.create(project=proj, target_segment="child Health")
        self.assertEqual(len(client1.get_project_target_segment_list()), 1)

    def test_get_project_sub_target_segment_list(self):
        from v2.project.models import Project
        client1 = Client.objects.get(display_name="client 123")
        proj = Project.objects.create(project_name = "testing project 1", client=client1, project_status=200, budget=120, total_utilised_amount=600)
        ProjectSubTargetSegment.objects.create(project=proj, sub_target_segment="child Health")
        self.assertEqual(len(client1.get_project_sub_target_segment_list()), 1)

    def test_get_projects_sdg_goals(self):
        from v2.project.models import Project
        client1 = Client.objects.get(display_name="client 123")
        proj = Project.objects.create(project_name = "testing project 1", client=client1, project_status=200, budget=120, total_utilised_amount=600)
        g = ProjectSDGGoals.objects.create(project=proj, sdg_goal_name="test goal")
        self.assertEqual(len(client1.get_projects_sdg_goals()), 0)

    def test_get_project_over_utilization(self):
        from v2.project.models import Project
        client1 = Client.objects.get(display_name="client 123")
        proj = Project.objects.create(project_name = "testing project 1", client=client1, project_status=200, budget=120, total_utilised_amount=600)
        self.assertEqual(len(client1.get_project_over_utilization()), 0)

    def test_get_last_compliance(self):
        from v2.client.models import ClientComplianceCheckList
        client1 = Client.objects.get(display_name="client 123")
        c = ClientComplianceCheckList.objects.create(client=client1)
        ClientComplianceEvaluation.objects.create(client=client1, compliance_checklist=c)
        # self.assertEqual(len(), 19)

    def test_get_last_compliance_date(self):
        from v2.client.models import ClientComplianceCheckList
        date_captured = datetime.date.today()
        client1 = Client.objects.get(display_name="client 123")
        c = ClientComplianceCheckList.objects.create(client=client1)
        ClientComplianceEvaluation.objects.create(client=client1, compliance_checklist=c, date_captured=date_captured)
        self.assertEqual(client1.get_last_compliance_date(), date_captured)

    @patch('v2.project.tasks.calculate_total_and_planned_disbursement_amount.apply_async')
    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def test_get_count_of_risk_status(self, mock_send,mock_send2):
        from v2.risk_management.models import RiskEntry, RiskCategory, Risk
        from v2.plant.models import Plant
        from v2.project.models import Milestone, Project

        client1 = Client.objects.get(display_name="client 123")
        proj = Project.objects.create(project_name = "testing project 1", client=client1, project_status=200, budget=120, total_utilised_amount=600)
        risk_category = RiskCategory.objects.create(name="dummy")
        risk = Risk.objects.create(severity=0, name="risk 1", trigger_name="trigger 1", category=risk_category, description="Dummy description")
        plant = Plant.objects.create(plant_name="plant 1", client=client1)
        milestone = Milestone.objects.create(milestone_name="milestone 1", milestone_description="no description", status="OK", project=proj)
        risk_entry = RiskEntry.objects.create(name="risk entry 1", project=proj, client=client1, plant=plant, risk=risk, category=risk_category, content="DUMMY TEXT", milestone=milestone)
        self.assertEqual(client1.get_count_of_risk_status()['Low'], 1)

    def test_get_all_client_files(self):
        pass

    def test_get_all_client_images(self):
        pass

    def test_get_all_projects_list_by_date_range(self):
        from v2.project.models import Project
        start_date = datetime.date.today()
        end_date = datetime.date.today()
        client1 = Client.objects.get(display_name="client 123")
        proj = Project.objects.create(project_name = "testing project 1", client=client1, project_status=200, budget=120, total_utilised_amount=600, start_date=start_date, end_date=end_date)
        self.assertEqual(client1.get_all_projects_list_by_date_range(start_date, end_date).count(), 1)


    def test_get_all_plants_list_by_date_range(self):
        from v2.project.models import Project, Plant
        start_date = datetime.date.today()
        end_date = datetime.date.today()
        client1 = Client.objects.get(display_name="client 123")
        plant = Plant.objects.create(plant_name="plant 1", client=client1, start_date=start_date, end_date=end_date)
        self.assertEqual(client1.get_all_plants_list_by_date_range(start_date, end_date).count(), 1)


    def test_get_dashboard(self):
        client1 = Client.objects.get(display_name="client 123")
        dashboard = ClientDashboard.objects.create(client=client1, provider=0, dashboard_name="D1", url="dashboard.com", embedded_content="emb content", password="123#")
        self.assertEqual(client1.get_dashboard().count(), 1)


class ClientDocumentTestCase(TestSetUp):
    def setUp(self):
        super().setUp()
        ClientDocument.objects.create(client=self.client_obj,
         document_file=SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
         document_tag=['tag', 'tag2'],
         path='path')

    def test_values(self):
        client_document = ClientDocument.objects.get(client=self.client_obj)
        self.assertEqual(client_document.document_tag, ['tag', 'tag2'])
        self.assertEqual(client_document.path, 'path')


class ClientImageTestCase(TestSetUp):
    def setUp(self):
        super().setUp()
        ClientImage.objects.create(
            client=self.client_obj,
            image_file = SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            path = "dummy_path",
            document_tag = ['tag', 'tag2']
        )

    def test_values(self):
        clientImage = ClientImage.objects.get(client=self.client_obj)
        self.assertEqual(clientImage.path, 'dummy_path')
        self.assertEqual(clientImage.document_tag, ['tag', 'tag2'])

class ClientOverviewTestCase(TestSetUp):
    def setUp(self):
        super().setUp()
        ClientOverview.objects.create(client=self.client_obj, vision='vision', mission='mission')


    def test_values(self):
        client_overview = ClientOverview.objects.get(client=self.client_obj)
        self.assertEqual(client_overview.vision, 'vision')
        self.assertEqual(client_overview.mission, 'mission')


class ClientUserTestCase(TestSetUp):
    def setUp(self):
        super().setUp()
        ClientUser.objects.create(client=self.client_obj, user=User.objects.get(email='diksha@test.com'))

    def test_values(self):
        client_user = ClientUser.objects.get(client=self.client_obj)
        self.assertEqual(client_user.user.email, 'diksha@test.com')


class ClientDetailTestCase(TestSetUp):
    def setUp(self):
        super().setUp()
        ClientDetail.objects.create(
            client=self.client_obj,
            contact_name="Person name",
            contact_email="person_email@example.com",
            primary_phone='57236403',
            alternative_number='1263871263',
            address='32, street 1',
            pin_code='283203',
            state='Uttar Pradesh',
            city='City'
        )

    def test_values(self):
        client_detail = ClientDetail.objects.get(client=self.client_obj)
        self.assertEqual(client_detail.contact_name, 'Person name')
        self.assertEqual(client_detail.contact_email, 'person_email@example.com')
        self.assertEqual(client_detail.primary_phone, '57236403')
        self.assertEqual(client_detail.alternative_number, '1263871263')
        self.assertEqual(client_detail.address, '32, street 1')
        self.assertEqual(client_detail.pin_code, '283203')
        self.assertEqual(client_detail.state, 'Uttar Pradesh')
        self.assertEqual(client_detail.city, 'City')


class ClientComplianceCheckListTestCase(TestSetUp):
    def setUp(self):
        super().setUp()
        self.checklist = ClientComplianceCheckList.objects.create(
            client=Client.objects.get(display_name='client Test10'),
            area="Education",
            indicator="An indicator to assess",
            compulsion=True,
            document_required=True
        )

    def test_values(self):
        compliance_checklist = ClientComplianceCheckList.objects.get(checklist_id=self.checklist.checklist_id)
        self.assertEqual(compliance_checklist.area, 'Education')
        self.assertEqual(compliance_checklist.indicator, 'An indicator to assess')
        self.assertEqual(compliance_checklist.compulsion, True)
        self.assertEqual(compliance_checklist.document_required, True)


class ClientComplianceScheduleTestCase(TestSetUp):
    def setUp(self):
        super().setUp()
        self.schedule = ClientComplianceSchedule.objects.create(frequency='Annually')

    def test_values(self):
        compliance_schedule = ClientComplianceSchedule.objects.get(id=self.schedule.id)
        self.assertEqual(compliance_schedule.frequency, 'Annually')


class ClientComplianceEvaluationTestCase(TestSetUp):
    def setUp(self):
        super().setUp()
        checklist = ClientComplianceCheckList.objects.create(
            client=Client.objects.get(display_name = 'client Test10'),
            area="Education",
            indicator="An indicator",
            compulsion=True,
            document_required=True
        )
        self.evaluation = ClientComplianceEvaluation.objects.create(
            client=Client.objects.get(display_name = 'client Test10'),
            compliance_checklist=checklist,
            value=True,
            support_document='file',
            date_captured=datetime.date(2021, 8, 10),
            batch_id='B73982VJH'
        )

    def test_values(self):
        compliance_evaluation = ClientComplianceEvaluation.objects.get(id=self.evaluation.id)
        self.assertEqual(compliance_evaluation.value, True)
        self.assertEqual(compliance_evaluation.support_document, 'file')
        self.assertEqual(compliance_evaluation.date_captured, datetime.date(2021, 8, 10))
        self.assertEqual(compliance_evaluation.batch_id, 'B73982VJH')

class TestUserNotificationSettings(TestSetUp):
    def setUp(self):
        super().setUp()
        self.notificationSetting = UserNotificationSettings.objects.create(
            user=User.objects.get(email='diksha@test.com'),
            settings_name = 9,
            is_enabled = True,
            is_email_enabled = True,
            is_periodic = True,
            days = 5
            )

    def test_validation(self):
        with self.assertRaises(ValueError):
            UserNotificationSettings.objects.create(
            user=User.objects.get(email='diksha@test.com'),
            settings_name = "hi",
            is_enabled = True,
            is_email_enabled = True,
            is_periodic = True,
            days = "hi"
            )

    def test_values(self):
        notification = UserNotificationSettings.objects.get(id=self.notificationSetting.id)
        self.assertEqual(notification.user.email, "diksha@test.com")
        self.assertEqual(notification.settings_name, 9)
        self.assertEqual(notification.is_enabled, True)
        self.assertEqual(notification.is_email_enabled, True)
        self.assertEqual(notification.is_periodic, True)
        self.assertEqual(notification.days, 5)

class ClientDashboardTestCase(TestSetUp):
    def setUp(self):
        super().setUp()
        ClientDashboard.objects.create(
            client=Client.objects.get(display_name='client Test10'),
            provider=0,
            url='http://www.qa.shift.sattva.co.in/',
            dashboard_name='Dashboard 1',
            embedded_content='content',
            password='Password123'
        )

    def test_values(self):
        dashboard = ClientDashboard.objects.get(client=self.client_obj)
        self.assertEqual(dashboard.provider, 0)
        self.assertEqual(dashboard.url, 'http://www.qa.shift.sattva.co.in/')
        self.assertEqual(dashboard.dashboard_name, 'Dashboard 1')
        self.assertEqual(dashboard.embedded_content, 'content')
        self.assertEqual(dashboard.password, 'Password123')


class CustomFolderTestCase(TestSetUp):
    def setUp(self):
        super().setUp()
        CustomFolder.objects.create(
            client=Client.objects.get(display_name='client Test10'),
            folder_name='Folder',
            path='/documents/images',
            parent='C15_P21_DOCUMENTS',
            document_tag='tag'
        )

    def test_values(self):
        folder = CustomFolder.objects.get(client=self.client_obj)
        self.assertEqual(folder.folder_name, 'Folder')
        self.assertEqual(folder.path, '/documents/images')
        self.assertEqual(folder.parent, 'C15_P21_DOCUMENTS')
        self.assertEqual(folder.document_tag, 'tag')


class ClientSettingsTestCase(TestSetUp):
    def setUp(self):
        super().setUp()
        ClientSettings.objects.update_or_create(
            client=Client.objects.get(display_name='client Test10'),
            defaults={
                'disbursement_workflow':True,
                'utilisation_workflow':False,
                'impact_workflow':True
            }

        )

    def test_values(self):
        setting = ClientSettings.objects.get(client=self.client_obj)
        self.assertEqual(setting.disbursement_workflow, True)
        self.assertEqual(setting.utilisation_workflow, False)
        self.assertEqual(setting.impact_workflow, True)
