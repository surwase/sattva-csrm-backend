# Generated by Django 2.0.5 on 2019-09-12 05:48

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('created_by', models.CharField(blank=True, max_length=50)),
                ('updated_date', models.DateTimeField(blank=True, null=True)),
                ('updated_by', models.CharField(blank=True, max_length=50, null=True)),
                ('client_name', models.CharField(max_length=511, unique=True)),
                ('display_name', models.CharField(default='', max_length=511, unique=True)),
                ('website_url', models.CharField(blank=True, max_length=2047, null=True)),
                ('client_cin', models.CharField(blank=True, max_length=21, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
