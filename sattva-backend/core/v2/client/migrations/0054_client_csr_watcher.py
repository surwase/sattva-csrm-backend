# Generated by Django 2.2.16 on 2021-08-05 14:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0053_clientsettings_impact_workflow'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='csr_watcher',
            field=models.IntegerField(default=5),
        ),
    ]
