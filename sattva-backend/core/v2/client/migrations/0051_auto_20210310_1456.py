# Generated by Django 2.2.16 on 2021-03-10 09:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0050_client_is_kobo_enabled'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='csr_admin',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='client',
            name='csr_user',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='client',
            name='data_collector',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='client',
            name='ngo_user',
            field=models.IntegerField(default=0),
        ),
    ]
