from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.postgres.fields import JSONField


"""
    This Model is used to store the Help Sections.
"""
class HelpSections(models.Model):
    section_name = models.CharField(max_length=127)
    path = models.CharField(max_length=127)

    def __str__(self):
        return self.section_name


"""
    This Model is used to store the help sub sections.
"""
class HelpSubSections(models.Model):
    section = models.ForeignKey(HelpSections, on_delete=models.CASCADE)
    title = models.CharField(max_length=127)
    path = models.CharField(max_length=127)
    code = models.CharField(max_length=127)
    roles = JSONField(default=[])
    level = models.FloatField(max_length=31, default=0)
    is_head = models.BooleanField(default=False)

    def __str__(self):
        return self.code


"""
    This Model is used to store the help articles.
"""
class HelpArticles(models.Model):
    content = RichTextUploadingField()
    title = models.CharField(max_length=127)
    sub_section = models.ForeignKey(HelpSubSections, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


"""
    This Model is used to store the Video Details for help section.
"""
class VideoDetails(models.Model):
    link = models.TextField(default='')
    title = models.CharField(default='', max_length=255)
    description = models.TextField(default='')
    sub_section = models.ForeignKey(HelpSubSections, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
