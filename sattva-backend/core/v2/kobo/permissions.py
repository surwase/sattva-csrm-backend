from v2.project.permissions import check_if_user_has_project_feature_permission, BaseProjectPermission


class ObservePermission(BaseProjectPermission):
    feature = 'IMPACT'

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        project = request.GET.get('shift_project')
        if request.user.is_anonymous:
            return False
        elif request.user.application_role.role_name in ['sattva-admin', 'sattva-user']:
            return True
        elif view.action == 'list' and project:
            return True
        elif view.action == 'list':
            return False
        return True

    def has_object_permission(self, request, view, obj):
        """
        Return `True` if permission is granted, `False` otherwise.
        This will be used when a specific object is accessed using
        pk.
        """
        if request.user.application_role.role_name in ['sattva-admin', 'sattva-user']:
            return True
        return False
