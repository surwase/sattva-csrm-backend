SMALL_CASE = "abcdefghijklmnopqrstuvwxyz"
CAPITAL_CASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
DIGITS = "01234567890"
SMALL_CASE_LENGTH = 5
CAPITAL_CASE_LENGTH = 1
DIGIT_LENGTH = 1

DB_BASE_USER_POSTFIX = "_base_user"
DB_NAME_PREFIX = "CL_"

CREATE_USER_QUERY = "CREATE ROLE {0} WITH LOGIN PASSWORD '{1}'"
CREATE_DATABASE_QUERY = """CREATE DATABASE {0} WITH OWNER {1}"""
CREATE_SCHEMA_QUERY = """CREATE SCHEMA {0}"""
GRANT_SCHEMA_PERMISSION = """GRANT USAGE ON SCHEMA {0} TO {1};"""
GRANT_FUTURE_TABLES_TO_ROLE = (
    """ALTER DEFAULT PRIVILEGES IN SCHEMA {0} GRANT SELECT ON TABLES TO {1}"""
)
GRANT_EXISTING_TABLES_TO_ROLE = """GRANT SELECT ON ALL TABLES IN SCHEMA {0} TO {1}"""

TABLE_EXISTS_QUERY = "SELECT 1 from information_schema.tables WHERE table_name = '{0}'"

DROP_USER_QUERY = """
drop role {0};
"""

DROP_SCHEMA_QUERY = """
drop schema {0} cascade;
"""


CREATE_TABLE_CLIENT = """
        CREATE TABLE {1}.client_client AS
        SELECT * FROM public.client_client
        WHERE id={0};
        """

CREATE_TABLE_PLANT = """
        CREATE TABLE {1}.plant_plant AS
        SELECT * FROM public.plant_plant
        WHERE client_id={0};
        """

CREATE_TABLE_PLANT_FOCUS_AREA = """
        CREATE TABLE {1}.plant_plantfocusarea AS
            SELECT * from plant_plantfocusarea
            WHERE plant_plantfocusarea.plant_id
            IN(
                SELECT id FROM
                plant_plant
                WHERE plant_plant.client_id = {0}
            )
    """

CREATE_TABLE_PLANT_SUB_FOCUS_AREA = """
    CREATE TABLE {1}.plant_plantsubfocusarea AS
    SELECT *
    FROM plant_plantsubfocusarea
    WHERE plant_plantsubfocusarea.plant_id
    IN (
        SELECT id FROM
      plant_plant
      WHERE plant_plant.client_id = {0}
  );"""

CREATE_TABLE_PLANT_TARGET_SEGMENT = """
    CREATE TABLE {1}.plant_planttargetsegment AS
    SELECT * FROM plant_planttargetsegment
    WHERE plant_planttargetsegment.plant_id
    IN (
        SELECT id FROM
      plant_plant
      WHERE plant_plant.client_id = {0}
      );
    """

CREATE_TABLE_PLANT_SUB_TARGET_SEGMENT = """
    CREATE TABLE {1}.plant_plantsubtargetsegment AS
    SELECT * FROM plant_plantsubtargetsegment
    WHERE plant_plantsubtargetsegment.plant_id
    IN (
        SELECT id FROM
      plant_plant
      WHERE plant_plant.client_id = {0}
  );
"""

CREATE_TABLE_PLANT_SDG = """
    CREATE TABLE {1}.plant_plantsdggoals AS
    SELECT * FROM plant_plantsdggoals
    WHERE plant_plantsdggoals.plant_id
    IN (
    SELECT id FROM
      plant_plant
      WHERE plant_plant.client_id = {0}
    );
"""

CREATE_TABLE_PLANT_LOCATION = """
    CREATE TABLE {1}.plant_plantlocation AS
    SELECT * FROM plant_plantlocation
    WHERE plant_plantlocation.plant_id
    IN (
      SELECT id FROM
      plant_plant
      WHERE plant_plant.client_id = {0}
      );
"""

CREATE_TABLE_PLANT_S7 = """
    CREATE TABLE {1}.plant_plantschedulevii AS
    SELECT * FROM plant_plantschedulevii
    WHERE plant_plantschedulevii.plant_id
    IN (
      SELECT id FROM
      plant_plant
      WHERE plant_plant.client_id = {0}
      );
"""

CREATE_TABLE_PROJECT = """
    CREATE TABLE {1}.project_project AS
    SELECT * FROM project_project
    WHERE project_project.client_id = {0};
"""

CREATE_TABLE_PROJECT_FOCUS_AREA = """
    CREATE TABLE {1}.project_projectfocusarea AS
    SELECT * FROM project_projectfocusarea
    WHERE project_projectfocusarea.project_id
    IN (
      SELECT id FROM
      project_project
      WHERE project_project.client_id = {0}
    );

"""

CREATE_TABLE_PROJECT_SUB_FOCUS_AREA = """
    CREATE TABLE {1}.project_projectsubfocusarea AS
    SELECT * FROM project_projectsubfocusarea
    WHERE project_projectsubfocusarea.project_id
    IN (
      SELECT id FROM
      project_project
      WHERE project_project.client_id = {0}
    );
"""

CREATE_TABLE_PROJECT_TARGET_SEGMENT = """
    CREATE TABLE {1}.project_projecttargetsegment AS
    SELECT * FROM project_projecttargetsegment
    WHERE project_projecttargetsegment.project_id
    IN (
      SELECT id FROM
      project_project
      WHERE project_project.client_id = {0}
    );
"""

CREATE_TABLE_PROJECT_SUB_TARGET_SEGMENT = """
    CREATE TABLE {1}.project_projectsubtargetsegment AS
    SELECT * FROM project_projectsubtargetsegment
    WHERE project_projectsubtargetsegment.project_id
    IN (
      SELECT id FROM
      project_project
      WHERE project_project.client_id = {0}
    );

"""

CREATE_TABLE_PROJECT_SDG = """
    CREATE TABLE {1}.project_projectsdggoals AS
    SELECT * FROM project_projectsdggoals
    WHERE project_projectsdggoals.project_id
    IN (
      SELECT id FROM
      project_project
      WHERE project_project.client_id = {0}
    );
"""

CREATE_TABLE_PROJECT_TAG = """
    CREATE TABLE {1}.project_projecttag AS
    SELECT * FROM project_projecttag
    WHERE project_projecttag.project_id
    IN (
      SELECT id FROM
      project_project
      WHERE project_project.client_id = {0}
    );
"""

CREATE_TABLE_PROJECT_LOCATION = """
    CREATE TABLE {1}.project_projectlocation AS
    SELECT * FROM project_projectlocation
    WHERE project_projectlocation.project_id
    IN (
      SELECT id FROM
        project_project
        WHERE project_project.client_id = {0}
    );
"""

CREATE_TABLE_PROJECT_NGOPARTNER = """

CREATE TABLE {1}.project_ngo_partner AS
SELECT * FROM project_ngo_partner
WHERE project_ngo_partner.project_id
IN (
  SELECT id FROM
  project_project
  WHERE project_project.client_id = {0}
);

"""

CREATE_TABLE_MILESTONE = """
CREATE TABLE {1}.milestone AS
SELECT * FROM milestone
WHERE milestone.project_id
IN (
  SELECT id FROM
  project_project
  WHERE project_project.client_id = {0}
);

"""

CREATE_TABLE_TASKS = """
CREATE TABLE {1}.project_task AS
SELECT * FROM project_task
WHERE project_task.client_id = {0};
"""

CREATE_TABLE_DISBURSEMENT = """
CREATE TABLE {1}.project_disbursement AS
SELECT * FROM project_disbursement
WHERE project_disbursement.project_id
IN (
  SELECT id FROM
  project_project
  WHERE project_project.client_id = {0}
);

"""

CREATE_TABLE_UTILISATION = """
CREATE TABLE {1}.project_utilisation AS
SELECT * FROM project_utilisation
WHERE project_utilisation.project_id
IN (
  SELECT id FROM
  project_project
  WHERE project_project.client_id = {0}
);
"""


CREATE_TABLE_CASE_STUDY = """
CREATE TABLE {1}.project_impactcasestudy AS
SELECT * FROM project_impactcasestudy
WHERE project_impactcasestudy.project_id
IN (
  SELECT id FROM
  project_project
  WHERE project_project.client_id = {0}
);
"""


CREATE_INDICATOR_TABLE = """
CREATE TABLE {1}.project_projectoutputoutcome as

SELECT frequency, output_indicator, outcome_indicator, is_beneficiary,
project_projectoutput.scheduled_date, project_projectoutput.period_end_date, planned_output, actual_output, planned_outcome,
actual_outcome, project_projectindicator.milestone_id as milestone_id, project_projectindicator.project_id as project_id, project_projectindicator.created_date,
project_projectindicator.created_by, project_projectindicator.updated_date, project_projectindicator.updated_by, project_projectindicator.created_by as indicator_batch,
project_projectindicator.updated_by_username, project_projectindicator.created_by_username, project_projectoutcome.period_name,
project_projectoutput.is_included, output_calculation, outcome_calculation, project_projectindicator.id as id, project_projectindicator.location

FROM project_projectindicator, project_projectoutcome, project_projectoutput

WHERE project_projectindicator.id = project_projectoutcome.project_indicator_id
AND project_projectindicator.id = project_projectoutput.project_indicator_id
AND project_projectoutcome.period_name = project_projectoutput.period_name
AND project_projectindicator.project_id IN (
  SELECT id FROM
  project_project
  WHERE project_project.client_id = {0}
);
;
"""

CREATE_PROJECT_USER_TABLE = """
CREATE TABLE {1}.project_projectuser AS
SELECT * FROM project_projectuser
WHERE project_projectuser.project_id
IN (
  SELECT id FROM
  project_project
  WHERE project_project.client_id = {0}
);
"""

CREATE_TABLE_NGO_NGOPARTNER = """

CREATE TABLE {1}.ngo_ngopartner AS
SELECT * FROM ngo_ngopartner;

"""


CREATE_TABLE_UTILISATIONEXPENSE = """
CREATE TABLE {1}.project_utilisationexpense AS
SELECT pue.id, pue.created_date, pue.created_by, pue.created_by_username, pue.updated_date, pue.updated_by,
pue.updated_by_username, pue.split_frequency, pue.period_start_date, pue.period_end_date, pue.estimate_cost,
pue.number_of_units, pue.unit_cost, pue.actual_cost, pue.remarks, pue.comments, pue.utilisation_id,
pue.utilisation_upload_id, pue.description, pue.planned_cost
FROM project_utilisationexpense pue
INNER JOIN project_utilisation ON (pue.utilisation_id = project_utilisation.id) WHERE project_utilisation.project_id
IN (
  SELECT project_project.id FROM
  project_project
  WHERE project_project.client_id = {0}
);
"""

CREATE_TABLE_UTILISATIONUPLOAD = """
CREATE TABLE {1}.project_utilisationupload AS
SELECT * FROM project_utilisationupload
WHERE project_utilisationupload.project_id
IN (
  SELECT id FROM
  project_project
  WHERE project_project.client_id = {0}
);
"""

CREATE_TABLE_UTILISATIONEXPENSEDOCUMENT = """
CREATE TABLE {1}.project_utilisationexpensedocument AS
SELECT * FROM project_utilisationexpensedocument WHERE utilisation_expense_id IN(
SELECT pue.id FROM project_utilisationexpense pue
INNER JOIN project_utilisation ON (pue.utilisation_id = project_utilisation.id) WHERE project_utilisation.project_id
IN (
  SELECT project_project.id FROM
  project_project
  WHERE project_project.client_id = {0}
)
);
"""




IMPACT_DATA_TABLE = """
CREATE TABLE {2}.{0}
as select * from {1};
"""

CLIENT_TABLES = (
    ("CREATE_TABLE_CLIENT", CREATE_TABLE_CLIENT),
    ("CREATE_TABLE_PLANT", CREATE_TABLE_PLANT),
    ("CREATE_TABLE_PLANT_FOCUS_AREA", CREATE_TABLE_PLANT_FOCUS_AREA),
    ("CREATE_TABLE_PLANT_SUB_FOCUS_AREA", CREATE_TABLE_PLANT_SUB_FOCUS_AREA),
    ("CREATE_TABLE_PLANT_TARGET_SEGMENT", CREATE_TABLE_PLANT_TARGET_SEGMENT),
    ("CREATE_TABLE_PLANT_SUB_TARGET_SEGMENT", CREATE_TABLE_PLANT_SUB_TARGET_SEGMENT),
    ("CREATE_TABLE_PLANT_SDG", CREATE_TABLE_PLANT_SDG),
    ("CREATE_TABLE_PLANT_LOCATION", CREATE_TABLE_PLANT_LOCATION),
    ("CREATE_TABLE_PLANT_S7", CREATE_TABLE_PLANT_S7),
    ("CREATE_TABLE_PROJECT", CREATE_TABLE_PROJECT),
    ("CREATE_TABLE_PROJECT_FOCUS_AREA", CREATE_TABLE_PROJECT_FOCUS_AREA),
    ("CREATE_TABLE_PROJECT_SUB_FOCUS_AREA", CREATE_TABLE_PROJECT_SUB_FOCUS_AREA),
    ("CREATE_TABLE_PROJECT_TARGET_SEGMENT", CREATE_TABLE_PROJECT_TARGET_SEGMENT),
    (
        "CREATE_TABLE_PROJECT_SUB_TARGET_SEGMENT",
        CREATE_TABLE_PROJECT_SUB_TARGET_SEGMENT,
    ),
    ("CREATE_TABLE_PROJECT_SDG", CREATE_TABLE_PROJECT_SDG),
    ("CREATE_TABLE_PROJECT_TAG", CREATE_TABLE_PROJECT_TAG),
    ("CREATE_TABLE_PROJECT_LOCATION", CREATE_TABLE_PROJECT_LOCATION),
    ("CREATE_TABLE_PROJECT_NGOPARTNER", CREATE_TABLE_PROJECT_NGOPARTNER),
    ("CREATE_TABLE_MILESTONE", CREATE_TABLE_MILESTONE),
    ("CREATE_TABLE_TASKS", CREATE_TABLE_TASKS),
    ("CREATE_TABLE_DISBURSEMENT", CREATE_TABLE_DISBURSEMENT),
    ("CREATE_TABLE_UTILISATION", CREATE_TABLE_UTILISATION),
    ("CREATE_TABLE_CASE_STUDY", CREATE_TABLE_CASE_STUDY),
    ("CREATE_INDICATOR_TABLE", CREATE_INDICATOR_TABLE),
    ("CREATE_TABLE_NGO_NGOPARTNER", CREATE_TABLE_NGO_NGOPARTNER),
    ("CREATE_TABLE_UTILISATIONEXPENSE", CREATE_TABLE_UTILISATIONEXPENSE),
    ("CREATE_TABLE_UTILISATIONUPLOAD", CREATE_TABLE_UTILISATIONUPLOAD),
    ("CREATE_TABLE_UTILISATIONEXPENSEDOCUMENT", CREATE_TABLE_UTILISATIONEXPENSEDOCUMENT),
)

CREATE_PRIMARY_KEY = """
ALTER TABLE {0}.client_client ADD PRIMARY KEY (id);
ALTER TABLE {0}.plant_plant ADD PRIMARY KEY (id);
ALTER TABLE {0}.plant_plantfocusarea ADD PRIMARY KEY (id);
ALTER TABLE {0}.plant_plantschedulevii ADD PRIMARY KEY (id);
ALTER TABLE {0}.plant_plantsdggoals ADD PRIMARY KEY (id);
ALTER TABLE {0}.plant_plantsubfocusarea ADD PRIMARY KEY (id);
ALTER TABLE {0}.plant_plantsubtargetsegment ADD PRIMARY KEY (id);
ALTER TABLE {0}.plant_planttargetsegment ADD PRIMARY KEY (id);
ALTER TABLE {0}.project_disbursement ADD PRIMARY KEY (id);
ALTER TABLE {0}.milestone ADD PRIMARY KEY (id);
ALTER TABLE {0}.project_ngo_partner ADD PRIMARY KEY (id);
ALTER TABLE {0}.project_project ADD PRIMARY KEY (id);
ALTER TABLE {0}.project_projectfocusarea ADD PRIMARY KEY (id);
ALTER TABLE {0}.project_projectlocation ADD PRIMARY KEY (id);
ALTER TABLE {0}.project_projectsdggoals ADD PRIMARY KEY (id);
ALTER TABLE {0}.project_projectsubfocusarea ADD PRIMARY KEY (id);
ALTER TABLE {0}.project_projectsubtargetsegment ADD PRIMARY KEY (id);
ALTER TABLE {0}.project_projecttag ADD PRIMARY KEY (id);
ALTER TABLE {0}.project_projecttargetsegment ADD PRIMARY KEY (id);
ALTER TABLE {0}.project_task ADD PRIMARY KEY (id);
ALTER TABLE {0}.project_utilisation ADD PRIMARY KEY (id);
ALTER TABLE {0}.project_impactcasestudy ADD PRIMARY KEY (id);
ALTER TABLE {0}.ngo_ngopartner ADD PRIMARY KEY (id);
ALTER TABLE {0}.project_utilisationexpense ADD PRIMARY KEY (id);
ALTER TABLE {0}.project_utilisationexpensedocument ADD PRIMARY KEY (id);
ALTER TABLE {0}.project_utilisationupload ADD PRIMARY KEY (id);
"""


CREATE_FOREGIN_KEY_CONSTRAINTS = """
ALTER TABLE {0}.plant_plant ADD FOREIGN KEY (client_id) REFERENCES {0}.client_client(id);
ALTER TABLE {0}.plant_plantfocusarea ADD FOREIGN KEY (plant_id) REFERENCES {0}.plant_plant(id);
ALTER TABLE {0}.plant_plantschedulevii ADD FOREIGN KEY (plant_id) REFERENCES {0}.plant_plant(id);
ALTER TABLE {0}.plant_plantsdggoals ADD FOREIGN KEY (plant_id) REFERENCES {0}.plant_plant(id);
ALTER TABLE {0}.plant_plantsubfocusarea ADD FOREIGN KEY (plant_id) REFERENCES {0}.plant_plant(id);
ALTER TABLE {0}.plant_plantsubtargetsegment ADD FOREIGN KEY (plant_id) REFERENCES {0}.plant_plant(id);
ALTER TABLE {0}.plant_planttargetsegment ADD FOREIGN KEY (plant_id) REFERENCES {0}.plant_plant(id);
ALTER TABLE {0}.project_project ADD FOREIGN KEY (client_id) REFERENCES {0}.client_client(id);
ALTER TABLE {0}.project_project ADD FOREIGN KEY (ngo_partner_id) REFERENCES {0}.ngo_ngopartner(id);
ALTER TABLE {0}.project_project ADD FOREIGN KEY (plant_id) REFERENCES {0}.plant_plant(id);
ALTER TABLE {0}.milestone ADD FOREIGN KEY (project_id) REFERENCES {0}.project_project(id);
ALTER TABLE {0}.project_disbursement ADD FOREIGN KEY (project_id) REFERENCES {0}.project_project(id);
ALTER TABLE {0}.project_disbursement ADD FOREIGN KEY (milestone_id) REFERENCES {0}.milestone(id);
-- ALTER TABLE {0}.project_ngo_partner ADD FOREIGN KEY (ngo_id) REFERENCES {0}.ngo(ngo_id);
ALTER TABLE {0}.project_ngo_partner ADD FOREIGN KEY (project_id) REFERENCES {0}.project_project(id);
ALTER TABLE {0}.project_projectfocusarea ADD FOREIGN KEY (project_id) REFERENCES {0}.project_project(id);
ALTER TABLE {0}.project_projectlocation ADD FOREIGN KEY (project_id) REFERENCES {0}.project_project(id);
ALTER TABLE {0}.project_projectsdggoals ADD FOREIGN KEY (project_id) REFERENCES {0}.project_project(id);
ALTER TABLE {0}.project_projectsubfocusarea ADD FOREIGN KEY (project_id) REFERENCES {0}.project_project(id);
ALTER TABLE {0}.project_projectsubtargetsegment ADD FOREIGN KEY (project_id) REFERENCES {0}.project_project(id);
ALTER TABLE {0}.project_projecttag ADD FOREIGN KEY (project_id) REFERENCES {0}.project_project(id);
ALTER TABLE {0}.project_projecttargetsegment ADD FOREIGN KEY (project_id) REFERENCES {0}.project_project(id);
ALTER TABLE {0}.project_task ADD FOREIGN KEY (client_id) REFERENCES {0}.client_client(id);
ALTER TABLE {0}.project_task ADD FOREIGN KEY (milestone_id) REFERENCES {0}.milestone(id);
ALTER TABLE {0}.project_task ADD FOREIGN KEY (project_id) REFERENCES {0}.project_project(id);
ALTER TABLE {0}.project_utilisation ADD FOREIGN KEY (project_id) REFERENCES {0}.project_project(id);
ALTER TABLE {0}.project_impactcasestudy ADD FOREIGN KEY (project_id) REFERENCES {0}.project_project(id);
ALTER TABLE {0}.project_projectoutputoutcome ADD FOREIGN KEY (project_id) REFERENCES {0}.project_project(id);
ALTER TABLE {0}.project_projectoutputoutcome ADD FOREIGN KEY (milestone_id) REFERENCES {0}.milestone(id);
ALTER TABLE {0}.project_utilisationexpense ADD FOREIGN KEY (utilisation_id) REFERENCES {0}.project_utilisation(id);
ALTER TABLE {0}.project_utilisationexpense ADD FOREIGN KEY (utilisation_upload_id) REFERENCES {0}.project_utilisationupload(id);
ALTER TABLE {0}.project_utilisationexpensedocument ADD FOREIGN KEY (utilisation_expense_id) REFERENCES {0}.project_utilisationexpense(id);
ALTER TABLE {0}.project_utilisationupload ADD FOREIGN KEY (project_id) REFERENCES {0}.project_project(id);
"""
