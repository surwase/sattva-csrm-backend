import django_filters
from django_filters import rest_framework as filters
from rest_framework import viewsets, serializers, status
from rest_framework.views import APIView
from rest_framework.response import Response
from domainlibraries.models import CSRComplianceCheckList, FocusArea, SubFocusArea, TargetSegment, SubTargetSegment, \
    ScheduleVIIActivity, MCASector
from domainlibraries.serializers import CSRComplianceCheckListSerializer, FocusAreaSerializer, SubFocusAreaSerializer, \
    TargetSegmentSerializer, SubTargetSegmentSerializer, MCASectorSerializer


class CSRComplianceCheckListFilter(django_filters.FilterSet):
    class Meta:
        model = CSRComplianceCheckList
        fields = {
            'area': ['icontains'],
            'indicator': ['icontains'],
        }


class CSRComplianceCheckListModelViewSet(viewsets.ModelViewSet):
    serializer_class = CSRComplianceCheckListSerializer
    queryset = CSRComplianceCheckList.objects.all().order_by('area', 'indicator')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = CSRComplianceCheckListFilter


class FocusAreaFilter(django_filters.FilterSet):
    class Meta:
        model = FocusArea
        fields = {
            'focus_area_name': ['icontains'],
        }


class FocusAreaViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'focusarea:view',
        'POST': 'focusarea:add',
        'PUT': 'focusarea:change',
        'DELETE': 'focusarea:delete',
        'PATCH': 'focusarea:patch'
    }
    serializer_class = FocusAreaSerializer
    queryset = FocusArea.objects.all().order_by('focus_area_name')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = FocusAreaFilter


class SubFocusAreaFilter(django_filters.FilterSet):
    class Meta:
        model = SubFocusArea
        fields = {
            'focus_area_name': ['exact'],
            'sub_focus_area_name': ['icontains'],
        }


class SubFocusAreaViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'subfocusarea:view',
        'POST': 'subfocusarea:add',
        'PUT': 'subfocusarea:change',
        'DELETE': 'subfocusarea:delete',
        'PATCH': 'subfocusarea:patch'
    }
    serializer_class = SubFocusAreaSerializer
    queryset = SubFocusArea.objects.all().order_by('sub_focus_area_name')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = SubFocusAreaFilter


class TargetSegmentViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'targetsegment:view',
        'POST': 'targetsegment:add',
        'PUT': 'targetsegment:change',
        'DELETE': 'targetsegment:delete',
        'PATCH': 'targetsegment:patch'
    }
    serializer_class = TargetSegmentSerializer
    queryset = TargetSegment.objects.all().order_by('target_segment')


class SubTargetSegmentFilter(django_filters.FilterSet):
    class Meta:
        model = SubTargetSegment
        fields = {
            'target_segment': ['exact'],
            'sub_target_segment': ['icontains']
        }


class SubTargetSegmentViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'subtargetsegment:view',
        'POST': 'subtargetsegment:add',
        'PUT': 'subtargetsegment:change',
        'DELETE': 'subtargetsegment:delete',
        'PATCH': 'subtargetsegment:patch'
    }
    serializer_class = SubTargetSegmentSerializer
    queryset = SubTargetSegment.objects.all().order_by('sub_target_segment')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = SubTargetSegmentFilter


class ScheduleVIIActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = ScheduleVIIActivity
        fields = '__all__'


class ScheduleVIIActivityViewSet(viewsets.ModelViewSet):
    serializer_class = ScheduleVIIActivitySerializer
    queryset = ScheduleVIIActivity.objects.all().order_by('id')


class MCASectorFilter(django_filters.FilterSet):
    class Meta:
        model = MCASector
        fields = {
            'mca_focus_area_name': ['icontains'],
        }


class MCASectorViewSet(viewsets.ModelViewSet):
    keycloak_scopes = {
        'GET': 'mcasector:view',
        'POST': 'mcasector:add',
        'PUT': 'mcasector:change',
        'DELETE': 'mcasector:delete',
        'PATCH': 'mcasector:patch'
    }
    serializer_class = MCASectorSerializer
    queryset = MCASector.objects.all().order_by('id')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = MCASectorFilter

class MCASectorApiView(APIView):
    @staticmethod
    def get(request):
        try:
            data = {}
            mca_obj = MCASector.objects.all().values()
            mca_obj2 = MCASector.objects.distinct('mca_focus_area_name').values()
            for i in mca_obj:
                if data.get(i.get('mca_focus_area_name')):
                    data[i.get('mca_focus_area_name')] = data.get(i.get('mca_focus_area_name'))+[i.get('mca_sub_focus_area_name')]
                else:
                    data[i.get('mca_focus_area_name')] = [i.get('mca_sub_focus_area_name')]
            for obj in mca_obj2:
                if data.get(obj.get('mca_focus_area_name')):
                    obj['mca_sub_focus_area_name'] = data[obj.get('mca_focus_area_name')]
                else:
                    obj['mca_sub_focus_area_name'] = [obj.get('mca_sub_focus_area_name')]
            return Response(mca_obj2, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"error": "Something went wrong:" + str(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)




# class MCASubFocusAreaFilter(django_filters.FilterSet):
#     class Meta:
#         model = MCASubFocusArea
#         fields = {
#             'mca_focus_area_name': ['exact'],
#             'mca_sub_focus_area_name': ['icontains'],
#         }
#
#
# class MCASubFocusAreaViewSet(viewsets.ModelViewSet):
#     keycloak_scopes = {
#         'GET': 'mcasubfocusarea:view',
#         'POST': 'mcasubfocusarea:add',
#         'PUT': 'mcasubfocusarea:change',
#         'DELETE': 'mcasubfocusarea:delete',
#         'PATCH': 'mcasubfocusarea:patch'
#     }
#     serializer_class = MCASubFocusAreaSerializer
#     queryset = MCASubFocusArea.objects.all().order_by('mca_sub_focus_area_name')
#     filter_backends = (filters.DjangoFilterBackend,)
#     filter_class = MCASubFocusAreaFilter
