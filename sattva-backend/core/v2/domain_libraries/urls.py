from django.conf.urls import url

from .views import *

app_name = "domain_libraries"

urlpatterns = [
    url(r'^compliance_checklist/$', CSRComplianceCheckListModelViewSet.as_view(
        {'get': 'list'}),
        name='compliance_checklist'),
    url(r'^focus_areas/$', FocusAreaViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='focusarea-list'),
    url(r'^focus_areas/(?P<pk>\d+)/$', FocusAreaViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='focusarea-detail'),

    url(r'^sub_focus_areas/$', SubFocusAreaViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='subfocusarea-list'),
    url(r'^sub_focus_areas/(?P<pk>\d+)/$', SubFocusAreaViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='subfocusarea-detail'),
    url(r'^target_segment/$', TargetSegmentViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='targetsegment-list'),
    url(r'^target_segment/(?P<pk>\d+)/$', TargetSegmentViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='targetsegment-detail'),

    url(r'^sub_target_segment/$', SubTargetSegmentViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='subtargetsegment-list'),
    url(r'^sub_target_segment/(?P<pk>\d+)/$', SubTargetSegmentViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='subtargetsegment-detail'),
    url(r'^schedule_vii_activity/$', ScheduleVIIActivityViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='Schedule VII Activity'),
    url(r'^mca_sector/$', MCASectorViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='mca_sector_list'),
    url(r'^mca_sector/(?P<pk>\d+)/$', MCASectorViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='mca_sector_detail'),
    url(r'^get_mca_sector/$', MCASectorApiView.as_view(),
        name='mca_sector_get'),
]
