import datetime

from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.db import models
from django.db.models import Sum
from django.forms import model_to_dict

import v2.user_management as user_management
from core.authentication import CurrentUser
from domainlibraries.models import DataType, FocusArea, ScheduleVIIActivity, SubFocusArea, TargetSegment
from geolibraries.models import AuditFields, validate_file_extension
from v2.client.models import Client, ClientUser, UserNotificationSettings, CustomFolder
from v2.ngo.models import NGOPartner
from v2.notification import notification_choices, settings_choices
from v2.notification.models import Notification
from v2.plant.models import Plant
from django.core.exceptions import ObjectDoesNotExist


"""
   This model will be used To Store the project.
"""
class Project(AuditFields):
    project_name = models.CharField(max_length=511)
    project_description = models.TextField(null=True, blank=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    plant = models.ForeignKey(Plant, null=True, blank=True, on_delete=models.SET_NULL)
    start_date = models.DateTimeField(default=datetime.datetime.now)
    end_date = models.DateTimeField(default=datetime.datetime.now)
    budget = models.IntegerField(default=0)
    ngo_partner = models.ForeignKey(NGOPartner, on_delete=models.SET_NULL, null=True, blank=True)
    project_status = models.FloatField(max_length=31, default=0, blank=True)
    total_disbursement_amount = models.FloatField(default=0, blank=True)
    planned_disbursement_amount = models.FloatField(default=0, blank=True)
    total_utilised_amount = models.FloatField(default=0, blank=True)

    def save(self, *args, **kwargs):
        new_object = True
        if self.pk:
            new_object = False
        super(Project, self).save(*args, **kwargs)
        if new_object:
            features = user_management.models.ProjectFeature.objects.all()
            for feature in features:
                user_management.models.ProjectFeaturePermission.objects.bulk_create([
                    user_management.models.ProjectFeaturePermission(project=self, feature=feature, permission='View'),
                    user_management.models.ProjectFeaturePermission(project=self, feature=feature, permission='Change'),
                    user_management.models.ProjectFeaturePermission(project=self, feature=feature, permission='Delete'),
                ])

    def __str__(self):
        return self.client.client_name + " - " + self.project_name

    def get_partner_display(self):
        return self.ngo_partner.ngo_name if self.ngo_partner else ''

    def get_milestones(self, milestones=None):
        return Milestone.objects.filter(project=self).order_by('end_date', 'start_date',
                                                               'milestone_name') if milestones is None else milestones

    def get_name(self):
        return self.project_name

    def get_client(self):
        return self.client

    def get_all_project_users(self):
        return ProjectUser.objects.filter(project=self)

    def get_plant(self):
        return self.plant if not self is None else Plant.objects.none()

    def get_completed_milestones(self, milestones=None):
        return self.get_milestones(milestones=milestones).filter(status="Completed")

    def get_milestone_tasks(self, milestone):
        return Task.objects.filter(milestone=milestone).order_by('position', 'start_date', 'task_name')

    def get_project_status(self, milestones=None):
        if not milestones:
            milestones = self.get_milestones()
        milestone_count = milestones.count()
        project_status = 0
        for milestone in milestones:
            tasks = self.get_milestone_tasks(milestone)
            milestone_status = 0
            task_count = tasks.count()
            if milestone.status == 'Completed':
                milestone_status = 1
            else:
                for task in tasks:
                    milestone_status += (1 / task_count) * (
                        task.percentage_completed / 100 if task.percentage_completed else 0 / 100)
            project_status += (1 / milestone_count) * milestone_status
        return round(project_status * 100, 2)

    def get_locations(self):
        locations = ProjectLocation.objects.filter(project=self).values_list('location', flat=True)
        return locations

    def get_focus_area(self):
        focus_area = ProjectFocusArea.objects.filter(project=self).values_list('focus_area', flat=True).order_by(
            'focus_area')
        return focus_area

    def get_sub_focus_area(self):
        sub_focus_area = ProjectSubFocusArea.objects.filter(project=self).values_list('sub_focus_area',
                                                                                      flat=True).order_by(
            'sub_focus_area')
        return sub_focus_area

    def get_target_segment(self):
        target_segment = ProjectTargetSegment.objects.filter(project=self).values_list('target_segment',
                                                                                       flat=True).order_by(
            'target_segment')
        return target_segment

    def get_sub_target_segment(self):
        sub_target_segment = ProjectSubTargetSegment.objects.filter(project=self).values_list('sub_target_segment',
                                                                                              flat=True).order_by(
            'sub_target_segment')
        return sub_target_segment

    def get_sdg_goals(self):
        sdg_goals = ProjectSDGGoals.objects.filter(project=self).values_list('sdg_goal', flat=True).order_by(
            'sdg_goal_name')
        return sdg_goals

    def get_tags(self):
        tags = ProjectTag.objects.filter(project=self, tag__isnull=False).values_list('tag', flat=True).order_by('tag')
        return tags

    def get_all_disbursements(self):
        return Disbursement.objects.filter(project=self).order_by('start_date')

    def get_all_utilisations(self):
        return Utilisation.objects.filter(project=self)

    def get_all_disbursements_documents(self):
        disbursement = self.get_all_disbursements()
        if disbursement:
            disbursement_list = disbursement.values_list('pk', flat=True)
            return DisbursementDocument.objects.filter(disbursement__in=disbursement_list)

    def get_schedule_vii_activites(self):
        return ProjectScheduleVII.objects.filter(project=self)

    def get_all_project_file(self):
        return ProjectDocument.objects.filter(project=self)

    def get_all_disbursement_project_file(self):
        return DisbursementDocument.objects.filter(disbursement__project=self)

    def get_all_milestones_file(self):
        return MilestoneDocument.objects.filter(milestone__project=self)

    def get_all_utilisations_file(self):
        return UtilisationDocument.objects.filter(utilisation__project=self)

    def get_all_utilisation_upload_file(self):
        return UtilisationUploadDocument.objects.filter(utilisation_upload__project=self)

    def get_all_utilisation_expense_file(self):
        return UtilisationExpenseDocument.objects.filter(utilisation_expense__utilisation__project=self)

    def get_all_tasks_file(self):
        return TaskDocument.objects.filter(task__project=self)

    def get_all_output_file(self):
        return ProjectOutputDocument.objects.filter(project_output__project=self)

    def get_all_outcome_file(self):
        return ProjectOutcomeDocument.objects.filter(project_outcome__project=self)

    def get_all_indicators_file(self):
        return IndicatorDocument.objects.filter(indicator__project=self)

    def get_all_casestudies_file(self):
        return CaseStudyDocuments.objects.filter(case_study__project=self)

    def get_all_images(self):
        return ProjectImage.objects.filter(project=self)

    def get_all_disbursement_total_amount(self, disbursement=None):
        if disbursement is not None:
            return disbursement.aggregate(Sum('actual_amount')).get("actual_amount__sum", 0) or 0
        else:
            return (self.get_all_disbursements().aggregate(Sum('actual_amount')).get("actual_amount__sum",
                                                                                     0) if self.get_all_disbursements() else 0) or 0

    def get_all_disbursement_planned_amount(self, disbursement=None):
        if disbursement is not None:
            return disbursement.aggregate(Sum('expected_amount')).get("expected_amount__sum", 0) or 0
        else:
            return self.get_all_disbursements().aggregate(Sum('expected_amount')).get("expected_amount__sum",
                                                                                      0) if self.get_all_disbursements() else 0 or 0

    def get_total_utilised_amount(self, disbursement=None):
        total_utilised_amount = 0
        standalone_utilization_amount = 0
        total_disbursement = disbursement
        if disbursement is None:
            total_disbursement = self.get_all_disbursements()
            standalone_utilization_amount = Utilisation.objects.filter(disbursement=None, project=self).aggregate(
                Sum('actual_cost')).get("actual_cost__sum", 0) or 0
        for disbursement in total_disbursement:
            total_utilised_amount += disbursement.get_overall_utilistation_total()
        return total_utilised_amount + standalone_utilization_amount

    def get_percentage_of_disbursement_by_category(self, disbursement=None):
        categories_list = Disbursement.get_all_categories()
        financial_spendings = {}
        total_budget = self.get_all_disbursement_total_amount(disbursement=disbursement)
        total_disbursement = self.get_all_disbursements()
        if disbursement is not None:
            total_disbursement = disbursement
        for category in categories_list:
            category_disbursement = total_disbursement.filter(expense_category=category)
            category_disbursement_total_amount = (category_disbursement.aggregate(Sum('actual_amount')).get(
                "actual_amount__sum", 0) if category_disbursement else 0) or 0
            financial_spendings.update({category: round(((category_disbursement_total_amount / total_budget) * 100),
                                                        2) if category_disbursement_total_amount > 0 else 0})
        return financial_spendings

    def get_percentage_of_utilisation_by_category(self, utilisations=None):
        categories_list = Utilisation.get_all_categories()
        financial_spendings = {}
        total_utilisation = self.get_total_utilised_amount()
        all_utilisations = utilisations
        if utilisations is None:
            all_utilisations = self.get_all_utilisations()
        for category in categories_list:
            category_utilisations = all_utilisations.filter(expense_category=category)
            category_utilisations_total_amount = (category_utilisations.aggregate(Sum('actual_cost')).get(
                "actual_cost__sum", 0) if category_utilisations else 0) or 0
            spending_percentage = round(((category_utilisations_total_amount / total_utilisation) * 100),
                                        2) if category_utilisations_total_amount > 0 else 0
            if spending_percentage > 0:
                financial_spendings.update(
                    {category: spending_percentage})
        return financial_spendings

    def get_percentages_of_milestones_status(self, milestones=None):
        milestone_stone_status_category_list = Milestone.get_all_status_categories()
        milestone_status = {}
        total_milestones_count = self.get_milestones().count()
        for category in milestone_stone_status_category_list:
            category_disbursement_count = self.get_milestones().filter(status=category).count()
            milestone_status.update({category: (
                round((category_disbursement_count / total_milestones_count) * 100,
                      2)) if total_milestones_count > 0 else 0})
        return milestone_status

    def get_count_of_risk_status(self, start_date=None, end_date=None, location=None):
        from v2.risk_management.models import RiskEntry
        all_project_risk_entries = RiskEntry.objects.filter(project=self)
        if start_date is not None and end_date is not None:
            all_project_risk_entries = all_project_risk_entries.filter(created_date__gte=start_date,
                                                                       created_date__lte=end_date)
        risk_entries_count = all_project_risk_entries.count()
        if risk_entries_count:
            risk_status = {
                'Low': all_project_risk_entries.filter(severity=0).count(),
                "Medium": all_project_risk_entries.filter(severity=1).count(),
                "High": all_project_risk_entries.filter(severity=2).count(),
                "Critical": all_project_risk_entries.filter(severity=3).count(),
                "Total": all_project_risk_entries.count(),
            }
        else:
            risk_status = {}
        return risk_status

    def get_dashboard(self):
        return ProjectDashboard.objects.filter(project=self)

    def is_plant_mapped(self):
        return True if self.plant else False

    def get_plant(self):
        return self.plant


"""
   This model will be used To Store the locations for project.
"""
class ProjectLocation(AuditFields):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    location = JSONField(default=[])
    area = models.CharField(max_length=256, default='', null=True)
    city = models.CharField(max_length=256, default='', null=True)
    state = models.CharField(max_length=256, default='', null=True)
    country = models.CharField(max_length=256, default='', null=True)
    latitude = models.FloatField(max_length=31, default=0, null=True)
    longitude = models.FloatField(max_length=31, default=0, null=True)
    # state_name = models.CharField(max_length=50, default='', null=True)

    def __str__(self):
        return self.project.project_name


"""
   This model will be used To Store the focus area for project.
"""
class ProjectFocusArea(AuditFields):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    focus_area = models.CharField(max_length=255)

    def __str__(self):
        return self.project.project_name


"""
   This model will be used To Store the sub focus area for project.
"""
class ProjectSubFocusArea(AuditFields):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    sub_focus_area = models.CharField(max_length=255)

    def __str__(self):
        return self.project.project_name


"""
   This model will be used To Store the Target Segment for project.
"""
class ProjectTargetSegment(AuditFields):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    target_segment = models.CharField(max_length=255)

    def __str__(self):
        return self.project.project_name


"""
   This model will be used To Store the Sub Target Segment for project.
"""
class ProjectSubTargetSegment(AuditFields):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    sub_target_segment = models.CharField(max_length=255)

    def __str__(self):
        return self.project.project_name


"""
   This model will be used To Store the SDGGoals for project.
"""
class ProjectSDGGoals(AuditFields):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    sdg_goal_name = models.CharField(max_length=255, null=True, blank=True)
    sdg_goal = JSONField(default=[], null=True)

    def __str__(self):
        return self.project.project_name


"""
   This model will be used To Store the tags for project.
"""
class ProjectTag(AuditFields):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    tag = models.CharField(max_length=511, null=True, blank=True)

    def __str__(self):
        return self.project.project_name


"""
   This model will be used To Store the ScheduleVII for project.
"""
class ProjectScheduleVII(AuditFields):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    schedule_vii_activity = models.ForeignKey(ScheduleVIIActivity, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('project', 'schedule_vii_activity')


"""
   This model will be used To store the project user.
"""
class ProjectUser(AuditFields):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return self.project.project_name + " - " + self.user.first_name

    class Meta:
        unique_together = ('project', 'user')


"""
   This model will be used To store the images for project.
"""
class ProjectImage(AuditFields):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    image_file = models.FileField(max_length=10000, validators=[validate_file_extension])
    image_size = models.BigIntegerField(null=True, blank=True)
    document_tag = JSONField(default=[], null=True, blank=True)
    path = models.TextField(default='')

    def __str__(self):
        return self.project.project_name + " - "

    def save(self, *args, **kwargs):
        if not self.pk:
            if self.project.plant:
                self.image_file.name = 'file_system/{0}/{1}/{2}/images/'.format(self.project.client.client_name,
                                                                                self.project.plant.plant_name,
                                                                                self.project.project_name) \
                                       + self.image_file.name
            else:
                self.image_file.name = 'file_system/{0}/{1}/images/'.format(self.project.client.client_name,
                                                                            self.project.project_name) + self.image_file.name
        super(ProjectImage, self).save(*args, **kwargs)


"""
   This model will be used To store the documents for project.
"""
class ProjectDocument(AuditFields):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    document_file = models.FileField(max_length=10000, validators=[validate_file_extension])
    doc_size = models.BigIntegerField(null=True, blank=True)
    document_tag = JSONField(default=[], null=True, blank=True)
    path = models.TextField(default='')

    def __str__(self):
        return self.project.project_name + " - "

    def save(self, *args, **kwargs):
        if not self.pk:
            if self.project.plant:
                self.document_file.name = 'file_system/{0}/{1}/{2}/'.format(self.project.client.client_name,
                                                                            self.project.plant.plant_name,
                                                                            self.project.project_name) + self.document_file.name
            else:
                self.document_file.name = 'file_system/{0}/{1}/'.format(self.project.client.client_name,
                                                                        self.project.project_name) + self.document_file.name
        super(ProjectDocument, self).save(*args, **kwargs)


"""
   This model will be used To store the milestone.
"""
class Milestone(AuditFields):
    milestone_name = models.CharField(max_length=511)
    milestone_description = models.TextField()
    status = models.CharField(max_length=63)
    participants = models.ManyToManyField(ProjectUser, null=True, blank=True)
    start_date = models.DateTimeField(default=datetime.datetime.now)
    end_date = models.DateTimeField(default=datetime.datetime.now)
    actual_start_date = models.DateTimeField(null=True, blank=True)
    actual_end_date = models.DateTimeField(null=True, blank=True)
    sub_focus_area = JSONField(default=[], null=True, blank=True)
    sub_target_segment = JSONField(default=[], null=True, blank=True)
    location = JSONField(default=[], null=True, blank=True)
    tags = JSONField(default=[], null=True, blank=True)
    project = models.ForeignKey(Project, null=True, on_delete=models.CASCADE)
    position = models.IntegerField(default=0)

    class Meta:
        db_table = 'milestone'

    def __str__(self):
        return self.project.project_name + " - " + self.milestone_name

    def get_name(self):
        return self.milestone_name

    @classmethod
    def get_all_status_categories(cls):
        return cls.objects.all().values_list('status', flat=True).distinct()

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import MilestoneWriteSerializer
        action = 'milestone_created'
        # calculate project status
        from v2.project.tasks import calculate_project_status
        old_value = {}
        if not self.pk:
            try:
                milestones = Milestone.objects.filter(project=self.project).order_by('position').reverse()
                if milestones.count() > 0:
                    last_milestone = milestones[0]
                    self.position = last_milestone.position + 1
                else:
                    self.position = 1
            except ObjectDoesNotExist as ex:
                self.position = 1
        old_position = 0
        new_position = 0
        if self.pk:
            action = 'milestone_edited'
            old_obj = Milestone.objects.get(pk=self.pk)
            old_value = MilestoneWriteSerializer(old_obj).data
            old_position = old_obj.position
            new_position = self.position
        super(Milestone, self).save(*args, **kwargs)
        if not self.pk or old_position == new_position:
            MilestoneActivityLog.objects.create(
                object_id=self.pk,
                milestone=self,
                action=action,
                new_value=MilestoneWriteSerializer(self).data,
                old_value=old_value
            )
            calculate_project_status.apply_async(kwargs={"project_id": self.project.id})

    def delete(self):
        # Overriding the delete method to create activity log.
        from v2.project.serializers import MilestoneWriteSerializer
        action = 'milestone_deleted'
        # calculate project status
        from v2.project.tasks import calculate_project_status

        old_obj = Milestone.objects.get(pk=self.pk)
        old_value = MilestoneWriteSerializer(old_obj).data
        MilestoneActivityLog.objects.create(
            object_id=self.pk,
            milestone=self,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(Milestone, self).delete()
        calculate_project_status.apply_async(kwargs={"project_id": self.project.id})


"""
   This model will be used To store the milestone notes.
"""
class MilestoneNote(AuditFields):
    milestone = models.ForeignKey(Milestone, on_delete=models.CASCADE)
    note = models.TextField()
    is_read = models.BooleanField(default=False)

    def __str__(self):
        return self.milestone.milestone_name

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import MilestoneNoteSerializer
        action = 'milestone_note_created'
        old_value = {}
        if self.pk:
            action = 'milestone_note_edited'
            old_obj = MilestoneNote.objects.get(pk=self.pk)
            old_value = MilestoneNoteSerializer(old_obj).data
        super(MilestoneNote, self).save(self.created_date, *args, **kwargs)
        MilestoneActivityLog.objects.create(
            object_id=self.pk,
            milestone=self.milestone,
            action=action,
            new_value=MilestoneNoteSerializer(self).data,
            old_value=old_value
        )

    def delete(self):
        # Overriding the delete method to create activity log.
        from v2.project.serializers import MilestoneNoteSerializer
        action = 'milestone_note_deleted'

        old_obj = MilestoneNote.objects.get(pk=self.pk)
        old_value = MilestoneNoteSerializer(old_obj).data
        MilestoneActivityLog.objects.create(
            object_id=self.pk,
            milestone=self.milestone,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(MilestoneNote, self).delete()


"""
   This model will be used To store the milestone documents.
"""
class MilestoneDocument(AuditFields):
    milestone = models.ForeignKey(Milestone, on_delete=models.CASCADE)
    document_file = models.FileField(max_length=10000, validators=[validate_file_extension])
    doc_size = models.BigIntegerField(null=True, blank=True)
    document_tag = JSONField(default=[], null=True, blank=True)
    path = models.TextField(default='')

    def __str__(self):
        return self.milestone.milestone_name

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import MilestoneDocumentSerializer
        action = 'document_added'
        old_value = {}
        if self.pk:
            action = 'document_edited'
            old_obj = MilestoneDocument.objects.get(pk=self.pk)
            old_value = MilestoneDocumentSerializer(old_obj).data
        if not self.pk:
            if self.milestone.project.plant:
                self.document_file.name = 'file_system/{0}/{1}/{2}/'.format(self.milestone.project.client.client_name,
                                                                            self.milestone.project.plant.plant_name,
                                                                            self.milestone.project.project_name) + self.document_file.name
            else:
                self.document_file.name = 'file_system/{0}/{1}/'.format(self.milestone.project.client.client_name,
                                                                        self.milestone.project.project_name) + self.document_file.name
        super(MilestoneDocument, self).save(*args, **kwargs)
        MilestoneActivityLog.objects.create(
            object_id=self.pk,
            milestone=self.milestone,
            action=action,
            new_value=MilestoneDocumentSerializer(self).data,
            old_value=old_value
        )

    def delete(self):
        # Overriding the delete method to create activity log.
        from v2.project.serializers import MilestoneDocumentSerializer
        action = 'document_deleted'

        old_obj = MilestoneDocument.objects.get(pk=self.pk)
        old_value = MilestoneDocumentSerializer(old_obj).data
        MilestoneActivityLog.objects.create(
            object_id=self.pk,
            milestone=self.milestone,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(MilestoneDocument, self).delete()


"""
   This model will be used To store the task.
"""
class Task(AuditFields):
    task_name = models.CharField(max_length=511)
    task_description = models.TextField(default='', blank=True)
    priority = models.CharField(max_length=255)
    status = models.CharField(max_length=63)
    start_date = models.DateTimeField(default=datetime.datetime.now)
    end_date = models.DateTimeField(default=datetime.datetime.now)
    actual_start_date = models.DateTimeField(null=True, blank=True)
    actual_end_date = models.DateTimeField(null=True, blank=True)
    task_tag = models.CharField(max_length=255, null=True, blank=True)
    tags = JSONField(default=[], null=True, blank=True)
    milestone = models.ForeignKey(Milestone, null=True, on_delete=models.SET_NULL)
    client = models.ForeignKey(Client, null=True, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, null=True, on_delete=models.CASCADE)
    participants = models.ManyToManyField(ProjectUser, null=True, blank=True)
    assignee = models.ForeignKey(ProjectUser, null=True, blank=True, on_delete=models.CASCADE,
                                 related_name="task_assignee")
    client_assignee = models.ForeignKey(ClientUser, null=True, blank=True, on_delete=models.CASCADE,
                                        related_name="client_task_assignee")
    percentage_completed = models.FloatField(default=0, blank=True, null=True)
    position = models.IntegerField(default=0)

    def __str__(self):
        return self.task_name

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import TaskWriteSerializer
        # calculate project status
        from v2.project.tasks import calculate_project_status
        action = 'task_created'
        old_value = {}
        if not self.pk:
            try:
                if self.milestone:
                    tasks = Task.objects.filter(milestone=self.milestone).order_by('position').reverse()
                    if tasks.count() > 0:
                        last_task = tasks[0]
                        self.position = last_task.position + 1
                    else:
                        self.position = 1
                elif self.project:
                    tasks = Task.objects.filter(project=self.project, milestone__isnull=True).order_by('position').reverse()
                    if tasks.count() > 0:
                        last_task = tasks[0]
                        self.position = last_task.position + 1
                    else:
                        self.position = 1
            except ObjectDoesNotExist as ex:
                self.position = 1
        old_position = 0
        new_position = 0
        if self.pk:
            action = 'task_edited'
            old_obj = Task.objects.get(pk=self.pk)
            old_value = TaskWriteSerializer(old_obj).data
            old_position = old_obj.position
            new_position = self.position

        super(Task, self).save(*args, **kwargs)
        if not self.pk or old_position == new_position:
            if self.project:
                calculate_project_status.apply_async(kwargs={"project_id": self.project.id})
            TaskActivityLog.objects.create(
                object_id=self.pk,
                task=self,
                action=action,
                new_value=TaskWriteSerializer(self).data,
                old_value=old_value
            )

    def delete(self):
        # Overriding the delete method to create activity log.
        from v2.project.serializers import TaskWriteSerializer
        # calculate project status
        from v2.project.tasks import calculate_project_status
        action = 'task_deleted'
        old_obj = Task.objects.get(pk=self.pk)
        old_value = TaskWriteSerializer(old_obj).data
        TaskActivityLog.objects.create(
            object_id=self.pk,
            task=self,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(Task, self).delete()
        if self.project:
            calculate_project_status.apply_async(kwargs={"project_id": self.project.id})

    # def save(self, *args, **kwargs):
    #     adding = self._state.adding
    #     if not self.project is None:
    #         self.client = self.project.get_client()
    #     super(Task, self).save(*args, **kwargs)
    #     project_name = self.project.project_name if not self.project is None else ""
    #     if adding:
    #         notification_type = notification_choices.TYPE_TASK_CREATED
    #         notification_title = "New Task Created"
    #         notification_text = "A new Task: {} has been created in the project {}". \
    #             format(self.task_name, project_name)
    #     else:
    #         notification_type = notification_choices.TYPE_TASK_UPDATED
    #         notification_title = "Task Updated"
    #         notification_text = "A Task: {} has been updated in the project {}". \
    #             format(self.task_name, project_name)
    #     project_users_list = [user.user for user in self.project.get_all_project_users()] if len(project_name) else []
    #     action_user = CurrentUser.get_current_user() if not CurrentUser.get_current_user().is_anonymous else None
    #     Notification.create(notification_type, category=1, action_user=action_user, action_performed_on_user=None,
    #                         action_params={"task_id": self.id},
    #                         users_to_send_to=project_users_list, title=notification_title,
    #                         text=notification_text, notification_tag="Milestone")


"""
   This model will be used To store the task comments.
"""
class TaskComment(AuditFields):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    comment = models.TextField()
    is_read = models.BooleanField(default=False)

    def __str__(self):
        return self.task.task_name + " - " + str(self.id)

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import TaskCommentSerializer
        action = 'task_comment_created'
        old_value = {}
        if self.pk:
            action = 'task_comment_edited'
            old_obj = TaskComment.objects.get(pk=self.pk)
            old_value = TaskCommentSerializer(old_obj).data
        super(TaskComment, self).save(self.created_date, *args, **kwargs)
        TaskActivityLog.objects.create(
            object_id=self.pk,
            task=self.task,
            action=action,
            new_value=TaskCommentSerializer(self).data,
            old_value=old_value
        )

    def delete(self):
        # Overriding the delete method to create activity log.
        from v2.project.serializers import TaskCommentSerializer
        action = 'task_comment_deleted'
        old_obj = TaskComment.objects.get(pk=self.pk)
        old_value = TaskCommentSerializer(old_obj).data
        TaskActivityLog.objects.create(
            object_id=self.pk,
            task=self.task,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(TaskComment, self).delete()

"""
   This model will be used To store the sub task.
"""
class SubTask(AuditFields):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    sub_task_description = models.TextField()
    participants = models.ManyToManyField(ProjectUser)

    def __str__(self):
        return self.task.task_name + " - " + str(self.id)


    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import SubTaskSerializer
        action = 'sub_task_created'
        old_value = {}
        if self.pk:
            action = 'sub_task_edited'
            old_obj = SubTask.objects.get(pk=self.pk)
            old_value = SubTaskSerializer(old_obj).data
        super(SubTask, self).save(*args, **kwargs)
        TaskActivityLog.objects.create(
            object_id=self.pk,
            task=self.task,
            action=action,
            new_value=SubTaskSerializer(self).data,
            old_value=old_value
        )

    def delete(self):
        # Overriding the delete method to create activity log.
        from v2.project.serializers import SubTaskSerializer
        action = 'sub_task_deleted'
        old_obj = SubTask.objects.get(pk=self.pk)
        old_value = SubTaskSerializer(old_obj).data
        TaskActivityLog.objects.create(
            object_id=self.pk,
            task=self.task,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(SubTask, self).delete()


"""
   This model will be used To store the task documents.
"""
class TaskDocument(AuditFields):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    document_file = models.FileField(max_length=10000, validators=[validate_file_extension])
    doc_size = models.BigIntegerField(null=True, blank=True)
    document_tag = JSONField(default=[], null=True, blank=True)
    path = models.TextField(default='')

    def __str__(self):
        return self.task.task_name + " - " + str(self.id)

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import TaskDocumentSerializer
        action = 'document_added'
        old_value = {}
        if self.pk:
            action = 'document_edited'
            old_obj = TaskDocument.objects.get(pk=self.pk)
            old_value = TaskDocumentSerializer(old_obj).data

        if not self.pk:
            if self.task.project and self.task.project.plant:
                self.document_file.name = 'file_system/{0}/{1}/{2}/tasks/'.format(self.task.project.client.client_name,
                                                                                  self.task.project.plant.plant_name,
                                                                                  self.task.project.project_name) + self.document_file.name
            elif self.task.project:
                self.document_file.name = 'file_system/{0}/{1}/tasks/'.format(self.task.project.client.client_name,
                                                                              self.task.project.project_name) + self.document_file.name
            else:
                self.document_file.name = 'file_system/{0}/tasks/'.format(
                    self.task.client.client_name) + self.document_file.name
        super(TaskDocument, self).save(*args, **kwargs)
        TaskActivityLog.objects.create(
            object_id=self.pk,
            task=self.task,
            action=action,
            new_value=TaskDocumentSerializer(self).data,
            old_value=old_value
        )

    def delete(self):
        # Overriding the delete method to create activity log.
        from v2.project.serializers import TaskDocumentSerializer
        action = 'document_deleted'
        old_obj = TaskDocument.objects.get(pk=self.pk)
        old_value = TaskDocumentSerializer(old_obj).data
        TaskActivityLog.objects.create(
            object_id=self.pk,
            task=self.task,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(TaskDocument, self).delete()


"""
   This Model is used to store the activity log for Task, Task Note, Task Document and SubTask.
"""
class TaskActivityLog(AuditFields):
    object_id = models.CharField(max_length=191, null=True)
    task = models.ForeignKey(Task, on_delete=models.SET_NULL, null=True)
    old_value = JSONField(default={})
    new_value = JSONField(default={})
    action = models.CharField(max_length=127)
    update_text = models.TextField(null=True, blank=True)


"""
   This Model is used to store the activity log for Milestone, Milestone Comment, Milestone Document.
"""
class MilestoneActivityLog(AuditFields):
    object_id = models.CharField(max_length=191, null=True)
    milestone = models.ForeignKey(Milestone, on_delete=models.SET_NULL, null=True)
    old_value = JSONField(default={})
    new_value = JSONField(default={})
    action = models.CharField(max_length=127)
    update_text = models.TextField(null=True, blank=True)


"""
   This Model is used to store the disbursement.
"""
class Disbursement(AuditFields):
    DISBURSEMENT_STATUS = (
        ('REQUEST_PENDING', 'REQUEST_PENDING'),
        ('SCHEDULED', 'SCHEDULED'),
        ('NEW_REQUEST', 'NEW_REQUEST'),
        ('VERIFIED', 'VERIFIED'),
        ('APPROVED', 'APPROVED'),
        ('APPROVAL_REQUESTED', 'APPROVAL_REQUESTED'),
        ('APPROVED', 'APPROVED'),
        ('SATTVA_APPROVED', 'SATTVA_APPROVED'),
        ('CLIENT_APPROVED', 'CLIENT_APPROVED'),
        ('REJECTED', 'REJECTED'),
        ('PAYMENT_RELEASED', 'PAYMENT_RELEASED'),
        ('PAYMENT_RECEIVED', 'PAYMENT_RECEIVED'))
    disbursement_name = models.CharField(max_length=1023)
    disbursement_description = models.TextField()
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    milestone = models.ForeignKey(Milestone, null=True, on_delete=models.SET_NULL, blank=True)
    start_date = models.DateTimeField(default=datetime.datetime.now)
    end_date = models.DateTimeField(default=datetime.datetime.now)
    invoice_request_date = models.DateTimeField(null=True, blank=True)
    expected_date = models.DateTimeField(null=True, blank=True)
    actual_date = models.DateTimeField(null=True, blank=True)
    approval_date = models.DateTimeField(null=True, blank=True)
    status = models.CharField(max_length=63, choices=DISBURSEMENT_STATUS)
    participants = models.ManyToManyField(ProjectUser, null=True, blank=True)
    expected_amount = models.FloatField()
    actual_amount = models.FloatField(default=0, null=True, blank=True)
    expense_category = models.CharField(max_length=511, null=True, blank=True)
    sattva_approver = models.ForeignKey(ProjectUser, null=True, blank=True, on_delete=models.SET_NULL,
                                        related_name='sattva_approver')
    client_approver = models.ForeignKey(ProjectUser, null=True, blank=True, on_delete=models.SET_NULL,
                                        related_name='client_approver')
    due_diligence = models.BooleanField(default=False)
    # TODO: Remove off if not required
    sattva_approval_required = models.BooleanField(default=False)
    sattva_approval_status = models.BooleanField(default=False)
    tags = JSONField(default=[], null=True, blank=True)
    client_approval_status = models.BooleanField(default=False)
    is_custom_workflow = models.BooleanField(default=False)
    previous_utilisation_check = models.BooleanField(default=False)
    position = models.IntegerField(default=0)

    def __str__(self):
        return self.disbursement_name

    def get_all_utilisations(self):
        # Returns all utilisations linked to the disbursement
        return Utilisation.objects.filter(disbursement=self)

    def get_overall_utilistation_total(self):
        # returns sum of actual_cost of all the utilisations linked to it.
        return list(self.get_all_utilisations().aggregate(Sum('actual_cost')).values())[0] or 0

    @classmethod
    def get_all_categories(cls):
        # Returns various expense categories in the disbursement
        return cls.objects.all().values_list('expense_category', flat=True).distinct()

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import DisbursementWriteSerializer
        from v2.project.tasks import calculate_total_and_planned_disbursement_amount
        action = 'disbursement_created'
        old_value = {}
        if not self.pk:
            try:
                disbursements = Disbursement.objects.filter(project=self.project).order_by('position').reverse()
                if disbursements.count() > 0:
                    last_disbursement = disbursements[0]
                    self.position = last_disbursement.position + 1
                else:
                    self.position = 1
            except ObjectDoesNotExist as ex:
                self.position = 1
        old_position = 0
        new_position = 0
        if self.pk:
            action = 'disbursement_edited'
            old_obj = Disbursement.objects.get(pk=self.pk)
            old_value = DisbursementWriteSerializer(old_obj).data
            old_position = old_obj.position
            new_position = self.position
        super(Disbursement, self).save(*args, **kwargs)
        if not self.pk or old_position == new_position:
            # Creating disbursement log
            DisbursementActivityLog.objects.create(
                object_id=self.pk,
                disbursement=self,
                action=action,
                new_value=DisbursementWriteSerializer(self).data,
                old_value=old_value
            )
            calculate_total_and_planned_disbursement_amount.apply_async(kwargs={"project_id": self.project.id})


    def get_current_status(disbursement):
        # Returning current status of the disbursement in case custom workflow is on for it.
        try:
            status = DisbursementStatus.objects.filter(disbursement=disbursement).order_by('id').reverse()[0]
            if status.status in ['SCHEDULED', 'PAYMENT_RELEASED', 'PAYMENT_RECEIVED']:
                return {'status': status.status, 'status_text': status.status.replace('_', ' ').title()}
            elif status.status in ['REQUESTED']:
                return {'status': status.status, 'status_text': 'New Request'}
            elif status.rule:
                same_category_rules = DisbursementWorkflowRule.objects.filter(ruleset=status.rule.ruleset,
                                                                              rule_type=status.rule.rule_type).order_by(
                    'level')
                category_level = 0
                for cat_rule in same_category_rules:
                    category_level += 1
                    if cat_rule.id == status.rule.id:
                        break
                if status.status == 'APPROVED':
                    if status.rule.rule_type == 'CHECKER':
                        status_text = 'L{0} - Verified'.format(category_level)
                    else:
                        status_text = 'L{0} - Approved'.format(category_level)
                else:
                    if status.rule.rule_type == 'CHECKER':
                        status_text = 'L{0} - Verification Rejected'.format(category_level)
                    else:
                        status_text = 'L{0} - Approval Rejected'.format(category_level)
                return {'status': status.status, 'rule_type': status.rule.rule_type, 'level': category_level,
                        'status_text': status_text}
            else:
                return None
        except IndexError:
            return None

    def get_next_rule(self):
        # If the custom workflow is ON, it will return the next rule of the workflow
        from v2.project.serializers import DisbursementWorkflowRuleSerializer
        try:
            ruleset = DisbursementWorkflowRuleSet.objects.get(projects__in=[self.project])
            rule_list = ruleset.get_rules()
            last_rule = rule_list.reverse()[0] if rule_list else None
            if not self.due_diligence:
                rule_list = rule_list.exclude(rule_type='CHECKER')
            status = DisbursementStatus.objects.filter(disbursement=self).order_by('id').reverse()[0]
            if status.status == 'SCHEDULED':
                return DisbursementWorkflowRuleSerializer(rule_list[0]).data
            elif status.rule and status.status != 'REJECTED' and status.rule != last_rule:
                rule = rule_list.filter(level__gt=status.rule.level).order_by('level')[0]
                return DisbursementWorkflowRuleSerializer(rule).data
            elif status.rule and status.status == 'REJECTED':
                return DisbursementWorkflowRuleSerializer(status.rule).data
            elif status.rule and status.rule == last_rule:
                return {'next_status': 'PAYMENT_RELEASED'}
            elif status.status == 'PAYMENT_RELEASED':
                return {'next_status': 'PAYMENT_RECEIVED'}
        except IndexError:
            return None

    def get_requestor_rule(self):
        # returns the requestor rule if the workflow is ON
        try:
            ruleset = DisbursementWorkflowRuleSet.objects.get(projects__in=[self.project])
            rule_list = ruleset.get_rules()
            first_rule = rule_list[0] if rule_list else None
            return first_rule
        except Exception as ex:
            return None

    def get_final_approver_rule(self):
        # returns the last rule of the approver if the workflow is ON
        try:
            ruleset = DisbursementWorkflowRuleSet.objects.get(projects__in=[self.project])
            rule_list = ruleset.get_rules()
            last_rule = rule_list.reverse()[0] if rule_list else None
            status = DisbursementStatus.objects.get(disbursement=self, status='APPROVED', rule=last_rule)
            return status.rule
        except Exception as ex:
            return None

    def get_final_approval_date(self):
        # returns approval date of the disbursement if the workflow is on
        try:
            ruleset = DisbursementWorkflowRuleSet.objects.get(projects__in=[self.project])
            rule_list = ruleset.get_rules()
            last_rule = rule_list.reverse()[0] if rule_list else None
            status = DisbursementStatus.objects.get(disbursement=self, status='APPROVED', rule=last_rule)
            return status.created_date
        except Exception as ex:
            return None

    def delete(self):
        # Overriding the delete method to create activity log.
        from v2.project.serializers import DisbursementSerializer
        from v2.project.tasks import calculate_total_and_planned_disbursement_amount
        action = 'disbursement_deleted'
        old_obj = Disbursement.objects.get(pk=self.pk)
        old_value = DisbursementSerializer(old_obj).data
        DisbursementActivityLog.objects.create(
            object_id=self.pk,
            disbursement=self,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(Disbursement, self).delete()
        calculate_total_and_planned_disbursement_amount.apply_async(kwargs={"project_id": self.project.id})


"""
   This Model is used to store the disbursement comment.
"""
class DisbursementComment(AuditFields):
    disbursement = models.ForeignKey(Disbursement, on_delete=models.CASCADE)
    comment = models.TextField()
    is_read = models.BooleanField(default=False)

    def __str__(self):
        return self.disbursement.disbursement_name + " - " + str(self.id)

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import DisbursementCommentSerializer
        action = 'comment_added'
        old_value = {}
        if self.pk:
            action = 'comment_edited'
            old_obj = DisbursementComment.objects.get(pk=self.pk)
            old_value = DisbursementCommentSerializer(old_obj).data
        super(DisbursementComment, self).save(*args, **kwargs)
        DisbursementActivityLog.objects.create(
            object_id=self.pk,
            disbursement=self.disbursement,
            action=action,
            new_value=DisbursementCommentSerializer(self).data,
            old_value=old_value
        )

    def delete(self):
        # Overriding the delete method to create activity log
        from v2.project.serializers import DisbursementCommentSerializer
        action = 'comment_deleted'
        old_obj = DisbursementComment.objects.get(pk=self.pk)
        old_value = DisbursementCommentSerializer(old_obj).data
        DisbursementActivityLog.objects.create(
            object_id=self.pk,
            disbursement=self.disbursement,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(DisbursementComment, self).delete()


"""
   This Model is used to store the disbursement document.
"""
class DisbursementDocument(AuditFields):
    disbursement = models.ForeignKey(Disbursement, on_delete=models.CASCADE)
    document_file = models.FileField(max_length=10000, validators=[validate_file_extension])
    doc_size = models.BigIntegerField(null=True, blank=True)
    document_tag = JSONField(default=[], null=True, blank=True)
    path = models.TextField(default='')

    def __str__(self):
        return self.disbursement.disbursement_name + " - " + str(self.id)

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log and the save the document in proper folder in S3
        from v2.project.serializers import DisbursementDocumentSerializer
        action = 'document_added'
        old_value = {}
        if self.pk:
            action = 'document_edited'
            old_obj = DisbursementDocument.objects.get(pk=self.pk)
            old_value = DisbursementDocumentSerializer(old_obj).data
        if not self.pk:
            if self.disbursement.project.plant:
                self.document_file.name = 'file_system/{0}/{1}/{2}/'.format(
                    self.disbursement.project.client.client_name,
                    self.disbursement.project.plant.plant_name,
                    self.disbursement.project.project_name) + self.document_file.name
            else:
                self.document_file.name = 'file_system/{0}/{1}/'.format(self.disbursement.project.client.client_name,
                                                                        self.disbursement.project.project_name) + self.document_file.name
        super(DisbursementDocument, self).save(*args, **kwargs)
        DisbursementActivityLog.objects.create(
            object_id=self.pk,
            disbursement=self.disbursement,
            action=action,
            new_value=DisbursementDocumentSerializer(self).data,
            old_value=old_value
        )

    def delete(self):
        # Overriding the delete method to create activity log
        from v2.project.serializers import DisbursementDocumentSerializer
        action = 'document_deleted'
        old_obj = DisbursementDocument.objects.get(pk=self.pk)
        old_value = DisbursementDocumentSerializer(old_obj).data
        DisbursementActivityLog.objects.create(
            object_id=self.pk,
            disbursement=self.disbursement,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(DisbursementDocument, self).delete()


"""
   This Model is used to store the utilisation.
"""
class Utilisation(AuditFields):
    unit_cost = models.FloatField(blank=True, null=True)
    number_of_units = models.FloatField(blank=True, null=True)
    planned_cost = models.FloatField(blank=True, null=True)
    avg_unit = models.FloatField(blank=True, null=True)
    avg_unit_cost = models.FloatField(blank=True, null=True)
    total_estimate_cost = models.FloatField(blank=True, null=True)
    expense_category = models.CharField(max_length=511, blank=True, null=True)
    description = models.TextField(null=True, blank=True)
    particulars = models.TextField()
    actual_cost = models.FloatField(blank=True, null=True)
    disbursement = models.ForeignKey(Disbursement, on_delete=models.SET_NULL, null=True, blank=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True, blank=True)
    start_date = models.DateTimeField(default=datetime.datetime.now)
    end_date = models.DateTimeField(default=datetime.datetime.now)
    is_custom_workflow = models.BooleanField(default=False)
    previous_utilisation_check = models.BooleanField(default=False)
    tags = JSONField(default=[], null=True, blank=True)
    position = models.IntegerField(default=0)

    def get_current_status(self):
        # Returns the current status of the utilisation if the workflow is ON
        try:
            status = UtilisationStatus.objects.filter(utilisation=self).order_by('id').reverse()[0]
            if status.status in ['NEW_REQUEST']:
                return {'status': status.status, 'status_text': status.status.replace('_', ' ').title()}
            elif status.rule:
                same_category_rules = UtilisationWorkflowRule.objects.filter(ruleset=status.rule.ruleset,
                                                                             rule_type=status.rule.rule_type).order_by(
                    'level')
                category_level = 0
                for cat_rule in same_category_rules:
                    category_level += 1
                    if cat_rule.id == status.rule.id:
                        break
                if status.status == 'APPROVED':
                    status_text = 'L{0} - Verified'.format(category_level)
                else:
                    status_text = 'L{0} - Verification Rejected'.format(category_level)
                return {'status': status.status, 'rule_type': status.rule.rule_type, 'level': category_level,
                        'status_text': status_text}
            else:
                return None
        except IndexError:
            return None

    def get_next_rule(self, utilisation):
        # Returns the next rule if the utilisation if the workflow is ON
        from v2.project.serializers import UtilisationWorkflowRuleSerializer
        try:
            ruleset = UtilisationWorkflowRuleSet.objects.get(projects__in=[self.project])
            rule_list = ruleset.get_rules()
            last_rule = rule_list.reverse()[0] if rule_list else None
            status = UtilisationStatus.objects.filter(utilisation=utilisation).order_by('id').reverse()[0]
            if status.status == 'SCHEDULED':
                return UtilisationWorkflowRuleSerializer(rule_list[0]).data
            elif status.rule and status.status != 'REJECTED' and status.rule != last_rule:
                rule = rule_list.filter(level__gt=status.rule.level).order_by('level')[0]
                return UtilisationWorkflowRuleSerializer(rule).data
            elif status.rule and status.status == 'REJECTED':
                return UtilisationWorkflowRuleSerializer(status.rule).data
            elif status.rule and status.rule == last_rule:
                return {'next_status': None}
        except IndexError:
            return None

    def __str__(self):
        return self.particulars

    @classmethod
    def get_all_categories(cls):
        # Returns all expense categories being used in Utilisation
        return cls.objects.all().values_list('expense_category', flat=True).distinct()

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import UtilisationSerializer
        from v2.project.tasks import calculate_total_and_planned_disbursement_amount
        action = 'utilisation_created'
        old_value = {}
        if not self.pk:
            try:
                if self.disbursement:
                    utilisations = Utilisation.objects.filter(disbursement=self.disbursement).order_by('position').reverse()
                    if utilisations.count() > 0:
                        last_utilisation = utilisations[0]
                        self.position = last_utilisation.position + 1
                    else:
                        self.position = 1
                elif self.project:
                    utilisations = Utilisation.objects.filter(project=self.project, disbursement__isnull=True).order_by('position').reverse()
                    if utilisations.count() > 0:
                        last_utilisation = utilisations[0]
                        self.position = last_utilisation.position + 1
                    else:
                        self.position = 1
            except ObjectDoesNotExist as ex:
                self.position = 1
        old_position = 0
        new_position = 0
        if self.pk:
            utilisation_expense = UtilisationExpense.objects.filter(utilisation=self.pk)
            if len(utilisation_expense):
                estimate_cost_sum = utilisation_expense.aggregate(sum=Sum('estimate_cost'))['sum'] or 0
                actual_cost_sum = utilisation_expense.aggregate(sum=Sum('actual_cost'))['sum'] or 0
                self.total_estimate_cost = estimate_cost_sum
                self.actual_cost = actual_cost_sum
            action = 'utilisation_edited'
            old_obj = Utilisation.objects.get(pk=self.pk)
            old_value = UtilisationSerializer(old_obj).data
            old_position = old_obj.position
            new_position = self.position
        super(Utilisation, self).save(*args, **kwargs)
        if not self.pk or old_position == new_position:
            UtilisationActivityLog.objects.create(
                object_id=self.pk,
                utilisation=self,
                action=action,
                new_value=UtilisationSerializer(self).data,
                old_value=old_value
            )
            calculate_total_and_planned_disbursement_amount.apply_async(kwargs={"project_id": self.project.id})

    def delete(self):
        # Overriding the delete method to create activity log
        from v2.project.serializers import UtilisationSerializer
        from v2.project.tasks import calculate_total_and_planned_disbursement_amount
        action = 'utilisation_deleted'
        old_obj = Utilisation.objects.get(pk=self.pk)
        old_value = UtilisationSerializer(old_obj).data
        UtilisationActivityLog.objects.create(
            object_id=self.pk,
            utilisation=self,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(Utilisation, self).delete()
        calculate_total_and_planned_disbursement_amount.apply_async(kwargs={"project_id": self.project.id})


"""
   This Model is used to store the utilisation comment.
"""
class UtilisationComment(AuditFields):
    utilisation = models.ForeignKey(Utilisation, on_delete=models.CASCADE)
    comment = models.TextField()
    is_read = models.BooleanField(default=False)

    def __str__(self):
        return self.utilisation.particulars + " - Comment " + str(self.id)

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import UtilisationCommentSerializer
        action = 'comment_added'
        old_value = {}
        if self.pk:
            action = 'comment_edited'
            old_obj = UtilisationComment.objects.get(pk=self.pk)
            old_value = UtilisationCommentSerializer(old_obj).data
        super(UtilisationComment, self).save(*args, **kwargs)
        UtilisationActivityLog.objects.create(
            object_id=self.pk,
            utilisation=self.utilisation,
            action=action,
            new_value=UtilisationCommentSerializer(self).data,
            old_value=old_value
        )

    def delete(self):
        # Overriding the delete method to create activity log
        from v2.project.serializers import UtilisationCommentSerializer
        action = 'comment_deleted'
        old_obj = UtilisationComment.objects.get(pk=self.pk)
        old_value = UtilisationCommentSerializer(old_obj).data
        UtilisationActivityLog.objects.create(
            object_id=self.pk,
            utilisation=self.utilisation,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(UtilisationComment, self).delete()


"""
   This Model is used to store the utilisation documents.
"""
class UtilisationDocument(AuditFields):
    utilisation = models.ForeignKey(Utilisation, on_delete=models.CASCADE)
    document_file = models.FileField(max_length=10000, validators=[validate_file_extension])
    doc_size = models.BigIntegerField(null=True, blank=True)
    document_tag = JSONField(default=[], null=True, blank=True)
    path = models.TextField(default='')

    def __str__(self):
        return self.utilisation.particulars + " - " + str(self.id)

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log and to save document in proper folder in S3
        from v2.project.serializers import UtilisationDocumentSerializer
        action = 'document_added'
        old_value = {}
        if self.pk:
            action = 'document_edited'
            old_obj = UtilisationDocument.objects.get(pk=self.pk)
            old_value = UtilisationDocumentSerializer(old_obj).data

        if not self.pk:
            if self.utilisation.project.plant:
                self.document_file.name = 'file_system/{0}/{1}/{2}/'.format(self.utilisation.project.client.client_name,
                                                                            self.utilisation.project.plant.plant_name,
                                                                            self.utilisation.project.project_name) + self.document_file.name
            else:
                self.document_file.name = 'file_system/{0}/{1}/'.format(self.utilisation.project.client.client_name,
                                                                        self.utilisation.project.project_name) + self.document_file.name
        super(UtilisationDocument, self).save(*args, **kwargs)
        UtilisationActivityLog.objects.create(
            object_id=self.pk,
            utilisation=self.utilisation,
            action=action,
            new_value=UtilisationDocumentSerializer(self).data,
            old_value=old_value
        )

    def delete(self):
        # Overriding the delete method to create activity log
        from v2.project.serializers import UtilisationDocumentSerializer
        action = 'document_deleted'
        old_obj = UtilisationDocument.objects.get(pk=self.pk)
        old_value = UtilisationDocumentSerializer(old_obj).data
        UtilisationActivityLog.objects.create(
            object_id=self.pk,
            utilisation=self.utilisation,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(UtilisationDocument, self).delete()


"""
   This Model is used to save UtilisationUpload data
        (Bulk upload utilisation).
"""
class UtilisationUpload(AuditFields):  # UtilisationSubEntry
    upload_name = models.CharField(max_length=255, blank=True, null=True)
    csv_file = models.FileField(max_length=2048, validators=[validate_file_extension],
                                null=True, blank=True)
    batch_id = models.CharField(max_length=63, blank=True, null=True)
    file_name = models.CharField(max_length=63, blank=True, null=True)
    upload_type = models.CharField(max_length=63, blank=True, null=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True, blank=True)
    tags = JSONField(default=[], null=True, blank=True)
    verified_by = models.ForeignKey(ProjectUser,
                                    on_delete=models.CASCADE, null=True, blank=True)
    is_verified = models.BooleanField(default=False, blank=True)

    def save(self, *args, **kwargs):
        super(UtilisationUpload, self).save(*args, **kwargs)
        if self.project:
            folder_name = 'utilisation_upload_' + str(self.id)
            if self.upload_name:
                folder_name = self.upload_name
            if self.project.plant:
                parent = 'C' + str(self.project.client.id) + "_PL" + \
                         str(self.project.plant.id) + "_PR" + \
                         str(self.project.id) + "_UTIL"
            else:
                parent = 'C' + str(self.project.client.id) + "_PL0" + "_PR" + \
                         str(self.project.id) + "_UTIL"
            CustomFolder.objects.get_or_create(
                client=self.project.client,
                folder_name=folder_name,
                parent=parent
            )


"""
   This Model is used to store Utilisation Documents.
"""
class UtilisationUploadDocument(AuditFields):
    utilisation_upload = models.ForeignKey(UtilisationUpload, on_delete=models.CASCADE)
    document_file = models.FileField(max_length=10000, validators=[validate_file_extension])
    doc_size = models.BigIntegerField(null=True, blank=True)
    document_tag = JSONField(default=[], null=True, blank=True)
    path = models.TextField(default='')
    parent = models.CharField(default='', max_length=255)

    def save(self, *args, **kwargs):
        if not self.pk:
            folder_name = 'utilisation_upload_' + str(self.utilisation_upload.id)
            if self.utilisation_upload.upload_name:
                folder_name = self.utilisation_upload.upload_name
            parent = ''
            if self.utilisation_upload.project.plant:
                self.document_file.name = 'file_system/{0}/{1}/{2}/{3}/'.format(
                    self.utilisation_upload.project.client.client_name,
                    self.utilisation_upload.project.plant.plant_name,
                    self.utilisation_upload.project.project_name, folder_name) + self.document_file.name
                parent = 'C' + str(self.utilisation_upload.project.client.id) + "_PL" + \
                         str(self.utilisation_upload.project.plant.id) + "_PR" + \
                         str(self.utilisation_upload.project.id) + "_UTIL"
            else:
                self.document_file.name = 'file_system/{0}/{1}/{2}/'.format(
                    self.utilisation_upload.project.client.client_name,
                    self.utilisation_upload.project.project_name, folder_name) + self.document_file.name
                parent = 'C' + str(self.utilisation_upload.project.client.id) + "_PL0" + "_PR" + \
                         str(self.utilisation_upload.project.id) + "_UTIL"
            custom_folder = CustomFolder.objects.get(
                client=self.utilisation_upload.project.client,
                folder_name=folder_name,
                parent=parent
            )
            self.parent = parent + '_FL' + str(custom_folder.id)
        super(UtilisationUploadDocument, self).save(*args, **kwargs)


"""
   This Model is used to save UtilisationExpense data (UtilisationSubEntry - Child Utilisation)
        (Mutiple entries for a single utilisation)
"""
class UtilisationExpense(AuditFields):
    description = models.TextField(default='', blank=True, null=True)
    split_frequency = models.CharField(max_length=31, blank=True, null=True)
    period_start_date = models.DateTimeField(default=datetime.datetime.now)
    period_end_date = models.DateTimeField(default=datetime.datetime.now)
    estimate_cost = models.FloatField(blank=True, null=True)

    number_of_units = models.FloatField(blank=True, null=True)
    unit_cost = models.FloatField(blank=True, null=True)
    actual_cost = models.FloatField(blank=True, null=True)
    planned_cost = models.FloatField(blank=True, null=True)
    remarks = models.TextField(null=True, blank=True)
    comments = models.TextField(null=True, blank=True)

    # get or create new utilization (project id, disburesement name, particulars)
    utilisation = models.ForeignKey(Utilisation,
                                    on_delete=models.CASCADE, null=True, blank=True)
    utilisation_upload = models.ForeignKey(UtilisationUpload,
                                           on_delete=models.CASCADE, null=True, blank=True)

    def save(self, *args, **kwargs):
        super(UtilisationExpense, self).save(*args, **kwargs)
        utilisation=Utilisation.objects.filter(id=self.utilisation.id).first()
        utilisation_expense = UtilisationExpense.objects.filter(utilisation=self.utilisation.id)
        if len(utilisation_expense):
            estimate_cost_sum = utilisation_expense.aggregate(sum=Sum('estimate_cost'))['sum'] or 0
            actual_cost_sum = utilisation_expense.aggregate(sum=Sum('actual_cost'))['sum'] or 0
            utilisation.total_estimate_cost = estimate_cost_sum
            utilisation.actual_cost = actual_cost_sum
            utilisation.save()



"""
   This Model is used to store Utilisation Documents.
"""
class UtilisationExpenseDocument(AuditFields):
    utilisation_expense = models.ForeignKey(UtilisationExpense, on_delete=models.CASCADE)
    document_file = models.FileField(max_length=10000, validators=[validate_file_extension])
    doc_size = models.BigIntegerField(null=True, blank=True)
    document_tag = JSONField(default=[], null=True, blank=True)
    path = models.TextField(default='')
    parent = models.CharField(default='', max_length=255)

    def __str__(self):
        return self.utilisation_expense.description + " - " + str(self.id)

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log and to save document in proper folder in S3
        from v2.project.serializers import UtilisationExpenseDocumentSerializer
        action = 'document_added'
        old_value = {}
        if self.pk:
            action = 'document_edited'
            old_obj = UtilisationDocument.objects.get(pk=self.pk)
            old_value = UtilisationExpenseDocumentSerializer(old_obj).data

        if not self.pk:
            if self.utilisation_expense.utilisation.project.plant:
                self.document_file.name = 'file_system/{0}/{1}/{2}/'.format(
                    self.utilisation_expense.utilisation.project.client.client_name,
                    self.utilisation_expense.utilisation.project.plant.plant_name,
                    self.utilisation_expense.utilisation.project.project_name) + self.document_file.name
            else:
                self.document_file.name = 'file_system/{0}/{1}/'.format(
                    self.utilisation_expense.utilisation.project.client.client_name,
                    self.utilisation_expense.utilisation.project.project_name) + self.document_file.name
        super(UtilisationExpenseDocument, self).save(*args, **kwargs)
        UtilisationActivityLog.objects.create(
            object_id=self.pk,
            utilisation=self.utilisation_expense.utilisation,
            action=action,
            new_value=UtilisationExpenseDocumentSerializer(self).data,
            old_value=old_value
        )

    def delete(self):
        # Overriding the delete method to create activity log
        from v2.project.serializers import UtilisationExpenseDocumentSerializer
        action = 'document_deleted'
        old_obj = UtilisationExpenseDocument.objects.get(pk=self.pk)
        old_value = UtilisationExpenseDocumentSerializer(old_obj).data
        UtilisationActivityLog.objects.create(
            object_id=self.pk,
            utilisation=self.utilisation_expense.utilisation,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(UtilisationExpenseDocument, self).delete()

"""
   This Model is used to store Utilisation Ruleset for the workflow
    A ruleset is a collection of rules to be applied for a workflow
"""
class ImpactWorkflowRuleSet(AuditFields):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    projects = models.ManyToManyField(Project, blank=True)
    rule_name = models.CharField(max_length=511, blank=True, null=True)
    tags = JSONField(default=[], blank=True, null=True)
    enabled = models.BooleanField(default=True, blank=True)

    def get_rules(self):
        return ImpactWorkflowRule.objects.filter(ruleset=self).order_by('level')


"""
   This Model is used to store a single rule from the workflow.
"""
class ImpactWorkflowRule(AuditFields):
    RULE_TYPE = (('REQUESTER', 'REQUESTER'), ('CHECKER', 'CHECKER'))
    ruleset = models.ForeignKey(ImpactWorkflowRuleSet, on_delete=models.CASCADE)
    rule_type = models.CharField(choices=RULE_TYPE, max_length=63)
    level = models.IntegerField()
    level_name = models.CharField(max_length=511, null=True, blank=True)


"""
   This Model is used to store assignee for a rule.
"""
class ImpactWorkflowRuleAssignee(AuditFields):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    user = models.ForeignKey(ProjectUser, on_delete=models.CASCADE)
    rule = models.ForeignKey(ImpactWorkflowRule, on_delete=models.CASCADE)


"""
   This Model is used to store project indicators.
"""
class ProjectIndicator(AuditFields):
    FREQUENCY_BUCKETS = (
        ('MONTHLY', 'MONTHLY'), ('QUARTERLY', 'QUARTERLY'), ('HALF-YEARLY', 'HALF-YEARLY'), ('ANNUALLY', 'ANNUALLY'))
    CALCULATION_CHOICE = (
        ('SUM', 'SUM'), ('AVERAGE', 'AVERAGE')
    )
    STATUS_CHOICES = (
        ('Verified', 'Verified'),
        ('Unverified', 'Unverified')
    )
    frequency = models.CharField(max_length=61, choices=FREQUENCY_BUCKETS)
    milestone = models.ForeignKey(Milestone, null=True, blank=True, on_delete=models.SET_NULL)
    period_name = models.CharField(max_length=255, null=True, blank=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    output_indicator = models.CharField(max_length=255, null=True, blank=True)
    output_calculation = models.CharField(max_length=63, choices=CALCULATION_CHOICE, default='SUM')
    outcome_indicator = models.CharField(max_length=255, null=True, blank=True)
    outcome_calculation = models.CharField(max_length=63, choices=CALCULATION_CHOICE, default='SUM')
    is_custom_workflow = models.BooleanField(default=False)
    is_beneficiary = models.BooleanField(default=False)
    output_status = models.CharField(max_length=15, choices=STATUS_CHOICES, default='Unverified')
    outcome_status = models.CharField(max_length=15, choices=STATUS_CHOICES, default='Unverified')
    position = models.IntegerField(default=0)
    location = JSONField(default=[], null=True, blank=True)

    def get_planned_project_output(self):
        project_output = ProjectOutput.objects.filter(project_indicator=self.pk)
        if len(project_output) is 0:
            return 0
        sum_planned_output = 0
        count_is_included = 0
        for output in project_output:
            if output.planned_output is not None and output.is_included:
                if output.planned_output % 1 == 0:
                    sum_planned_output = sum_planned_output + round(output.planned_output)
                    count_is_included = count_is_included + 1
                else:
                    sum_planned_output = sum_planned_output + output.planned_output
                    count_is_included = count_is_included + 1
        if isinstance(sum_planned_output, int):
            if self.output_calculation == "SUM":
                return round(sum_planned_output)
            return round((sum_planned_output / count_is_included))
        if self.output_calculation == "SUM":
            return round(sum_planned_output, 2)
        return round((sum_planned_output / count_is_included), 2)

    def get_planned_project_outcome(self):
        project_outcome = ProjectOutcome.objects.filter(project_indicator=self.pk)
        if len(project_outcome) is 0:
            return 0
        sum_planned_outcome = 0
        count_is_included = 0
        for outcome in project_outcome:
            if outcome.planned_outcome is not None and outcome.is_included:
                if outcome.planned_outcome % 1 == 0:
                    sum_planned_outcome = sum_planned_outcome + round(outcome.planned_outcome)
                    count_is_included = count_is_included + 1
                else:
                    sum_planned_outcome = sum_planned_outcome + outcome.planned_outcome
                    count_is_included = count_is_included + 1
        if isinstance(sum_planned_outcome, int):
            if self.outcome_calculation == "SUM":
                return round(sum_planned_outcome)
            return round((sum_planned_outcome / count_is_included))
        if self.outcome_calculation == "SUM":
            return round(sum_planned_outcome, 2)
        return round((sum_planned_outcome / count_is_included), 2)

    def get_actual_project_output(self):
        project_output = ProjectOutput.objects.filter(project_indicator=self.pk)
        if len(project_output) is 0:
            return 0
        sum_actual_output = 0
        count_is_included = 0
        for output in project_output:
            if output.actual_output is not None and output.is_included:
                if output.actual_output % 1 == 0:
                    sum_actual_output = sum_actual_output + round(output.actual_output)
                    count_is_included = count_is_included + 1
                else:
                    sum_actual_output = sum_actual_output + output.actual_output
                    count_is_included = count_is_included + 1
        if isinstance(sum_actual_output, int):
            if self.output_calculation == "SUM":
                return round(sum_actual_output)
            return round((sum_actual_output / count_is_included))
        if self.output_calculation == "SUM":
            return round(sum_actual_output, 2)
        return round((sum_actual_output / count_is_included), 2)

    def get_actual_project_outcome(self):
        project_outcome = ProjectOutcome.objects.filter(project_indicator=self.pk)
        if len(project_outcome) is 0:
            return 0
        sum_actual_outcome = 0
        count_is_included = 0
        for outcome in project_outcome:
            if outcome.actual_outcome is not None and outcome.is_included:
                if outcome.actual_outcome % 1 == 0:
                    sum_actual_outcome = sum_actual_outcome + round(outcome.actual_outcome)
                    count_is_included = count_is_included + 1
                else:
                    sum_actual_outcome = sum_actual_outcome + outcome.actual_outcome
                    count_is_included = count_is_included + 1
        if isinstance(sum_actual_outcome, int):
            if self.outcome_calculation == "SUM":
                return round(sum_actual_outcome, 2)
            return round((sum_actual_outcome / count_is_included), 2)
        if self.outcome_calculation == "SUM":
            return round(sum_actual_outcome, 2)
        return round((sum_actual_outcome / count_is_included), 2)


    def save(self, activity_log=None, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import ProjectIndicatorSerializer
        action = 'indicator_created'
        old_value = {}
        if not self.pk:
            try:
                indicators = ProjectIndicator.objects.filter(project=self.project).order_by('position').reverse()
                if indicators.count() > 0:
                    last_indicator = indicators[0]
                    self.position = last_indicator.position + 1
                else:
                    self.position = 1
            except ObjectDoesNotExist as ex:
                self.position = 1
        old_position = 0
        new_position = 0
        if self.pk:
            action = 'indicator_edited'
            old_obj = ProjectIndicator.objects.get(pk=self.pk)
            old_value = ProjectIndicatorSerializer(old_obj).data
            old_position = old_obj.position
            new_position = self.position
        super(ProjectIndicator, self).save(*args, **kwargs)
        if not self.pk or old_position == new_position:
            if activity_log is None:
                IndicatorActivityLog.objects.create(
                    object_id=self.pk,
                    indicator=self,
                    action=action,
                    new_value=ProjectIndicatorSerializer(self).data,
                    old_value=old_value
                )

    def delete(self):
        # Overriding the delete method to create activity log
        from v2.project.serializers import ProjectIndicatorSerializer
        action = 'indicator_deleted'
        old_obj = ProjectIndicator.objects.get(pk=self.pk)
        old_value = ProjectIndicatorSerializer(old_obj).data
        IndicatorActivityLog.objects.create(
            object_id=self.pk,
            indicator=self,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(ProjectIndicator, self).delete()

"""
   This Model is used to store Activity Log for Indicator, Indicator Document, Indicator Comments and Indicator Status.
"""
class IndicatorActivityLog(AuditFields):
    object_id = models.CharField(max_length=191, null=True)
    indicator = models.ForeignKey(ProjectIndicator, on_delete=models.SET_NULL, null=True)
    old_value = JSONField(default={})
    new_value = JSONField(default={})
    action = models.CharField(max_length=127)
    update_text = models.TextField(null=True, blank=True)

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        super(IndicatorActivityLog, self).save(*args, **kwargs)

"""
   This Model is used to store Indicator Documents.
"""
class IndicatorDocument(AuditFields):
    indicator = models.ForeignKey(ProjectIndicator, on_delete=models.CASCADE)
    document_file = models.FileField(max_length=10000, validators=[validate_file_extension])
    doc_size = models.BigIntegerField(null=True, blank=True)
    document_tag = JSONField(default=[], null=True, blank=True)
    path = models.TextField(default='')


    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import IndicatorDocumentSerializer
        action = 'document_added'
        old_value = {}
        if self.pk:
            action = 'document_edited'
            old_obj = IndicatorDocument.objects.get(pk=self.pk)
            old_value = IndicatorDocumentSerializer(old_obj).data

        super(IndicatorDocument, self).save(*args, **kwargs)
        IndicatorActivityLog.objects.create(
            indicator=self.indicator,
            action=action,
            new_value=IndicatorDocumentSerializer(self).data,
            old_value=old_value
        )

    def delete(self):
        # Overriding the delete method to create activity log
        from v2.project.serializers import IndicatorDocumentSerializer
        action = 'document_deleted'
        old_obj = IndicatorDocument.objects.get(pk=self.pk)
        old_value = IndicatorDocumentSerializer(old_obj).data
        IndicatorActivityLog.objects.create(
            object_id=self.pk,
            indicator=self.indicator,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(IndicatorDocument, self).delete()

"""
   This Model is used to store Indicator Comment.
"""
class IndicatorComment(AuditFields):
    indicator = models.ForeignKey(ProjectIndicator, on_delete=models.CASCADE)
    comment = models.TextField()

    # def __str__(self):
    #     return self.utilisation.particulars + " - Comment " + str(self.id)

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import IndicatorCommentSerializer
        action = 'comment_added'
        old_value = {}
        if self.pk:
            action = 'comment_edited'
            old_obj = IndicatorComment.objects.get(pk=self.pk)
            old_value = IndicatorCommentSerializer(old_obj).data
        super(IndicatorComment, self).save(*args, **kwargs)
        IndicatorActivityLog.objects.create(
            object_id=self.pk,
            indicator=self.indicator,
            action=action,
            new_value=IndicatorCommentSerializer(self).data,
            old_value=old_value
        )

    def delete(self):
        # Overriding the delete method to create activity log
        from v2.project.serializers import IndicatorCommentSerializer
        action = 'comment_deleted'
        old_obj = IndicatorComment.objects.get(pk=self.pk)
        old_value = IndicatorCommentSerializer(old_obj).data
        IndicatorActivityLog.objects.create(
            object_id=self.pk,
            indicator=self.indicator,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(IndicatorComment, self).delete()

"""
   This Model is used to store Project Output.
"""
class ProjectOutput(AuditFields):
    project_indicator = models.ForeignKey(ProjectIndicator, on_delete=models.CASCADE)
    scheduled_date = models.DateField(null=True)
    period_end_date = models.DateField(null=True, blank=True)
    planned_output = models.FloatField(blank=True, null=True,  default=0)
    actual_output = models.FloatField(blank=True, null=True,  default=0)
    period_name = models.CharField(max_length=255, null=True, blank=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    is_included = models.BooleanField(default=True, blank=True)
    comments = models.TextField(null=True, default='', blank=True)

    def get_output_current_status(self):
        # Returns the current status of the projectOutput if the workflow is ON
        try:
            status = ProjectOutputStatus.objects.filter(projectOutput=self).order_by('id').reverse()[0]
            if status.status in ['UNVERIFIED']:
                return {'status': status.status, 'status_text': status.status.replace('_', ' ').title()}
            elif status.rule:
                same_category_rules = ImpactWorkflowRule.objects.filter(ruleset=status.rule.ruleset,
                                                                             rule_type=status.rule.rule_type).order_by(
                    'level')
                category_level = 0
                for cat_rule in same_category_rules:
                    category_level += 1
                    if cat_rule.id == status.rule.id:
                        break
                if status.status == 'APPROVED':
                    status_text = 'L{0} - Verified'.format(category_level)
                else:
                    status_text = 'L{0} - Verification Rejected'.format(category_level)
                return {'status': status.status, 'rule_type': status.rule.rule_type, 'level': category_level,
                        'status_text': status_text}
            else:
                return None
        except IndexError:
            return None

    def get_output_next_rule(self):
        # Returns the next rule if the project output if the workflow is ON
        from v2.project.serializers import ImpactWorkflowRuleSerializer
        try:
            ruleset = ImpactWorkflowRuleSet.objects.get(projects__in=[self.project])
            rule_list = ruleset.get_rules()
            last_rule = rule_list.reverse()[0] if rule_list else None
            status = ProjectOutputStatus.objects.filter(projectOutput=self).order_by('id')
            if len(status) > 0:
                status = status.reverse()[0]
                if status.rule and status.status != 'REJECTED' and status.rule != last_rule:
                    rule = rule_list.filter(level__gt=status.rule.level).order_by('level')[0]
                    return ImpactWorkflowRuleSerializer(rule).data
                elif status.rule and status.status == 'REJECTED':
                    return ImpactWorkflowRuleSerializer(status.rule).data
                elif status.rule and status.rule == last_rule:
                    return {'next_status': None}
            else:
                return None
        except ObjectDoesNotExist:
            return None

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import ProjectOutputReadSerializer
        action = 'output_created'
        old_value = {}
        if self.pk:
            action = 'output_edited'
            old_obj = ProjectOutput.objects.get(pk=self.pk)
            old_value = ProjectOutputReadSerializer(old_obj).data
        super(ProjectOutput, self).save(*args, **kwargs)
        IndicatorActivityLog.objects.create(
            object_id=self.pk,
            indicator=self.project_indicator,
            action=action,
            new_value=ProjectOutputReadSerializer(self).data,
            old_value=old_value
        )

    def delete(self):
        # Overriding the delete method to create activity log
        from v2.project.serializers import ProjectOutputReadSerializer
        action = 'output_deleted'
        old_obj = ProjectOutput.objects.get(pk=self.pk)
        old_value = ProjectOutputReadSerializer(old_obj).data
        IndicatorActivityLog.objects.create(
            object_id=self.pk,
            indicator=self.project_indicator,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(ProjectOutput, self).delete()


"""
   This Model is used to store Project Outcome.
"""
class ProjectOutcome(AuditFields):
    project_indicator = models.ForeignKey(ProjectIndicator, on_delete=models.CASCADE)
    scheduled_date = models.DateField()
    period_end_date = models.DateField(null=True, blank=True)
    planned_outcome = models.FloatField(blank=True, null=True, default=0)
    actual_outcome = models.FloatField(blank=True, null=True,  default=0)
    period_name = models.CharField(max_length=255, null=True, blank=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    is_included = models.BooleanField(default=True, blank=True)
    comments = models.TextField(null=True, default='', blank=True)

    def get_outcome_current_status(self):
        # Returns the current status of the project output if the workflow is ON
        try:
            status = ProjectOutcomeStatus.objects.filter(projectOutcome=self).order_by('id').reverse()[0]
            if status.status in ['UNVERIFIED']:
                return {'status': status.status, 'status_text': status.status.replace('_', ' ').title()}
            elif status.rule:
                same_category_rules = ImpactWorkflowRule.objects.filter(ruleset=status.rule.ruleset,
                                                                             rule_type=status.rule.rule_type).order_by(
                    'level')
                category_level = 0
                for cat_rule in same_category_rules:
                    category_level += 1
                    if cat_rule.id == status.rule.id:
                        break
                if status.status == 'APPROVED':
                    status_text = 'L{0} - Verified'.format(category_level)
                else:
                    status_text = 'L{0} - Verification Rejected'.format(category_level)
                return {'status': status.status, 'rule_type': status.rule.rule_type, 'level': category_level,
                        'status_text': status_text}
            else:
                return None
        except IndexError:
            return None

    def get_outcome_next_rule(self):
        # Returns the next rule if the project outcome if the workflow is ON
        from v2.project.serializers import ImpactWorkflowRuleSerializer
        try:
            ruleset = ImpactWorkflowRuleSet.objects.get(projects__in=[self.project])
            rule_list = ruleset.get_rules()
            last_rule = rule_list.reverse()[0] if rule_list else None
            status = ProjectOutcomeStatus.objects.filter(projectOutcome=self).order_by('id')
            if len(status) > 0:
                status = status.reverse()[0]
                if status.rule and status.status != 'REJECTED' and status.rule != last_rule:
                    rule = rule_list.filter(level__gt=status.rule.level).order_by('level')[0]
                    return ImpactWorkflowRuleSerializer(rule).data
                elif status.rule and status.status == 'REJECTED':
                    return ImpactWorkflowRuleSerializer(status.rule).data
                elif status.rule and status.rule == last_rule:
                    return {'next_status': None}
            else:
                return None
        except ObjectDoesNotExist:
            return None

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import ProjectOutcomeReadSerializer
        action = 'outcome_created'
        old_value = {}
        if self.pk:
            action = 'outcome_edited'
            old_obj = ProjectOutcome.objects.get(pk=self.pk)
            old_value = ProjectOutcomeReadSerializer(old_obj).data
        super(ProjectOutcome, self).save(*args, **kwargs)
        IndicatorActivityLog.objects.create(
            object_id=self.pk,
            indicator=self.project_indicator,
            action=action,
            new_value=ProjectOutcomeReadSerializer(self).data,
            old_value=old_value
        )

    def delete(self):
        # Overriding the delete method to create activity log
        from v2.project.serializers import ProjectOutcomeReadSerializer
        action = 'outcome_deleted'
        old_obj = ProjectOutcome.objects.get(pk=self.pk)
        old_value = ProjectOutcomeReadSerializer(old_obj).data
        IndicatorActivityLog.objects.create(
            object_id=self.pk,
            indicator=self.project_indicator,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(ProjectOutcome, self).delete()


"""
   This Model is used to store Project Output Status.
"""
class ProjectOutputStatus(AuditFields):
    PROJECT_OUTPUT_STATUS = (
        ('UNVERIFIED', 'UNVERIFIED'),
        ('APPROVED', 'APPROVED'),
        ('REJECTED', 'REJECTED'))
    projectOutput = models.ForeignKey(ProjectOutput, on_delete=models.CASCADE)
    action_by = models.ForeignKey(ProjectUser, null=True, blank=True, on_delete=models.SET_NULL)
    status = models.CharField(choices=PROJECT_OUTPUT_STATUS, max_length=63)
    rule = models.ForeignKey(ImpactWorkflowRule, null=True, blank=True, on_delete=models.CASCADE)
    comment = models.TextField(null=True, blank=True)

    def save(self, *args, **kwargs):
        from v2.project.serializers import ProjectOutputStatusSerializer
        from v2.project.tasks import calculate_output_data_status

        adding = True if not self.pk else False
        current_status = self.projectOutput.get_output_current_status()
        super(ProjectOutputStatus, self).save(*args, **kwargs)
        if self.status != 'UNVERIFIED' and adding:
            calculate_output_data_status.apply_async(kwargs={"indicator_id": self.projectOutput.project_indicator.id})
            action = 'output_status_changed'
            if self.rule:
                same_category_rules = ImpactWorkflowRule.objects.filter(ruleset=self.rule.ruleset,
                                                                             rule_type=self.rule.rule_type).order_by(
                    'level')
                category_level = 0
                for cat_rule in same_category_rules:
                    category_level += 1
                    if cat_rule.id == self.rule.id:
                        break
                if self.status == 'APPROVED':
                    status_text = 'L{0} - Verified'.format(category_level)
                else:
                    status_text = 'L{0} - Verification Rejected'.format(category_level)
                data = ProjectOutputStatusSerializer(self).data
                data['status_text'] = status_text
                IndicatorActivityLog.objects.create(
                    object_id=self.pk,
                    indicator=self.projectOutput.project_indicator,
                    action=action,
                    new_value=data,
                    old_value=current_status
                )

"""
   This Model is used to store Project Outcome Status.
"""
class ProjectOutcomeStatus(AuditFields):
    PROJECT_OUTCOME_STATUS = (
        ('UNVERIFIED', 'UNVERIFIED'),
        ('APPROVED', 'APPROVED'),
        ('REJECTED', 'REJECTED'))
    projectOutcome = models.ForeignKey(ProjectOutcome, on_delete=models.CASCADE)
    action_by = models.ForeignKey(ProjectUser, null=True, blank=True, on_delete=models.SET_NULL)
    status = models.CharField(choices=PROJECT_OUTCOME_STATUS, max_length=63)
    rule = models.ForeignKey(ImpactWorkflowRule, null=True, blank=True, on_delete=models.CASCADE)
    comment = models.TextField(null=True, blank=True)

    def save(self, *args, **kwargs):
        from v2.project.serializers import ProjectOutcomeStatusSerializer
        from v2.project.tasks import calculate_outcome_data_status

        adding = True if not self.pk else False
        current_status = self.projectOutcome.get_outcome_current_status()
        super(ProjectOutcomeStatus, self).save(*args, **kwargs)
        if self.status != 'UNVERIFIED' and adding:
            calculate_outcome_data_status.apply_async(kwargs={"indicator_id": self.projectOutcome.project_indicator.id})
            action = 'outcome_status_changed'
            if self.rule:
                same_category_rules = ImpactWorkflowRule.objects.filter(ruleset=self.rule.ruleset,
                                                                             rule_type=self.rule.rule_type).order_by(
                    'level')
                category_level = 0
                for cat_rule in same_category_rules:
                    category_level += 1
                    if cat_rule.id == self.rule.id:
                        break
                if self.status == 'APPROVED':
                    status_text = 'L{0} - Verified'.format(category_level)
                else:
                    status_text = 'L{0} - Verification Rejected'.format(category_level)
                data = ProjectOutcomeStatusSerializer(self).data
                data['status_text'] = status_text
                IndicatorActivityLog.objects.create(
                    object_id=self.pk,
                    indicator=self.projectOutcome.project_indicator,
                    action=action,
                    new_value=data,
                    old_value=current_status
                )


"""
   This Model is used to store indicator Outcome.
"""
class IndicatorOutcome(AuditFields):
    OUTCOME_TYPE = (('SHORT', 'SHORT'), ('LONG', 'LONG'))
    indicator_batch = models.CharField(max_length=61, null=True, blank=True)
    outcome = models.TextField()
    outcome_type = models.CharField(max_length=31, choices=OUTCOME_TYPE)


"""
   This Model is used to store Project Indicator Outcome.
"""
class ProjectIndicatorOutcome(AuditFields):
    OUTCOME_TYPE = (('SHORT', 'SHORT'), ('LONG', 'LONG'))
    project_indicator = models.ForeignKey(ProjectIndicator, on_delete=models.CASCADE)
    outcome = models.TextField()
    outcome_type = models.CharField(max_length=31, choices=OUTCOME_TYPE)

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import ProjectIndicatorOutcomeSerializer
        action = 'term_outcome_added'
        old_value = {}
        if self.pk:
            action = 'term_outcome_edited'
            old_obj = ProjectIndicatorOutcome.objects.get(pk=self.pk)
            # if ild_
            old_value = ProjectIndicatorOutcomeSerializer(old_obj).data
        super(ProjectIndicatorOutcome, self).save(*args, **kwargs)
        IndicatorActivityLog.objects.create(
            object_id=self.pk,
            indicator=self.project_indicator,
            action=action,
            new_value=ProjectIndicatorOutcomeSerializer(self).data,
            old_value=old_value
        )

    def delete(self):
        # Overriding the delete method to create activity log
        from v2.project.serializers import ProjectIndicatorOutcomeSerializer
        action = 'term_outcome_deleted'
        old_obj = ProjectIndicatorOutcome.objects.get(pk=self.pk)
        old_value = ProjectIndicatorOutcomeSerializer(old_obj).data
        IndicatorActivityLog.objects.create(
            object_id=self.pk,
            indicator=self.project_indicator,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(ProjectIndicatorOutcome, self).delete()


"""
   This Model is used to store Project Output Documents.
"""
class ProjectOutputDocument(AuditFields):
    project_output = models.ForeignKey(ProjectOutput, on_delete=models.CASCADE)
    document_file = models.FileField(max_length=10000, validators=[validate_file_extension])
    doc_size = models.BigIntegerField(null=True, blank=True)
    document_tag = JSONField(default=[], null=True, blank=True)
    path = models.TextField(default='')
    parent = models.CharField(default='', max_length=255)

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import ProjectOutputDocumentSerializer
        action = 'output_document_added'
        old_value = {}
        if self.pk:
            action = 'output_document_edited'
            old_obj = ProjectOutputDocument.objects.get(pk=self.pk)
            old_value = ProjectOutputDocumentSerializer(old_obj).data
        super(ProjectOutputDocument, self).save(*args, **kwargs)
        IndicatorActivityLog.objects.create(
            object_id=self.pk,
            indicator=self.project_output.project_indicator,
            action=action,
            new_value=ProjectOutputDocumentSerializer(self).data,
            old_value=old_value
        )

    def delete(self):
        # Overriding the delete method to create activity log
        from v2.project.serializers import ProjectOutputDocumentSerializer
        action = 'output_document_deleted'
        old_obj = ProjectOutputDocument.objects.get(pk=self.pk)
        old_value = ProjectOutputDocumentSerializer(old_obj).data
        IndicatorActivityLog.objects.create(
            object_id=self.pk,
            indicator=self.project_output.project_indicator,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(ProjectOutputDocument, self).delete()


"""
   This Model is used to store Project Outcome Documents.
"""
class ProjectOutcomeDocument(AuditFields):
    project_outcome = models.ForeignKey(ProjectOutcome, on_delete=models.CASCADE)
    document_file = models.FileField(max_length=10000, validators=[validate_file_extension])
    doc_size = models.BigIntegerField(null=True, blank=True)
    document_tag = JSONField(default=[], null=True, blank=True)
    path = models.TextField(default='')
    parent = models.CharField(default='', max_length=255)

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import ProjectOutcomeDocumentSerializer
        action = 'outcome_document_added'
        old_value = {}
        if self.pk:
            action = 'outcome_document_edited'
            old_obj = ProjectOutcomeDocument.objects.get(pk=self.pk)
            old_value = ProjectOutcomeDocumentSerializer(old_obj).data
        super(ProjectOutcomeDocument, self).save(*args, **kwargs)
        IndicatorActivityLog.objects.create(
            object_id=self.pk,
            indicator=self.project_outcome.project_indicator,
            action=action,
            new_value=ProjectOutcomeDocumentSerializer(self).data,
            old_value=old_value
        )

    def delete(self):
        # Overriding the delete method to create activity log
        from v2.project.serializers import ProjectOutcomeDocumentSerializer
        action = 'outcome_document_deleted'
        old_obj = ProjectOutcomeDocument.objects.get(pk=self.pk)
        old_value = ProjectOutcomeDocumentSerializer(old_obj).data
        IndicatorActivityLog.objects.create(
            object_id=self.pk,
            indicator=self.project_outcome.project_indicator,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(ProjectOutcomeDocument, self).delete()

"""
   This Model is used to store Project Remarks.
"""
class ProjectRemark(AuditFields):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    qualitative_outcome = models.TextField(null=True, default='', blank=True)
    long_term_remark = models.TextField(null=True, default='', blank=True)
    short_term_remark = models.TextField(null=True, default='', blank=True)

    def __str__(self):
        return self.project.project_name


"""
   This Model is used to store Template Type.
"""
class TemplateType(AuditFields):
    TEMPLATE_TYPE_CHOICES = (
        ('Beneficiary', 'Beneficiary'),
        ('Institution', 'Institution'),
        ('Environment', 'Environment'),
    )
    template_type_id = models.AutoField(primary_key=True)
    template_type = models.CharField(max_length=11, choices=TEMPLATE_TYPE_CHOICES, unique=True)

    def __str__(self):
        return self.template_type


"""
   This Model is used to store Template.
"""
class Template(AuditFields):
    STATUS_CHOICES = (
        ('Verified', 'Verified'),
        ('Unverified', 'Unverified')
    )
    template_name = models.CharField(max_length=511)
    focus_area = models.ForeignKey(FocusArea, null=True, on_delete=models.SET_NULL)
    template_type = models.ForeignKey(TemplateType, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.SET_NULL, null=True, blank=True)
    client = models.ForeignKey(Client, on_delete=models.SET_NULL, null=True, blank=True)
    is_added = models.BooleanField(default=True)
    is_standard = models.BooleanField(default=False)
    standardization_requested = models.BooleanField(default=False)
    table_created = models.BooleanField(default=False)
    data_status = models.CharField(max_length=15, choices=STATUS_CHOICES, default='Unverified')

    class Meta:
        unique_together = ('project', 'template_name')

    def save(self, activity_log=None, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import TemplateSerializer
        action = 'template_created'
        old_value = {}
        if self.pk:
            action = 'template_edited'
            old_obj = Template.objects.get(pk=self.pk)
            old_value = TemplateSerializer(old_obj).data
        super(Template, self).save(*args, **kwargs)
        if activity_log is None:
            ImpactDataAndTemplateActivityLog.objects.create(
                object_id=self.pk,
                template=self,
                action=action,
                new_value=TemplateSerializer(self).data,
                old_value=old_value
            )

    def get_table_name(self):
        if self.project:
            table_name = 'C' + str(self.project.client.id) + '_P' + str(self.project.id) + '_T' + str(self.id)
            return table_name.lower()
        return None

    def delete(self):
        from v2.project.serializers import TemplateSerializer
        action = 'template_deleted'
        old_obj = Template.objects.get(pk=self.pk)
        old_value = TemplateSerializer(old_obj).data
        ImpactDataAndTemplateActivityLog.objects.create(
            object_id=self.pk,
            template=self,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(Template, self).delete()


"""
   This Model is used to store Template attribute.
"""
class TemplateAttribute(AuditFields):
    entity_requirement_choices = (
        ('Mandatory', 'Mandatory'),
        ('Optional', 'Optional'),
    )
    template = models.ForeignKey(Template, on_delete=models.CASCADE)
    attribute_name = models.CharField(max_length=63)
    attribute_type = models.ForeignKey(DataType, to_field='data_type', db_column='attribute_type',
                                       on_delete=models.CASCADE)
    attribute_order = models.SmallIntegerField(null=False)
    attribute_requirement = models.CharField(max_length=9, choices=entity_requirement_choices,
                                             default='Mandatory', null=True)
    attribute_required = models.BooleanField(default=True)
    attribute_grouping = models.CharField(max_length=60)

    class Meta:
        unique_together = ('template', 'attribute_name')

    def delete(self):
        from v2.project.serializers import TemplateAttributeSerializer
        action = 'template_attribute_deleted'
        old_obj = TemplateAttribute.objects.get(pk=self.pk)
        old_value = TemplateAttributeSerializer(old_obj).data
        ImpactDataAndTemplateActivityLog.objects.create(
            object_id=self.pk,
            template=self.template,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(TemplateAttribute, self).delete()


"""
   This Model is used to store CSVUpload.
"""
class CSVUpload(AuditFields):
    STATUS_CHOICES = (
        ('Active', 'Active'),
        ('Deleted', 'Deleted')
    )
    csv_file = models.FileField(max_length=2048, validators=[validate_file_extension])
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    template = models.ForeignKey(Template, on_delete=models.CASCADE)
    status = models.CharField(max_length=8, choices=STATUS_CHOICES, default='Active')
    table_name = models.CharField(max_length=31, default='')
    batch_id = models.IntegerField(null=True, default=1, blank=True)
    upload_successful = models.BooleanField(default=True, blank=True)
    is_deleted = models.BooleanField(default=False, blank=True)
    is_custom_workflow = models.BooleanField(default=False)

    def get_impact_upload_current_status(self):
        # Returns the current status of the impact upload if the workflow is ON
        try:
            status = ImpactUploadStatus.objects.filter(impact_upload=self).order_by('id').reverse()[0]
            if status.status in ['UNVERIFIED']:
                return {'status': status.status, 'status_text': status.status.replace('_', ' ').title()}
            elif status.rule:
                same_category_rules = ImpactWorkflowRule.objects.filter(ruleset=status.rule.ruleset,
                                                                             rule_type=status.rule.rule_type).order_by(
                    'level')
                category_level = 0
                for cat_rule in same_category_rules:
                    category_level += 1
                    if cat_rule.id == status.rule.id:
                        break
                if status.status == 'APPROVED':
                    status_text = 'L{0} - Verified'.format(category_level)
                else:
                    status_text = 'L{0} - Verification Rejected'.format(category_level)
                return {'status': status.status, 'rule_type': status.rule.rule_type, 'level': category_level,
                        'status_text': status_text}
            else:
                return None
        except IndexError:
            return None

    def get_impact_upload_next_rule(self):
        # Returns the next rule if the impact upload if the workflow is ON
        from v2.project.serializers import ImpactWorkflowRuleSerializer
        try:
            ruleset = ImpactWorkflowRuleSet.objects.get(projects__in=[self.project])
            rule_list = ruleset.get_rules()
            last_rule = rule_list.reverse()[0] if rule_list else None
            status = ImpactUploadStatus.objects.filter(impact_upload=self).order_by('id').reverse()[0]
            if status.rule and status.status != 'REJECTED' and status.rule != last_rule:
                rule = rule_list.filter(level__gt=status.rule.level).order_by('level')[0]
                return ImpactWorkflowRuleSerializer(rule).data
            elif status.rule and status.status == 'REJECTED':
                return ImpactWorkflowRuleSerializer(status.rule).data
            elif status.rule and status.rule == last_rule:
                return {'next_status': None}
        except ObjectDoesNotExist:
            return None

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import CSVUploadSerializer
        if self.pk and self.upload_successful:
            action = 'data_updated'
            old_obj = CSVUpload.objects.get(pk=self.pk)
            old_value = CSVUploadSerializer(old_obj).data
            ImpactDataAndTemplateActivityLog.objects.create(
                object_id=self.pk,
                template=self.template,
                action=action,
                new_value=CSVUploadSerializer(self).data,
                old_value=old_value
            )
        super(CSVUpload, self).save(*args, **kwargs)

    def delete(self):
        from v2.project.serializers import CSVUploadSerializer
        action = 'data_deleted'
        old_obj = CSVUpload.objects.get(pk=self.pk)
        old_value = CSVUploadSerializer(old_obj).data
        ImpactDataAndTemplateActivityLog.objects.create(
            object_id=self.pk,
            template=self.template,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(CSVUpload, self).delete()


"""
   This Model is used to store Impact Upload Status.
"""
class ImpactUploadStatus(AuditFields):
    IMPACT_UPLOAD_STATUS = (
        ('UNVERIFIED', 'UNVERIFIED'),
        ('APPROVED', 'APPROVED'),
        ('REJECTED', 'REJECTED'))
    impact_upload = models.ForeignKey(CSVUpload, on_delete=models.CASCADE)
    action_by = models.ForeignKey(ProjectUser, null=True, blank=True, on_delete=models.SET_NULL)
    status = models.CharField(choices=IMPACT_UPLOAD_STATUS, max_length=63)
    rule = models.ForeignKey(ImpactWorkflowRule, null=True, blank=True, on_delete=models.CASCADE)
    comment = models.TextField(null=True, blank=True)

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import ImpactUploadStatusSerializer
        from v2.project.tasks import calculate_impact_data_status

        adding = True if not self.pk else False
        current_status = self.impact_upload.get_impact_upload_current_status()
        super(ImpactUploadStatus, self).save(*args, **kwargs)
        if self.status != 'UNVERIFIED' and adding:
            template = self.impact_upload.template
            calculate_impact_data_status.apply_async(kwargs={"template_id": template.id})
            action = 'data_status_changed'
            if self.rule:
                same_category_rules = ImpactWorkflowRule.objects.filter(ruleset=self.rule.ruleset,
                                                                             rule_type=self.rule.rule_type).order_by(
                    'level')
                category_level = 0
                for cat_rule in same_category_rules:
                    category_level += 1
                    if cat_rule.id == self.rule.id:
                        break
                if self.status == 'APPROVED':
                    status_text = 'L{0} - Verified'.format(category_level)
                else:
                    status_text = 'L{0} - Verification Rejected'.format(category_level)
                data = ImpactUploadStatusSerializer(self).data
                data['status_text'] = status_text
                data['batch_id'] = self.impact_upload.batch_id
                ImpactDataAndTemplateActivityLog.objects.create(
                    object_id=self.pk,
                    template=self.impact_upload.template,
                    action=action,
                    new_value=data,
                    old_value=current_status
                )

"""
   This Model is used to store the activity log for Impact Upload Template and Data.
"""
class ImpactDataAndTemplateActivityLog(AuditFields):
    object_id = models.CharField(max_length=191, null=True)
    template = models.ForeignKey(Template, on_delete=models.SET_NULL, null=True)
    old_value = JSONField(default={})
    new_value = JSONField(default={})
    action = models.CharField(max_length=127)
    update_text = models.TextField(null=True, blank=True)


"""
   This Model is used to store impact casestudy.
"""
class ImpactCaseStudy(AuditFields):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    title = models.CharField(max_length=256)
    description = models.TextField()
    focus_area = JSONField(default=[], null=True, blank=True)
    sub_focus_area = JSONField(default=[], null=True, blank=True)
    target_segment = JSONField(default=[], null=True, blank=True)
    sub_target_segment = JSONField(default=[], null=True, blank=True)
    location = JSONField(default=[], null=True, blank=True)
    tags = JSONField(default=[], null=True, blank=True)
    logo = models.FileField(null=True, blank=True, max_length=10000, validators=[validate_file_extension])
    is_key_case_study = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import ImpactCaseStudySerializer
        action = 'case_study_added'
        old_value = {}
        if self.pk:
            action = 'case_study_edited'
            old_obj = ImpactCaseStudy.objects.get(pk=self.pk)
            old_value = ImpactCaseStudySerializer(old_obj).data
        super(ImpactCaseStudy, self).save(*args, **kwargs)
        CaseStudyActivityLog.objects.create(
            object_id=self.pk,
            case_study=self,
            action=action,
            new_value=ImpactCaseStudySerializer(self).data,
            old_value=old_value
        )

    def delete(self):
        # Overriding the delete method to create activity log
        from v2.project.serializers import ImpactCaseStudySerializer
        action = 'case_study_deleted'
        old_obj = ImpactCaseStudy.objects.get(pk=self.pk)
        old_value = ImpactCaseStudySerializer(old_obj).data
        CaseStudyActivityLog.objects.create(
            object_id=self.pk,
            case_study=self,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(ImpactCaseStudy, self).delete()


"""
   This Model is used to store Case Study Documents.
"""
class CaseStudyDocuments(AuditFields):
    case_study = models.ForeignKey(ImpactCaseStudy, on_delete=models.CASCADE)
    document_file = models.FileField(max_length=10000, validators=[validate_file_extension])
    doc_size = models.BigIntegerField(null=True, blank=True)
    document_tag = JSONField(default=[], null=True, blank=True)
    path = models.TextField(default='')

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import CaseStudyDocumentsSerializer
        action = 'case_study_document_added'
        old_value = {}
        if self.pk:
            action = 'case_study_document_edited'
            old_obj = CaseStudyDocuments.objects.get(pk=self.pk)
            old_value = CaseStudyDocumentsSerializer(old_obj).data
        super(CaseStudyDocuments, self).save(*args, **kwargs)
        CaseStudyActivityLog.objects.create(
            object_id=self.pk,
            case_study=self.case_study,
            action=action,
            new_value=CaseStudyDocumentsSerializer(self).data,
            old_value=old_value
        )

    def delete(self):
        # Overriding the delete method to create activity log
        from v2.project.serializers import CaseStudyDocumentsSerializer
        action = 'case_study_document_deleted'
        old_obj = CaseStudyDocuments.objects.get(pk=self.pk)
        old_value = CaseStudyDocumentsSerializer(old_obj).data
        CaseStudyActivityLog.objects.create(
            object_id=self.pk,
            case_study=self.case_study,
            action=action,
            new_value={},
            old_value=old_value
        )
        super(CaseStudyDocuments, self).delete()

"""
   This Model is used to store the activity log for ImpactCaseStudy, case Study Document.
"""
class CaseStudyActivityLog(AuditFields):
    object_id = models.CharField(max_length=191, null=True)
    case_study = models.ForeignKey(ImpactCaseStudy, on_delete=models.SET_NULL, null=True)
    old_value = JSONField(default={})
    new_value = JSONField(default={})
    action = models.CharField(max_length=127)
    update_text = models.TextField(null=True, blank=True)

"""
   This Model is used to store the project dashboard.
"""
class ProjectDashboard(AuditFields):
    PROVIDER_METABASE = 0
    PROVIDER_ZOHO = 1
    PROVIDER_CHOICES = ((PROVIDER_METABASE, 'Metabase'), (PROVIDER_ZOHO, 'Zoho'))
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    provider = models.SmallIntegerField(choices=PROVIDER_CHOICES)
    dashboard_name = models.CharField(max_length=511, default='Insights')
    url = models.TextField()
    embedded_content = models.TextField()
    password = models.CharField(max_length=220, null=True, blank=True)


"""
   This Model is used to store Disbursement Workflow Ruleset
    A Ruleset is set of rules that is to be applied to a workflow.
"""
class DisbursementWorkflowRuleSet(AuditFields):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    projects = models.ManyToManyField(Project, blank=True)
    rule_name = models.CharField(max_length=511, blank=True, null=True)
    tags = JSONField(default=[], blank=True, null=True)
    enabled = models.BooleanField(default=True, blank=True)

    def get_rules(self):
        return DisbursementWorkflowRule.objects.filter(ruleset=self).order_by('level')


"""
   This Model is used to store Disbursement Workflow Rule
    A single rule from a workflow
"""
class DisbursementWorkflowRule(AuditFields):
    RULE_TYPE = (('REQUESTER', 'REQUESTER'), ('CHECKER', 'CHECKER'), ('APPROVER', 'APPROVER'))
    ruleset = models.ForeignKey(DisbursementWorkflowRuleSet, on_delete=models.CASCADE)
    rule_type = models.CharField(choices=RULE_TYPE, max_length=63)
    level = models.IntegerField()
    level_name = models.CharField(max_length=511, null=True, blank=True)

"""
    This Model is used to store assignee for a rule.
"""
class DisbursementWorkflowRuleAssignee(AuditFields):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    user = models.ForeignKey(ProjectUser, on_delete=models.CASCADE)
    rule = models.ForeignKey(DisbursementWorkflowRule, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('project', 'rule', 'user')


"""
    This Model is used to store Disbursement status in case the custom workflow is on.
"""
class DisbursementStatus(AuditFields):
    DISBURSEMENT_STATUS = (
        ('SCHEDULED', 'SCHEDULED'),
        ('REQUESTED', 'REQUESTED'),
        ('APPROVED', 'APPROVED'),
        ('REJECTED', 'REJECTED'),
        ('PAYMENT_RELEASED', 'PAYMENT_RELEASED'),
        ('PAYMENT_RECEIVED', 'PAYMENT_RECEIVED'))
    disbursement = models.ForeignKey(Disbursement, on_delete=models.CASCADE)
    action_by = models.ForeignKey(ProjectUser, null=True, blank=True, on_delete=models.SET_NULL)
    status = models.CharField(choices=DISBURSEMENT_STATUS, max_length=63)
    rule = models.ForeignKey(DisbursementWorkflowRule, null=True, blank=True, on_delete=models.CASCADE)
    comment = models.TextField(null=True, blank=True)

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import DisbursementStatusSerializer
        adding = True if not self.pk else False
        current_status = self.disbursement.get_current_status()
        super(DisbursementStatus, self).save(*args, **kwargs)
        if self.status != 'SCHEDULED' and adding:
            action = 'status_changed'
            if self.status in ['SCHEDULED', 'PAYMENT_RELEASED', 'PAYMENT_RECEIVED', 'REQUESTED']:
                status_text = self.status.replace('_', ' ').title()
            elif self.rule:
                same_category_rules = DisbursementWorkflowRule.objects.filter(ruleset=self.rule.ruleset,
                                                                              rule_type=self.rule.rule_type).order_by(
                    'level')
                category_level = 0
                for cat_rule in same_category_rules:
                    category_level += 1
                    if cat_rule.id == self.rule.id:
                        break
                if self.status == 'APPROVED':
                    if self.rule.rule_type == 'CHECKER':
                        status_text = 'L{0} - Verified'.format(category_level)
                    else:
                        status_text = 'L{0} - Approved'.format(category_level)
                else:
                    if self.rule.rule_type == 'CHECKER':
                        status_text = 'L{0} - Verification Rejected'.format(category_level)
                    else:
                        status_text = 'L{0} - Approval Rejected'.format(category_level)
                return {'status': self.status, 'rule_type': self.rule.rule_type, 'level': category_level,
                        'status_text': status_text}
            data = DisbursementStatusSerializer(self).data
            data['status_text'] = status_text
            DisbursementActivityLog.objects.create(
                object_id=self.pk,
                disbursement=self.disbursement,
                action=action,
                new_value=data,
                old_value=current_status
            )


"""
    This Model is used to store the activity log for Disbursement, Disbursement Comment, Disbursment Document and Disbursement Status.
"""
class DisbursementActivityLog(AuditFields):
    object_id = models.CharField(max_length=191, null=True)
    disbursement = models.ForeignKey(Disbursement, on_delete=models.SET_NULL, null=True)
    old_value = JSONField(default={})
    new_value = JSONField(default={})
    action = models.CharField(max_length=127)
    update_text = models.TextField(null=True, blank=True)

    def save(self, *args, **kwargs):
        from v2.project.triggers import create_notification_on_disbursement_creation, \
            create_notification_on_disbursement_update, create_notification_on_disbursement_status_change, \
            create_notification_on_disbursement_delete
        adding = True if not self.pk else False
        super(DisbursementActivityLog, self).save(*args, **kwargs)
        if adding:
            # Write the logic here to create notification.
            if self.action == 'disbursement_created':
                create_notification_on_disbursement_creation(self.disbursement, self.created_by)
            elif self.action in ['disbursement_edited', 'comment_added', 'comment_edited',
                                 'comment_deleted', 'document_added', 'document_deleted']:
                create_notification_on_disbursement_update(self.disbursement, self.created_by)
            elif self.action == 'status_changed':
                create_notification_on_disbursement_status_change(self.disbursement, self.created_by,
                                                                  self.old_value['status_text'],
                                                                  self.new_value['status_text'], self.new_value)
            elif self.action == 'disbursement_deleted':
                create_notification_on_disbursement_delete(self.disbursement, self.created_by)


"""
    This Model is used to store the Utilisation Ruleset for the workflow
    A ruleset is a collection of rules to be applied for a workflow.
"""
class UtilisationWorkflowRuleSet(AuditFields):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    projects = models.ManyToManyField(Project, blank=True)
    rule_name = models.CharField(max_length=511, blank=True, null=True)
    tags = JSONField(default=[], blank=True, null=True)
    enabled = models.BooleanField(default=True, blank=True)

    def get_rules(self):
        return UtilisationWorkflowRule.objects.filter(ruleset=self).order_by('level')

"""
    This Model is used to store a single rule from the workflow

"""
class UtilisationWorkflowRule(AuditFields):
    RULE_TYPE = (('REQUESTER', 'REQUESTER'), ('CHECKER', 'CHECKER'))
    ruleset = models.ForeignKey(UtilisationWorkflowRuleSet, on_delete=models.CASCADE)
    rule_type = models.CharField(choices=RULE_TYPE, max_length=63)
    level = models.IntegerField()
    level_name = models.CharField(max_length=511, null=True, blank=True)

"""
   This Model is used to store assignee for a rule.
"""
class UtilisationWorkflowRuleAssignee(AuditFields):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    user = models.ForeignKey(ProjectUser, on_delete=models.CASCADE)
    rule = models.ForeignKey(UtilisationWorkflowRule, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('project', 'rule', 'user')

"""
   This Model is used to store Utilisation Status
"""
class UtilisationStatus(AuditFields):
    UTILISATION_STATUS = (
        ('NEW_REQUEST', 'NEW_REQUEST'),
        ('APPROVED', 'APPROVED'),
        ('REJECTED', 'REJECTED'))
    utilisation = models.ForeignKey(Utilisation, on_delete=models.CASCADE)
    action_by = models.ForeignKey(ProjectUser, null=True, blank=True, on_delete=models.SET_NULL)
    status = models.CharField(choices=UTILISATION_STATUS, max_length=63)
    rule = models.ForeignKey(UtilisationWorkflowRule, null=True, blank=True, on_delete=models.CASCADE)
    comment = models.TextField(null=True, blank=True)

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.serializers import UtilisationStatusSerializer
        adding = True if not self.pk else False
        current_status = self.utilisation.get_current_status()
        super(UtilisationStatus, self).save(*args, **kwargs)
        if self.status != 'NEW_REQUEST' and adding:
            action = 'status_changed'
            if self.rule:
                same_category_rules = UtilisationWorkflowRule.objects.filter(ruleset=self.rule.ruleset,
                                                                             rule_type=self.rule.rule_type).order_by(
                    'level')
                category_level = 0
                for cat_rule in same_category_rules:
                    category_level += 1
                    if cat_rule.id == self.rule.id:
                        break
                if self.status == 'APPROVED':
                    status_text = 'L{0} - Verified'.format(category_level)
                else:
                    status_text = 'L{0} - Verification Rejected'.format(category_level)
                data = UtilisationStatusSerializer(self).data
                data['status_text'] = status_text
                UtilisationActivityLog.objects.create(
                    object_id=self.pk,
                    utilisation=self.utilisation,
                    action=action,
                    new_value=data,
                    old_value=current_status
                )


"""
   This Model is used to store Activity Log for Utilisation, Utilisation Document, Utilisation Comments and Utilisation Status
"""
class UtilisationActivityLog(AuditFields):
    object_id = models.CharField(max_length=191, null=True)
    utilisation = models.ForeignKey(Utilisation, on_delete=models.SET_NULL, null=True)
    old_value = JSONField(default={})
    new_value = JSONField(default={})
    action = models.CharField(max_length=127)
    update_text = models.TextField(null=True, blank=True)

    def save(self, *args, **kwargs):
        # Overriding the save method to create activity log
        from v2.project.triggers import create_notification_on_utilisation_creation, \
            create_notification_on_utilisation_update, create_notification_on_utilisation_status_change, \
            create_notification_on_utilisation_delete
        adding = True if not self.pk else False
        super(UtilisationActivityLog, self).save(*args, **kwargs)
        if adding:
            # logic to create notification.
            if self.action == 'utilisation_created':
                create_notification_on_utilisation_creation(self.utilisation, self.created_by)
            elif self.action in ['utilisation_edited', 'comment_added', 'comment_edited',
                                 'comment_deleted', 'document_added', 'document_deleted']:
                create_notification_on_utilisation_update(self.utilisation, self.created_by)
            elif self.action == 'status_changed':
                create_notification_on_utilisation_status_change(self.utilisation, self.created_by,
                                                                 self.old_value['status_text'],
                                                                 self.new_value['status_text'], self.new_value)
            elif self.action == 'utilisation_deleted':
                create_notification_on_utilisation_delete(self.utilisation, self.created_by)

"""
    This Model is used to store the settings at project level
"""
class ProjectSetting(AuditFields):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, unique=True)
    disbursement_visible_fields = JSONField(default={})
    utilisation_visible_fields = JSONField(default={})
    expense_category = JSONField(default={})
    task_visible_fields = JSONField(default={})
    milestone_visible_fields = JSONField(default={})

class BulkUpload(AuditFields):
    file_name = models.FileField(max_length=2048, validators=[validate_file_extension])
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    uploaded=models.DateTimeField(auto_now_add=True)
    template_name = models.CharField(max_length=127)
    batch_id = models.CharField(max_length=63, blank=True, null=True)
    activated=models.BooleanField(default=False)
    errors=JSONField(default={})

    def __str__(self):
        return f"File id:{self.id}"

class Bulkfiles(AuditFields):
    file_name = models.FileField(max_length=2048, validators=[validate_file_extension])
    uploaded=models.DateTimeField(auto_now_add=True)
    template_name = models.CharField(max_length=127)
    template_desc = models.CharField(max_length=1277)


"""
   This model will be used To Store the mca focus area for project.
"""
class ProjectMCA(AuditFields):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    mca_focus_area = models.CharField(max_length=255)
    mca_sub_focus_area = models.CharField(max_length=255)
    mca = JSONField(default=[], null=True)

    def __str__(self):
        return self.project.project_name

