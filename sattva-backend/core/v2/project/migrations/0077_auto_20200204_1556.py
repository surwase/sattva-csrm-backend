# Generated by Django 2.0.5 on 2020-02-04 10:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0076_auto_20200204_1137'),
    ]

    operations = [
        migrations.AddField(
            model_name='projectlocation',
            name='latitude',
            field=models.FloatField(default=0, max_length=31, null=True),
        ),
        migrations.AddField(
            model_name='projectlocation',
            name='longitude',
            field=models.FloatField(default=0, max_length=31, null=True),
        ),
    ]
