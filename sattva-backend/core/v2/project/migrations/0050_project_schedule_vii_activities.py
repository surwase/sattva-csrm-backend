# Generated by Django 2.0.5 on 2020-01-02 06:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('domainlibraries', '0014_scheduleviiactivity'),
        ('project', '0049_auto_20191227_1556'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='schedule_vii_activities',
            field=models.ManyToManyField(blank=True, null=True, to='domainlibraries.ScheduleVIIActivity'),
        ),
    ]
