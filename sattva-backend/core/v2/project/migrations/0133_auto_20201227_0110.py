# Generated by Django 2.2.16 on 2020-12-27 01:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0132_utilisationexpense_planned_cost'),
    ]

    operations = [
        migrations.AlterField(
            model_name='projectsetting',
            name='project',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='project.Project', unique=True),
        ),
    ]
