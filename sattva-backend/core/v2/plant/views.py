import django_filters
from django.http import HttpResponse
from django_filters import rest_framework as filters
from rest_framework import serializers, viewsets, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_bulk import BulkModelViewSet
import urllib.request
from io import BytesIO
import zipfile
from core.users.models import UserActivityLog
from v2.authentication import KeycloakUserUpdateAuthentication
from v2.plant.models import Plant, PlantDocument, PlantUser, PlantImage, PlantDashboard, PlantScheduleVII, \
    PlantLocation, PlantFocusArea, PlantSubFocusArea, PlantTargetSegment, PlantSubTargetSegment, PlantSDGGoals, \
    PlantMCA
from v2.plant.permissions import PlantPermission, PlantDocumentPermission, PlantUserPermission, \
    DeletePlantUserPermission, PlantDashboardPermission
from v2.project.serializers import ProjectListSerializer
from v2.plant.serializers import PlantIndicatorSerializer, PlantSerializer, PlantDocumentSerializer, PlantUserSerializer, PlantDetailSerializer, \
    PlantImageSerializer, PlantProjectListSerializer, PlantDashboardSerializer, PlantScheduleVIIActivitySerializer, \
    ListPlantsSerializer, PlantListSerializer
from v2.project.models import Project, ProjectUser
from v2.user_management.permissions import ClientFeaturePermission, ProjectFeaturePermission, User
from v2.client.models import ClientUser


"""
   Filter for Plant which will filter Plant by exact client id
"""
class PlantFilter(django_filters.FilterSet):
    class Meta:
        model = Plant
        fields = {
            'client': ['exact']
        }

"""
    Viewset for Plant allows CRUD operations
"""
class PlantModelViewSet(viewsets.ModelViewSet):
    serializer_class = PlantSerializer
    queryset = Plant.objects.all().order_by('client')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = PlantFilter
    permission_classes = [PlantPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_serializer_class(self):
        """
        This function will get the serializer class if method is GET it will return ListPlantsSerializer else it will return PlantSerializer
        """
        method = self.request.method
        if method == 'GET' and self.action == 'list':
            return ListPlantsSerializer
        else:
            return PlantSerializer

    @staticmethod
    def add_locations(locations, plant_id):
        """
        This function will fetch the existing locations and delete them and create new location for particular plant.
        """
        existing_locations = PlantLocation.objects.filter(plant__id=plant_id)
        existing_locations.delete()
        location_objects = []
        for location in locations:
            if location.get('details'):
                location_objects.append(PlantLocation(
                    location=location,
                    area=location.get('area', ''),
                    city=location.get('city', ''),
                    state=location.get('state', ''),
                    country=location.get('country', ''),
                    latitude=location.get('latitude', 0),
                    longitude=location.get('longitude', 0),
                    plant=Plant.objects.get(id=plant_id)))
        PlantLocation.objects.bulk_create(location_objects)

    @staticmethod
    def add_focus_area(focus_areas, plant_id):
        """
        This function will fetch the existing focus_area and delete them and create new focus_area for particular plant
        """
        if type(focus_areas) == str:
            focus_areas = [focus_areas]
        existing_focus_area = PlantFocusArea.objects.filter(plant__id=plant_id)
        existing_focus_area.delete()
        focus_area_objects = []
        for focus_area in focus_areas:
            focus_area_objects.append(
                PlantFocusArea(focus_area=focus_area, plant=Plant.objects.get(id=plant_id)))
        PlantFocusArea.objects.bulk_create(focus_area_objects)

    @staticmethod
    def add_sub_focus_area(sub_focus_areas, plant_id):
        """
        This function will fetch the existing sub_focus_area and delete them and create new sub focus_area for particular plant
        """
        existing_sub_focus_area = PlantSubFocusArea.objects.filter(plant__id=plant_id)
        existing_sub_focus_area.delete()
        sub_focus_area_objects = []
        for sub_focus_area in sub_focus_areas:
            sub_focus_area_objects.append(
                PlantSubFocusArea(sub_focus_area=sub_focus_area, plant=Plant.objects.get(id=plant_id)))
        PlantSubFocusArea.objects.bulk_create(sub_focus_area_objects)

    @staticmethod
    def add_target_segment(target_segments, plant_id):
        """
        This function will fetch the existing target_segment and delete them and create new target_segment for particular plant
        """
        if type(target_segments) == str:
            target_segments = [target_segments]
        existing_target_segment = PlantTargetSegment.objects.filter(plant__id=plant_id)
        existing_target_segment.delete()
        target_segment_objects = []
        for target_segment in target_segments:
            target_segment_objects.append(
                PlantTargetSegment(target_segment=target_segment, plant=Plant.objects.get(id=plant_id)))
        PlantTargetSegment.objects.bulk_create(target_segment_objects)

    @staticmethod
    def add_sub_target_segment(sub_target_segments, plant_id):
        """
        This function will fetch the existing sub_target_segment and delete them and create new sub_target_segment for particular plant
        """
        if type(sub_target_segments) == str:
            sub_target_segments = [sub_target_segments]
        existing_sub_target_segment = PlantSubTargetSegment.objects.filter(plant__id=plant_id)
        existing_sub_target_segment.delete()
        sub_target_segment_objects = []
        for sub_target_segment in sub_target_segments:
            sub_target_segment_objects.append(
                PlantSubTargetSegment(sub_target_segment=sub_target_segment,
                                      plant=Plant.objects.get(id=plant_id)))
        PlantSubTargetSegment.objects.bulk_create(sub_target_segment_objects)

    @staticmethod
    def add_sdg_goals(sdg_goals, plant_id):
        """
        This function will fetch the existing sdg_goals and delete them and create new sdg_goals for particular plant
        """
        existing_sdg_goals = PlantSDGGoals.objects.filter(plant__id=plant_id)
        existing_sdg_goals.delete()
        sub_sdg_goals_objects = []
        for sdg_goal in sdg_goals:
            sub_sdg_goals_objects.append(
                PlantSDGGoals(sdg_goal=sdg_goal,
                              sdg_goal_name=sdg_goal['name'],
                              plant=Plant.objects.get(id=plant_id)))
        PlantSDGGoals.objects.bulk_create(sub_sdg_goals_objects)

    @staticmethod
    def add_mca_focus_area(mca_focus_area, plant_id):
        """
        This function will fetch the existing mca_focus_area and delete them and create new mca_focus_area for particular plant
        """
        existing_mca = PlantMCA.objects.filter(plant__id=plant_id)
        existing_mca.delete()
        sub_mca_focus_area_objects = []
        for mca in mca_focus_area:
            sub_mca_focus_area_objects.append(
                PlantMCA(mca=mca,
                         mca_focus_area=mca['mca_focus_area_name'],
                         mca_sub_focus_area=mca['mca_sub_focus_area_name'],
                         plant=Plant.objects.get(id=plant_id)))
        PlantMCA.objects.bulk_create(sub_mca_focus_area_objects)

    def add_child_data_for_plant(self, request, serializer):
        """
        This function will create the child data for particular plant
        """
        plant_id = serializer.data['id']
        locations = request.data.get('location')
        if locations:
            self.add_locations(locations, plant_id)
        focus_area = request.data.get('focus_area')
        if focus_area:
            self.add_focus_area(focus_area, plant_id)
        sub_focus_area = request.data.get('sub_focus_area')
        if sub_focus_area:
            self.add_sub_focus_area(sub_focus_area, plant_id)
        target_segment = request.data.get('target_segment')
        if target_segment:
            self.add_target_segment(target_segment, plant_id)
        sub_target_segment = request.data.get('sub_target_segment')
        if sub_target_segment:
            self.add_sub_target_segment(sub_target_segment, plant_id)
        sdg_goals = request.data.get('sdg_goals')
        if sdg_goals:
            self.add_sdg_goals(sdg_goals, plant_id)
        mca_focus_area = request.data.get('mca_focus_area')
        if mca_focus_area:
            self.add_mca_focus_area(mca_focus_area, plant_id)

    def get_queryset(self):
        """
        This function will return the queryset
        query set will fetch all the plants and if current user's application role is not sattva-admin it will fetch the plants of the particular user.
        """
        queryset = Plant.objects.all().order_by('client')
        if self.request.user.application_role.role_name != 'sattva-admin':
            user_projects = PlantUser.objects.filter(user=self.request.user).values_list('plant__id', flat=True)
            queryset = queryset.filter(id__in=user_projects)
        return queryset

    def create(self, request, *args, **kwargs):
        """
        This function will create the plant for the particular user if current user is not sattva admin
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        self.add_child_data_for_plant(request, serializer)
        plant=Plant.objects.get(id=serializer.data.get('id'))
        client_users=ClientUser.objects.filter(client=plant.client)
        for client in client_users:
            if client.user.application_role.role_name=="sattva-user":
                if client.user!=request.user:
                  sattva_user_added_as_plant_user = PlantUser.objects.create(user=client.user,
                   plant=Plant.objects.get(id=serializer.data.get('id')))
        if request.user.application_role is not None and request.user.application_role.role_name != 'sattva-admin':
            plant_user = PlantUser.objects.create(user=request.user,
                                                  plant=Plant.objects.get(id=serializer.data.get('id')))
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the plant for the particular user
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        self.add_child_data_for_plant(request, serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

"""
    Viewset for Plant Detail allows read operations
"""
class PlantDetailViewSet(viewsets.ModelViewSet):
    serializer_class = PlantDetailSerializer
    queryset = Plant.objects.all().order_by('client')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = PlantFilter
    permission_classes = [PlantPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
    Filter for PlantDocument which will filter document by exact plant id
"""
class PlantDocumentFilter(django_filters.FilterSet):
    class Meta:
        model = PlantDocument
        fields = {
            'plant': ['exact']
        }

"""
    Viewset for PlantDocument allows CRUD operations
"""
class PlantDocumentModelViewSet(viewsets.ModelViewSet):
    serializer_class = PlantDocumentSerializer
    queryset = PlantDocument.objects.all().order_by('plant')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = PlantDocumentFilter
    permission_classes = [PlantDocumentPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
    Filter for PlantImage which will filter image by exact plant id
"""
class PlantImageFilter(django_filters.FilterSet):
    class Meta:
        model = PlantImage
        fields = {
            'plant': ['exact']
        }


"""
    Viewset for PlantImage allows CRUD operations
"""
class PlantImageModelViewSet(viewsets.ModelViewSet):
    serializer_class = PlantImageSerializer
    queryset = PlantImage.objects.all().order_by('plant')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = PlantImageFilter
    permission_classes = [PlantDocumentPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
    Viewset for PlantUser allows CRUD operations
"""
class PlantUserModelViewSet(viewsets.ModelViewSet):
    serializer_class = PlantUserSerializer
    queryset = PlantUser.objects.all().order_by('plant')
    filter_backends = (filters.DjangoFilterBackend,)
    permission_classes = [PlantUserPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def create(self, request, *args, **kwargs):
        """
        This function will create the plant user for the particular plant
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        plant = serializer.instance.plant
        user = serializer.instance.user
        UserActivityLog.objects.create(
            object_id=user.id,
            user=user,
            action='user_entities_added',
            plants_changed = PlantListSerializer(plant).data
        )
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def destroy(self, request, *args, **kwargs):
        """
        This function will destroy the instance or delete the project user
        """
        instance = self.get_object()
        plant = instance.plant
        user = instance.user
        self.perform_destroy(instance)
        project_users = ProjectUser.objects.filter(project__client=plant.client, user=user)
        project_users.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

"""
    Viewset for Plant Model Project allows read operations
"""
class PlantModelProjectViewSet(viewsets.ModelViewSet):
    serializer_class = PlantProjectListSerializer
    queryset = Plant.objects.all().order_by('client_name')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = PlantFilter
    permission_classes = [PlantPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
    Viewset for PlantIndicator allows read operations
"""
class PlantIndicatorViewSet(viewsets.ModelViewSet):
    serializer_class = PlantIndicatorSerializer
    queryset = Plant.objects.all().order_by('client')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = PlantFilter
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
    Filter for PlantDashboard which will filter dashboard by exact plant id
"""
class PlantDashboardFilter(django_filters.FilterSet):
    class Meta:
        model = PlantDashboard
        fields = {
            'plant': ['exact'],
        }

"""
    Viewset for PlantDashboard allows CRUD operations
"""
class PlantDashboardModelViewSet(viewsets.ModelViewSet):
    serializer_class = PlantDashboardSerializer
    queryset = PlantDashboard.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = PlantDashboardFilter
    permission_classes = [PlantDashboardPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
    Filter for PlantScheduleVII which will filter ScheduleVII by exact plant id
"""
class PlantScheduleVIIActivityFilter(django_filters.FilterSet):
    class Meta:
        model = PlantScheduleVII
        fields = {
            'plant': ['exact']
        }

"""
    Viewset for PlantScheduleVII
"""
class PlantScheduleVIIActivityViewSet(BulkModelViewSet):
    serializer_class = PlantScheduleVIIActivitySerializer
    queryset = PlantScheduleVII.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = PlantScheduleVIIActivityFilter
    permission_classes = [ProjectFeaturePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]
    lookup_field = 'plant'

    def bulk_update(self, request, *args, **kwargs):
        """
        This function will update the PlantScheduleVII in bulk.
        """
        plant = request.data[0].get('plant')
        qs = PlantScheduleVII.objects.filter(plant__id=plant)
        filtered = self.filter_queryset(qs)
        for obj in filtered:
            self.perform_destroy(obj)
        serializer = self.get_serializer(data=request.data, many=True)
        serializer.is_valid(raise_exception=True)
        self.perform_bulk_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

"""
    Api view for Delete User Plant
"""
class DeleteUserPlantAPIView(APIView):
    permission_classes = [DeletePlantUserPermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def delete(request):
        """
        This function will delete the plant user.
        """
        plant = request.GET.get('plant')
        user = request.GET.get('user')
        user_obj = User.objects.get(id=user)
        projects = Project.objects.filter(plant=plant)
        plant_obj = Plant.objects.get(id=plant)
        project_user = ProjectUser.objects.filter(project__in=projects, user__id=user)
        project_user.delete()
        plant_user = PlantUser.objects.get(user__id=user, plant__id=plant)
        plant_user.delete()
        UserActivityLog.objects.create(
            object_id=user,
            user=user_obj,
            action='user_entities_removed',
            projects_changed = ProjectListSerializer(projects, many=True).data,
            plants_changed = PlantListSerializer(plant_obj).data
        )
        return Response({'success': True}, status=status.HTTP_200_OK)



class PlantImageDownload(APIView):
    @staticmethod
    def get(request):
        # try:
        plant_id = request.GET.get("plant")
        image_objects = PlantImage.objects.filter(plant=plant_id)
        url_list = image_objects[:]
        f = BytesIO()
        # zip = zipfile.ZipFile(f, 'w')
        with zipfile.ZipFile(f, mode='w') as zf:
            for image in url_list:
                url_path = image.image_file.url
                filename = url_path.split('/')[-1].split('.png')[0]
                url = urllib.request.urlopen(url_path)
                zf.writestr(filename+'.jpge', url.read())
        # zip.close()
        response = HttpResponse(f.getvalue(), content_type="application/zip")
        response['Content-Disposition'] = 'attachment; filename=plant4bar.zip'
        return response
        # except Exception as error:
        #      return Response({'error': 'Failed to Download file.' + str(err)}, status=status.HTTP_400_BAD_REQUEST)
