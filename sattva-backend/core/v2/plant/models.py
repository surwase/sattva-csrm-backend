from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.db import models

from domainlibraries.models import FocusArea, TargetSegment, ScheduleVIIActivity
from geolibraries.models import AuditFields, validate_file_extension
from v2.client.models import Client

"""
   This model will be used to store the plants
"""
class Plant(AuditFields):

    plant_name = models.CharField(max_length=511)
    plant_description = models.TextField(null=True, blank=True)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    start_date = models.DateTimeField(null=True, blank=True)
    end_date = models.DateTimeField(null=True, blank=True)
    budget = models.FloatField(default=0, null=True, blank=True)

    def get_name(self):
        return self.plant_name

    # fetch project
    def get_projects(self):
        from v2.project.models import Project
        return Project.objects.filter(plant=self)

    # fetch images
    def get_images(self):
        return PlantImage.objects.filter(plant=self)

    # fetch files
    def get_files(self):
        return PlantDocument.objects.filter(plant=self)

    # fetch all project budget
    def get_all_project_budget(self):
        all_projects = self.get_projects()
        budget = 0
        for project in all_projects:
            budget += project.budget
        return budget

    # fetch all project status
    def get_all_project_status(self, projects=None):
        all_projects = self.get_projects() if not projects else projects
        status = 0
        for project in all_projects:
            status += project.project_status
        return status

    # fetch all project disbursement total_amount
    def get_all_project_disbursement_total_amount(self, projects=None):
        all_projects = self.get_projects() if not projects else projects
        total = 0
        for project in all_projects:
            total += project.total_disbursement_amount
        return total

    # fetch all project disbursement planned_amount
    def get_all_project_disbursement_planned_amount(self, projects=None):
        all_projects = self.get_projects() if not projects else projects
        total = 0
        for project in all_projects:
            total += project.planned_disbursement_amount
        return total

    # fetch all project total_utilised_amount
    def get_all_project_total_utilised_amount(self, projects=None):
        all_projects = self.get_projects() if not projects else projects
        total = 0
        for project in all_projects:
            total += project.total_utilised_amount
        return total

    # fetch the total budget
    def get_total_budget(self, projects=None):
        project_list = self.get_projects() if projects is None else projects
        total_budget = 0
        for project in project_list:
            total_budget += project.budget
        return total_budget

    # fetch total disbursement amount
    def get_total_disbursement_amount(self, projects=None):
        project_list = self.get_projects() if projects is None else projects
        total_budget = 0
        for project in project_list:
            total_budget += project.total_disbursement_amount
        return total_budget

    # fetch planned disbursement amount
    def get_planned_disbursement_amount(self, projects=None):
        project_list = self.get_projects() if projects is None else projects
        total_budget = 0
        for project in project_list:
            total_budget += project.planned_disbursement_amount
        return total_budget

    # fetch total utilized amount
    def get_total_utilized_amount(self, projects=None):
        project_list = self.get_projects() if projects is None else projects
        total_utilized = 0
        for project in project_list:
            total_utilized += project.total_utilised_amount
        return total_utilized

    # fetch milestones
    def get_milestones(self, milestones=None):
        from v2.project.models import Milestone
        return Milestone.objects.filter(project__plant=self) if milestones is None else milestones

    # fetch all disbursements
    def get_all_disbursements(self):
        from v2.project.models import Disbursement
        return Disbursement.objects.filter(project__plant=self)

    # fetch all project locations
    def get_all_project_locations(self):
        from v2.project.models import ProjectLocation
        all_projects = self.get_projects()
        locations = ProjectLocation.objects.filter(project__in=all_projects).values_list('location', flat=True)
        return locations

    # fetch project focus_areas list
    def get_project_focus_areas_list(self, projects=None):
        from v2.project.models import ProjectFocusArea
        project_list = self.get_projects() if projects is None else projects
        focus_area = ProjectFocusArea.objects.filter(project__in=project_list).values_list('focus_area', flat=True)
        return list(set(focus_area)) if len(focus_area) else []

    # fetch project sub_focus_areas list
    def get_project_sub_focus_areas_list(self, projects=None):
        from v2.project.models import ProjectSubFocusArea
        project_list = self.get_projects() if projects is None else projects
        sub_focus_area = ProjectSubFocusArea.objects.filter(project__in=project_list).values_list('sub_focus_area',
                                                                                                  flat=True)
        return list(set(sub_focus_area)) if len(sub_focus_area) else []

    # fetch project target segment list
    def get_project_target_segment_list(self, projects=None):
        from v2.project.models import ProjectTargetSegment
        project_list = self.get_projects() if projects is None else projects
        target_segment = ProjectTargetSegment.objects.filter(project__in=project_list).values_list('target_segment',
                                                                                                   flat=True)
        return list(set(target_segment)) if len(target_segment) else []

    # fetch project sub target segment list
    def get_project_sub_target_segment_list(self, projects=None):
        from v2.project.models import ProjectSubTargetSegment
        project_list = self.get_projects() if projects is None else projects
        sub_target_segment = ProjectSubTargetSegment.objects.filter(project__in=project_list).values_list(
            'sub_target_segment', flat=True)
        return list(set(sub_target_segment)) if len(sub_target_segment) else []

    # fetch projects sdg goals
    def get_projects_sdg_goals(self, projects=None):
        from v2.project.models import ProjectSDGGoals
        all_projects = self.get_projects() if projects is None else projects
        sdg_goals = ProjectSDGGoals.objects.filter(project__in=all_projects).values_list('sdg_goal', flat=True)
        final_sdg_goals = []
        goal_ids = []
        for sdg_goal in list(sdg_goals):
            if sdg_goal and sdg_goal['id'] not in goal_ids:
                final_sdg_goals.append(sdg_goal)
                goal_ids.append(sdg_goal['id'])
        return final_sdg_goals

    def get_dashboard(self):
        return PlantDashboard.objects.filter(plant=self)

    # fetch project over utilization
    def get_project_over_utilization(self, projects=None):
        project_list = self.get_projects() if projects is None else projects
        utilization_focus_area = []
        for project in project_list:
            if project.total_utilised_amount > 0:
                if not project.get_focus_area() is None:
                    utilization_focus_area + list(project.get_focus_area())
        return list(set(utilization_focus_area)) if len(utilization_focus_area) else []

    # fetch all projects average status
    def get_all_projects_average_status(self, projects=None):
        project_list = self.get_projects() if projects is None else projects
        avg_status = 0
        for project in project_list:
            avg_status += float(project.project_status)
        return avg_status / project_list.count() if project_list.count() else 0

    # fetch all live projects
    def get_all_live_projects(self):
        from v2.project.models import Project
        project_list = self.get_projects()
        project_id_list = []
        for project in project_list:
            if project.project_status < 100:
                project_id_list.append(project.id)
        return Project.objects.filter(id__in=project_id_list)

    # fetch all completed projects
    def get_all_completed_projects(self):
        from v2.project.models import Project
        project_list = self.get_projects()
        project_id_list = []
        for project in project_list:
            if project.project_status >= 100:
                project_id_list.append(project.id)
        return Project.objects.filter(id__in=project_id_list)

    # fetch all open projects
    def get_all_open_projects(self):
        from v2.project.models import Project
        project_list = self.get_projects()
        project_id_list = []
        for project in project_list:
            if project.project_status == 0:
                project_id_list.append(project.id)
        return Project.objects.filter(id__in=project_id_list)

    # fetch all on going projects
    def get_all_on_going_projects(self):
        from v2.project.models import Project
        project_list = self.get_projects()
        project_id_list = []
        for project in project_list:
            if project.project_status > 0 and project.project_status < 100:
                project_id_list.append(project.id)
        return Project.objects.filter(id__in=project_id_list)

    # fetch count of risk status
    def get_count_of_risk_status(self, start_date=None, end_date=None, projects=None):
        from v2.risk_management.models import RiskEntry
        all_project_risk_entries = RiskEntry.objects.filter(plant=self)
        if projects is not None:
            all_project_risk_entries = all_project_risk_entries.filter(project__in=projects)
        if start_date is not None and end_date is not None:
            all_project_risk_entries = all_project_risk_entries.filter(created_date__gte=start_date,
                                                                       created_date__lte=end_date)
        risk_entries_count = all_project_risk_entries.count()
        if risk_entries_count:
            risk_status = {
                'Low': all_project_risk_entries.filter(severity=0).count(),
                "Medium": all_project_risk_entries.filter(severity=1).count(),
                "High": all_project_risk_entries.filter(severity=2).count(),
                "Critical": all_project_risk_entries.filter(severity=3).count(),
                "Total": all_project_risk_entries.count(),
            }
        else:
            risk_status = {}
        return risk_status

    # fetch all projects list by date range
    def get_all_projects_list_by_date_range(self, start_date, end_date):
        return self.get_projects().filter(start_date__lte=end_date, end_date__gte=start_date)

    class Meta:
        unique_together = ('client', 'plant_name')

    def __str__(self):
        return self.client.client_name + " | " + self.plant_name

"""
   This model will be used to store the plant location.
"""
class PlantLocation(AuditFields):
    plant = models.ForeignKey(Plant, on_delete=models.CASCADE)
    location = JSONField(default=[])
    area = models.CharField(max_length=256, default='', null=True)
    city = models.CharField(max_length=256, default='', null=True)
    state = models.CharField(max_length=256, default='', null=True)
    country = models.CharField(max_length=256, default='', null=True)
    latitude = models.FloatField(max_length=31, default=0, null=True)
    longitude = models.FloatField(max_length=31, default=0, null=True)

    def __str__(self):
        return self.plant.plant_name


"""
   This model will be used to store the focus area for plant.
"""
class PlantFocusArea(AuditFields):
    plant = models.ForeignKey(Plant, on_delete=models.CASCADE)
    focus_area = models.CharField(max_length=255)

    def __str__(self):
        return self.plant.plant_name


"""
   This model will be used to store the sub focus area for plant.
"""
class PlantSubFocusArea(AuditFields):
    plant = models.ForeignKey(Plant, on_delete=models.CASCADE)
    sub_focus_area = models.CharField(max_length=255)

    def __str__(self):
        return self.plant.plant_name


"""
   This model will be used to store the target segments for plant.
"""
class PlantTargetSegment(AuditFields):
    plant = models.ForeignKey(Plant, on_delete=models.CASCADE)
    target_segment = models.CharField(max_length=255)

    def __str__(self):
        return self.plant.plant_name


"""
   This model will be used to store the sub target segments for plant.
"""
class PlantSubTargetSegment(AuditFields):
    plant = models.ForeignKey(Plant, on_delete=models.CASCADE)
    sub_target_segment = models.CharField(max_length=255)

    def __str__(self):
        return self.plant.plant_name


"""
   This model will be used to store the SDG Goals for plant.
"""
class PlantSDGGoals(AuditFields):
    plant = models.ForeignKey(Plant, on_delete=models.CASCADE)
    sdg_goal = JSONField(default=[], null=True)
    sdg_goal_name = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.plant.plant_name


"""
   This model will be used to store the ScheduleVII for plant.
"""
class PlantScheduleVII(AuditFields):
    plant = models.ForeignKey(Plant, on_delete=models.CASCADE)
    schedule_vii_activity = models.ForeignKey(ScheduleVIIActivity, on_delete=models.CASCADE)


"""
   This model will be used to store the users for plant.
"""
class PlantUser(AuditFields):
    plant = models.ForeignKey(Plant, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return self.plant.plant_name + " - " + self.user.first_name

    class Meta:
        unique_together = ('plant', 'user')


"""
   This model will be used to store the documents for plant
"""
class PlantDocument(AuditFields):
    plant = models.ForeignKey(Plant, on_delete=models.CASCADE)
    document_file = models.FileField(max_length=10000, validators=[validate_file_extension])
    doc_size = models.BigIntegerField(null=True, blank=True)
    document_tag = JSONField(default=[], null=True, blank=True)
    path = models.TextField(default='')

    def __str__(self):
        return self.plant.plant_name + " - "

    def save(self, *args, **kwargs):
        if not self.pk:
            self.document_file.name = 'file_system/{0}/{1}/'.format(self.plant.client.client_name,
                                                                    self.plant.plant_name) + self.document_file.name
        super(PlantDocument, self).save(*args, **kwargs)


"""
   This model will be used to store the images for plant
"""
class PlantImage(AuditFields):
    plant = models.ForeignKey(Plant, on_delete=models.CASCADE)
    image_file = models.FileField(max_length=10000, validators=[validate_file_extension])
    document_tag = JSONField(default=[], null=True, blank=True)
    path = models.TextField(default='')

    def __str__(self):
        return self.plant.plant_name + " - "

    def save(self, *args, **kwargs):
        if not self.pk:
            self.image_file.name = 'file_system/{0}/{1}/images/'.format(self.plant.client.client_name,
                                                                        self.plant.plant_name) + self.image_file.name
        super(PlantImage, self).save(*args, **kwargs)


"""
   This model will be used to store the dashboard for plant
"""
class PlantDashboard(AuditFields):
    PROVIDER_METABASE = 0
    PROVIDER_ZOHO = 1
    PROVIDER_CHOICES = ((PROVIDER_METABASE, 'Metabase'), (PROVIDER_ZOHO, 'Zoho'))
    plant = models.ForeignKey(Plant, on_delete=models.CASCADE)
    provider = models.SmallIntegerField(choices=PROVIDER_CHOICES)
    dashboard_name = models.CharField(max_length=511, default='Insights')
    url = models.TextField()
    embedded_content = models.TextField()
    password = models.CharField(max_length=220, null=True, blank=True)


"""
   This model will be used to store the mca focus area for plant.
"""
class PlantMCA(AuditFields):
    plant = models.ForeignKey(Plant, on_delete=models.CASCADE)
    mca_focus_area = models.CharField(max_length=255)
    mca_sub_focus_area = models.CharField(max_length=255)
    mca = JSONField(default=[], null=True)

    def __str__(self):
        return self.plant.plant_name

