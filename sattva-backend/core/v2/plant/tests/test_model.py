from django.core.checks.messages import Error
from django.test import TestCase
from v2.plant.models import Plant,PlantLocation,PlantFocusArea,PlantSubFocusArea,PlantTargetSegment,PlantSubTargetSegment,PlantSDGGoals,PlantScheduleVII,PlantUser,PlantDocument,PlantImage,PlantDashboard
from .test_setup import TestSetUp
from domainlibraries.models import  DataType, ScheduleVIIActivity
from v2.project.models import Project,Milestone,NGOPartner,ProjectLocation,ProjectFocusArea,ProjectSubFocusArea,ProjectTargetSegment,ProjectSubTargetSegment,ProjectSDGGoals
from unittest.mock import patch
from django.db.utils import DataError
from django.core.files.uploadedfile import SimpleUploadedFile

class TestPlant(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self,mock_send):
        super().setUp()
        Plant.objects.create(
            plant_name = "plant1",
            plant_description = "plantdec",
            client = self.client_obj,
            budget = 1000.50,
            start_date ="2020-06-22T18:30:00Z",
            end_date = "2021-03-22T18:30:00Z"
            )

        plant1=Plant.objects.get( plant_name = "plant1")
        PlantLocation.objects.create(
            plant = plant1,
            location = ['lucknow'],
            area = "indira nagar",
            city = "lucknow",
            state = "up",
            country = "india",
            latitude = 15.5,
            longitude = 15.5)

        PlantFocusArea.objects.create(
            plant =  plant1,
            focus_area = "education"
        )

        PlantSubFocusArea.objects.create(
            plant = plant1,
            sub_focus_area ="children"
            )

        PlantTargetSegment.objects.create(
            plant = plant1,
            target_segment ="xyz"
            )

        PlantSubTargetSegment.objects.create(
            plant = plant1,
            sub_target_segment ="pqr"
            )

        PlantSDGGoals.objects.create(
            plant = plant1,
            sdg_goal = ['tagsdg1'],
            sdg_goal_name ="sdg"
            )

        self.schedulevii_obj=ScheduleVIIActivity.objects.create(
            activity_description = "xyzzzzzz",
            )

        PlantScheduleVII.objects.create(
            plant = plant1,
            schedule_vii_activity = self.schedulevii_obj
            )

        PlantUser.objects.create(
            plant = plant1,
            user = self.user
            )

        Project.objects.create(
            project_name = "xyz",
            project_description = "hi i am tested",
            client = self.client_obj,
            plant = plant1,
            budget = 1000,
            project_status = 72.22,
            total_disbursement_amount = 8804465,
            planned_disbursement_amount = 8804465,
            total_utilised_amount = 7000000,
            start_date ="2020-06-22T18:30:00Z",
            end_date = "2021-03-22T18:30:00Z"
            )

        project1 = Project.objects.get(project_name="xyz")
        Milestone.objects.create(
            milestone_name="milestone1",
            milestone_description="hi i am milestone",
            status="complete",
            project=project1,
            position=10,
            sub_focus_area=['malnutrition'],
            sub_target_segment=['children'],
            location=['lucknow'],
            tags=['tag1', 'tag2'],
        )

        ProjectLocation.objects.create(
                project=project1,
                location=['lucknow'],
                area="indira nagar",
                city="lucknow",
                state="up",
                country="India",
                latitude=59.9,
                longitude=60.3)

        ProjectFocusArea.objects.create(
                project=project1,
                focus_area="children")

        ProjectSubFocusArea.objects.create(
                project=project1,
                sub_focus_area="child")

        ProjectTargetSegment.objects.create(
                project=project1,
                target_segment="target1")

        ProjectSubTargetSegment.objects.create(
                project=project1,
                sub_target_segment="subtarget1")

        ProjectSDGGoals.objects.create(
                project=project1,
                sdg_goal_name="sdg1",
                sdg_goal=['sdg'])

        PlantDocument.objects.create(
         plant = plant1,
         document_file = SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
         doc_size = 1000,
         document_tag = ['tag1'],
         path = "avc/avg/ppp"
         )

        PlantImage.objects.create(
         plant = plant1,
         image_file = SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
         document_tag = ['tag1'],
         path = "avc/avg/ppp"
         )

        PlantDashboard.objects.create(
            plant = plant1,
            provider = 0,
            dashboard_name = "dash1",
            url = "www.xyz.com",
            embedded_content = "hi i am dash board",
            password = "zxcvbnm"
            )

    def test_validation_plant(self):
        with self.assertRaises(ValueError):
            Plant.objects.create(
                plant_name = "@#$",
                plant_description = "plantdec",
                client = self.client_obj,
                budget ="hi",
           )

    def test_values(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        dashboard=PlantDashboard.objects.get(dashboard_name = "dash1")
        self.assertEqual(plant1.plant_name , "plant1")
        self.assertEqual(plant1.plant_description ,"plantdec")
        self.assertEqual(plant1.client.client_name,"client Test10")
        self.assertEqual(plant1.budget , 1000.50)
        self.assertEqual(dashboard.dashboard_name , "dash1")
        self.assertEqual(dashboard.url , "www.xyz.com")
        self.assertEqual(dashboard.embedded_content , "hi i am dash board")
        self.assertEqual(dashboard.password , "zxcvbnm")

    def test_get_name(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_name(),"plant1")

    def test_get_projects(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_projects().count(),1)

    def test_get_images(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_images().count(),1)

    def test_get_files(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_files().count(),1)

    def test_get_all_project_budget(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_all_project_budget(),1000)

    def test_get_all_project_status(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_all_project_status(),72.22)

    def test_get_all_project_disbursement_total_amount(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_all_project_disbursement_total_amount(),8804465)

    def test_get_all_project_disbursement_planned_amount(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_all_project_disbursement_planned_amount(),8804465)

    def test_get_all_project_total_utilised_amount(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_all_project_total_utilised_amount(),7000000)

    def test_get_total_budget(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_total_budget(),1000)

    def test_get_total_disbursement_amount(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_total_disbursement_amount(),8804465)

    def test_get_planned_disbursement_amount(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_planned_disbursement_amount(),8804465)

    def test_get_total_utilized_amount(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_total_utilized_amount(),7000000)

    def test_get_milestones(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_milestones().count(),1)

    def test_get_all_disbursements(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_all_disbursements().count(),0)

    def test_get_all_project_locations(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_all_project_locations().count(),1)

    def test_get_project_focus_areas_list(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(len(plant1.get_project_focus_areas_list()),1)

    def test_get_project_sub_focus_areas_list(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(len(plant1.get_project_sub_focus_areas_list()),1)

    def test_get_project_target_segment_list(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(len(plant1.get_project_target_segment_list()),1)

    def test_get_project_sub_target_segment_list(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(len(plant1.get_project_sub_target_segment_list()),1)

    def test_get_dashboard(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_dashboard().count(),1)

    def test_get_project_over_utilization(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(len(plant1.get_project_over_utilization()),0)

    def test_get_all_projects_average_status(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_all_projects_average_status(),72.22)

    def test_get_all_live_projects(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_all_live_projects().count(),1)

    def test_get_all_completed_projects(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_all_completed_projects().count(),0)

    def test_get_all_open_projects(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_all_open_projects().count(),0)

    def test_get_all_on_going_projects(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_all_on_going_projects().count(),1)

    def test_get_count_of_risk_status(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_count_of_risk_status(),{})

    def test_get_all_projects_list_by_date_range(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.get_all_projects_list_by_date_range("2020-06-21","2021-03-23").count(),1)

    def test__str__(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        self.assertEqual(plant1.__str__(),"client Test10"+ " | " + "plant1")


class TestPlantLocation(TestSetUp):

    def setUp(self):
        super().setUp()
        Plant.objects.create(
            plant_name = "plant1",
            plant_description = "plantdec",
            client = self.client_obj,
            budget = 1000.50,
            start_date ="2020-06-22T18:30:00Z",
            end_date = "2021-03-22T18:30:00Z"
            )

        plant1=Plant.objects.get( plant_name = "plant1")
        PlantLocation.objects.create(
            plant = plant1,
            location = ['lucknow'],
            area = "indira nagar",
            city = "lucknow",
            state = "up",
            country = "india",
            latitude = 15.5,
            longitude = 15.5)

    def test_validation_plantlocation(self):
        with self.assertRaises(ValueError):
            plant1=Plant.objects.get( plant_name = "plant1")
            PlantLocation.objects.create(
            plant = plant1,
            location = 'lucknow',
            area = 123,
            city = 56,
            state = '@',
            country = "india",
            latitude = "latitude",
            longitude = "longitude")

    def test_values(self):
        plocation=PlantLocation.objects.get(area = "indira nagar")
        self.assertEqual(plocation.location ,['lucknow'])
        self.assertEqual(plocation.area , "indira nagar")
        self.assertEqual(plocation.city , "lucknow")
        self.assertEqual(plocation.state , "up")
        self.assertEqual(plocation.country, "india")
        self.assertEqual(plocation.latitude , 15.5)
        self.assertEqual(plocation.longitude , 15.5)

    def test__str__(self):
        plocation=PlantLocation.objects.get(area = "indira nagar")
        self.assertEqual(plocation.__str__() ,"plant1")


class TestPlantFocusArea(TestSetUp):

    def setUp(self):
        super().setUp()
        Plant.objects.create(
            plant_name = "plant1",
            plant_description = "plantdec",
            client = self.client_obj,
            budget = 1000.50,
            start_date ="2020-06-22T18:30:00Z",
            end_date = "2021-03-22T18:30:00Z"
        )

        plant1=Plant.objects.get( plant_name = "plant1")
        PlantFocusArea.objects.create(
            plant =  plant1,
            focus_area = "education"
        )

    def test_values(self):
        pfocus1=PlantFocusArea.objects.get(focus_area = "education")
        self.assertEqual(pfocus1.plant.plant_name , "plant1")
        self.assertEqual(pfocus1.focus_area , "education")

    def test__str__(self):
        pfocus1=PlantFocusArea.objects.get(focus_area = "education")
        self.assertEqual(pfocus1.__str__(),"plant1")


class TestPlantSubFocusArea(TestSetUp):

    def setUp(self):
        super().setUp()
        Plant.objects.create(
            plant_name = "plant1",
            plant_description = "plantdec",
            client = self.client_obj,
            budget = 1000.50,
            start_date ="2020-06-22T18:30:00Z",
            end_date = "2021-03-22T18:30:00Z"
            )

        plant1=Plant.objects.get( plant_name = "plant1")
        PlantSubFocusArea.objects.create(
            plant = plant1,
            sub_focus_area ="children"
            )

    def test_values(self):
        psubfocus=PlantSubFocusArea.objects.get(sub_focus_area ="children")
        self.assertEqual(psubfocus.plant.plant_name , "plant1")
        self.assertEqual(psubfocus.sub_focus_area ,"children")

    def test__str__(self):
        psubfocus=PlantSubFocusArea.objects.get(sub_focus_area ="children")
        self.assertEqual(psubfocus.__str__() ,"plant1" )


class TestPlantTargetSegment(TestSetUp):

    def setUp(self):
        super().setUp()
        Plant.objects.create(
            plant_name = "plant1",
            plant_description = "plantdec",
            client = self.client_obj,
            budget = 1000.50,
            start_date ="2020-06-22T18:30:00Z",
            end_date = "2021-03-22T18:30:00Z"
            )

        plant1=Plant.objects.get( plant_name = "plant1")
        PlantTargetSegment.objects.create(
            plant = plant1,
            target_segment ="xyz"
            )

    def test_values(self):
        ptarget=PlantTargetSegment.objects.get(target_segment ="xyz")
        self.assertEqual(ptarget.plant.plant_name , "plant1")
        self.assertEqual(ptarget.target_segment ,"xyz")

    def test__str__(self):
        ptarget=PlantTargetSegment.objects.get(target_segment ="xyz")
        self.assertEqual(ptarget.__str__() ,"plant1")


class TestPlantSubTargetSegmen(TestSetUp):

    def setUp(self):
        super().setUp()
        Plant.objects.create(
            plant_name = "plant1",
            plant_description = "plantdec",
            client = self.client_obj,
            budget = 1000.50,
            start_date ="2020-06-22T18:30:00Z",
            end_date = "2021-03-22T18:30:00Z"
            )

        plant1=Plant.objects.get( plant_name = "plant1")
        PlantSubTargetSegment.objects.create(
            plant = plant1,
            sub_target_segment ="pqr"
            )

    def test_values(self):
        psubtarget=PlantSubTargetSegment.objects.get(sub_target_segment ="pqr")
        self.assertEqual( psubtarget.plant.plant_name , "plant1")
        self.assertEqual( psubtarget.sub_target_segment ,"pqr")

    def test__str__(self):
        psubtarget=PlantSubTargetSegment.objects.get(sub_target_segment ="pqr")
        self.assertEqual( psubtarget.__str__(),"plant1")


class TestPlantSDGGoals(TestSetUp):

    def setUp(self):
        super().setUp()
        Plant.objects.create(
            plant_name = "plant1",
            plant_description = "plantdec",
            client = self.client_obj,
            budget = 1000.50,
            start_date ="2020-06-22T18:30:00Z",
            end_date = "2021-03-22T18:30:00Z"
            )

        plant1=Plant.objects.get( plant_name = "plant1")
        PlantSDGGoals.objects.create(
            plant = plant1,
            sdg_goal = ['tagsdg1'],
            sdg_goal_name ="sdg"
            )

    def test_validation(self):
            with self.assertRaises(DataError):
                plant1=Plant.objects.get( plant_name = "plant1")
                PlantSDGGoals.objects.create(
                plant = plant1,
                sdg_goal = 123,
                sdg_goal_name ="zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"
                )


    def test_values(self):
        psg=PlantSDGGoals.objects.get(sdg_goal_name ="sdg")
        self.assertEqual(psg.plant.plant_name , "plant1")
        self.assertEqual(psg.sdg_goal , ['tagsdg1'])
        self.assertEqual(psg.sdg_goal_name ,"sdg")

    def test__str__(self):
        psg=PlantSDGGoals.objects.get(sdg_goal_name ="sdg")
        self.assertEqual(psg.__str__(),"plant1")


class TestPlantUser(TestSetUp):

    def setUp(self):
        super().setUp()
        Plant.objects.create(
            plant_name = "plant1",
            plant_description = "plantdec",
            client = self.client_obj,
            budget = 1000.50,
            start_date ="2020-06-22T18:30:00Z",
            end_date = "2021-03-22T18:30:00Z"
            )

        plant1=Plant.objects.get( plant_name = "plant1")
        PlantUser.objects.create(
            plant = plant1,
            user = self.user
            )

    def test_values(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        pl1=PlantUser.objects.get(plant = plant1)
        self.assertEqual(pl1.plant.plant_name , "plant1")
        self.assertEqual(pl1.user.username,'diksha')


    def test__str__(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        pl1=PlantUser.objects.get(plant = plant1)
        self.assertEqual(pl1.__str__() ,'plant1' ' - ' 'Diksha')


class TestPlantDocument(TestSetUp):

    def setUp(self):
        super().setUp()
        Plant.objects.create(
            plant_name = "plant1",
            plant_description = "plantdec",
            client = self.client_obj,
            budget = 1000.50,
            start_date ="2020-06-22T18:30:00Z",
            end_date = "2021-03-22T18:30:00Z"
            )
        plant1=Plant.objects.get( plant_name = "plant1")
        PlantDocument.objects.create(
            plant = plant1,
            document_file = SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            doc_size = 1000,
            document_tag = ['tag1'],
            path = "avc/avg/ppp"
        )

    def test_values(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        pl1=PlantDocument.objects.get(plant = plant1)
        self.assertEqual(pl1.plant.plant_name , "plant1")
        self.assertEqual(pl1.path , "avc/avg/ppp")
        self.assertEqual(pl1.doc_size , 1000)
        self.assertEqual(pl1.document_tag , ['tag1'])

    def test__str__(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        pl1=PlantDocument.objects.get(plant = plant1)
        self.assertEqual(pl1.plant.plant_name + " - " ,"plant1" " - ")


class TestPlantImage(TestSetUp):

    def setUp(self):
        super().setUp()
        Plant.objects.create(
            plant_name = "plant1",
            plant_description = "plantdec",
            client = self.client_obj,
            budget = 1000.50,
            start_date ="2020-06-22T18:30:00Z",
            end_date = "2021-03-22T18:30:00Z"
            )
        plant1=Plant.objects.get( plant_name = "plant1")
        PlantImage.objects.create(
            plant = plant1,
            image_file = SimpleUploadedFile('best_file_eva.txt', b'these are the contents of the txt file'),
            document_tag = ['tag1'],
            path = "avc/avg/ppp"
        )

    def test_values(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        pl1=PlantImage.objects.get(plant = plant1)
        self.assertEqual(pl1.plant.plant_name , "plant1")
        self.assertEqual(pl1.document_tag , ['tag1'])
        self.assertEqual(pl1.path , "avc/avg/ppp")

    def test__str__(self):
        plant1=Plant.objects.get( plant_name = "plant1")
        pl1=PlantImage.objects.get(plant = plant1)
        self.assertEqual(pl1.__str__(),"plant1" " - " )

