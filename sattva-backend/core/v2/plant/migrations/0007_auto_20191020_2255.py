# Generated by Django 2.0.5 on 2019-10-20 17:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plant', '0006_auto_20191020_2226'),
    ]

    operations = [
        migrations.AlterField(
            model_name='plant',
            name='focus_area',
            field=models.CharField(default='', max_length=127, null=True),
        ),
        migrations.AlterField(
            model_name='plant',
            name='target_segment',
            field=models.CharField(default='', max_length=127, null=True),
        ),
    ]
