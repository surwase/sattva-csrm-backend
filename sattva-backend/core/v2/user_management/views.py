from rest_framework import serializers, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import pagination

from core.users.models import User
from core.utils import check_null_and_replace_to_none
import django_filters
from django_filters import rest_framework as filters
from rest_framework.filters import SearchFilter
from django.db.models import Q
from v2.authentication import KeycloakUserUpdateAuthentication
from v2.client.models import ClientUser, Client
from v2.project.models import ProjectUser, Project
from v2.plant.models import PlantUser
from core.users.models import UserActivityLog
from core.users.serializers import UserActivityLogSerializer
from v2.user_management.models import ClientFeature, ProjectFeature, ProjectFeaturePermission, ClientFeaturePermission
from v2.user_management.permissions import ClientFeaturePermission as ClientFeaturePermissionClass
from v2.user_management.serializers import UserSerializer, UserDetailedSerializer, UserMiniDetailedSerializer, ClientFeatureSerializer, \
    ProjectFeatureSerializer, ClientFeaturePermissionSerializer, ProjectFeaturePermissionSerializer, ClientFeaturePermissionMicroSerializer, \
    ProjectFeaturePermissionMicroSerializer, UserMicroDetailedSerializer, ProjectFeaturePermissionMiniSerializer, \
    ClientFeaturePermissionMiniSerializer, UserApplicationRoleSerializer

"""
   Pagination class for user default page size is 5 and max size is 100
"""
class UserPagination(pagination.PageNumberPagination):
    page_size = 5
    max_page_size = 100
    page_size_query_param = 'records'

    def get_paginated_response(self, data):
        return Response({
             'count': self.page.paginator.count,
             'current': self.page.number,
             'next': self.get_next_link(),
             'previous': self.get_previous_link(),
             'results': data
        })

"""
   Viewset for User Model allows CRUD operations
"""
class UserModelViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    pagination_class = UserPagination
    queryset = User.objects.all().exclude(username='sattva-admin')
    filter_backends = (filters.DjangoFilterBackend,)
    permission_classes = [ClientFeaturePermissionClass]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_queryset(self):
        """
        This function will return the queryset
        queryset contain all users excluding sattva admin.
        If current user's application role is not None and application role.rolename is not sattva-admin then it will fetch all the users
        of the particular client .If role is not None then it will fetch user by role
        """
        queryset = User.objects.all().exclude(username='sattva-admin')
        role = self.request.query_params.get('application_role')
        if self.request.user.application_role is not None and self.request.user.application_role.role_name != 'sattva-admin':
            user_clients = ClientUser.objects.filter(user=self.request.user).values_list('client', flat=True)
            client_users = ClientUser.objects.filter(client__in=user_clients).values_list('user__id', flat=True)
            queryset = queryset.filter(id__in=client_users)
        if role is not None:
            role_values = role.split(',')
            queryset = queryset.filter(application_role__role_name__in=role_values)
        queryset = self.search_results(queryset)
        queryset = self.filter_list(queryset)
        return queryset

    def search_results(self, queryset):
        """
        This function will return the queryset to fetch user by search results
        """
        search = check_null_and_replace_to_none(self.request.query_params.get('search'))

        if search is not None:
            clientUser = ClientUser.objects.filter(client__client_name__icontains=search)
            clientUser = clientUser.values_list('user__email', flat=True)

            plantUser = PlantUser.objects.filter(plant__plant_name__icontains=search)
            plantUser = plantUser.values_list('user__email', flat=True)

            projectUser = ProjectUser.objects.filter(project__project_name__icontains=search)
            projectUser = projectUser.values_list('user__email', flat=True)

            queryset = queryset.filter(Q(email__in=clientUser) | Q(email__in=projectUser) | Q(email__in=plantUser) |
            Q(application_role__role_name__icontains=search) | Q(name__icontains=search) | Q(email__icontains=search))
        return queryset


    def filter_list(self, queryset):
        """
        This function will return the queryset to fetch user by filterlist
        """
        project = check_null_and_replace_to_none(self.request.query_params.get('project'))
        plant = check_null_and_replace_to_none(self.request.query_params.get('plant'))
        role = check_null_and_replace_to_none(self.request.query_params.get('role'))
        clients = check_null_and_replace_to_none(self.request.query_params.get('clients'))

        if clients is not None:

            clients = clients.split(',')
            clientUser = ClientUser.objects.filter(client__id__in=clients)
            clientUser = clientUser.values_list('user__email', flat=True)
            queryset = queryset.filter(email__in=clientUser)

        if project is not None:

            project = project.split(',')
            projectUser = ProjectUser.objects.filter(project__id__in=project)
            projectUser = projectUser.values_list('user__email', flat=True)
            queryset = queryset.filter(email__in=projectUser)

        if plant is not None:
            plant = plant.split(',')
            plantUser = PlantUser.objects.filter(plant__id__in=plant)
            plantUser = plantUser.values_list('user__email', flat=True)
            queryset = queryset.filter(email__in=plantUser)

        if role is not None:
            role = role.split(',')
            queryset = queryset.filter(application_role__role_name__in=role)

        return queryset

"""
   Viewset for user mini details allows read operation
"""
class UserMiniDetailModelViewSet(viewsets.ModelViewSet):
    serializer_class = UserMicroDetailedSerializer
    queryset = User.objects.all().exclude(username='admin')
    permission_classes = [ClientFeaturePermissionClass]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Viewset for user details allows read operation
"""
class UserDetailModelViewSet(viewsets.ModelViewSet):
    serializer_class = UserDetailedSerializer
    queryset = User.objects.all().exclude(username='admin')
    permission_classes = [ClientFeaturePermissionClass]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for User ActivityLog which filters the activity logs by exact userid
"""
class UserActivityLogFilter(django_filters.FilterSet):
    class Meta:
        model = UserActivityLog
        fields = {
            'user': ['exact']
        }

"""
   Viewset for User Activity Log allows read operation
"""
class UserActivityLogViewSet(viewsets.ModelViewSet):
    serializer_class = UserActivityLogSerializer
    queryset = UserActivityLog.objects.all().order_by('created_date')
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = UserActivityLogFilter
    permission_classes = [ClientFeaturePermissionClass]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_queryset(self):
        queryset = UserActivityLog.objects.all().order_by('created_date')
        queryset = queryset.exclude(action__in=['user_edited'])
        return queryset

"""
   Viewset for Client Features allows read operation
"""
class ClientFeaturesViewSet(viewsets.ModelViewSet):
    serializer_class = ClientFeatureSerializer
    queryset = ClientFeature.objects.all()
    permission_classes = [ClientFeaturePermissionClass]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Viewset for Project Features allows read operation
"""
class ProjectFeaturesViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectFeatureSerializer
    queryset = ProjectFeature.objects.all()
    permission_classes = [ClientFeaturePermissionClass]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for Project Feature Permission which filters the Permission by exact project id
"""
class ProjectFeaturePermissionFilter(django_filters.FilterSet):
    class Meta:
        model = ProjectFeaturePermission
        fields = {
            'project': ['exact']
        }

"""
  Viewset for Project Feature Permission allows read operation
"""
class ProjectFeaturePermissionViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectFeaturePermissionSerializer
    queryset = ProjectFeaturePermission.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProjectFeaturePermissionFilter
    permission_classes = [ClientFeaturePermissionClass]
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Filter for Client Feature Permission which filters the Permission by exact client id
"""
class ClientFeaturePermissionFilter(django_filters.FilterSet):
    class Meta:
        model = ClientFeaturePermission
        fields = {
            'client': ['exact']
        }

"""
  Viewset for client Feature Permission allows read operation
"""
class ClientFeaturePermissionViewSet(viewsets.ModelViewSet):
    serializer_class = ClientFeaturePermissionSerializer
    queryset = ClientFeaturePermission.objects.all()
    permission_classes = [ClientFeaturePermissionClass]
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ClientFeaturePermissionFilter
    authentication_classes = [KeycloakUserUpdateAuthentication]

"""
   Api view for User Project Feature
"""
class UserProjectFeatureAPIView(APIView):
    permission_classes = [ClientFeaturePermissionClass]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def get(request, user_id, project_id):
        """
        This function will sent a get request and fetch Project Feature Permission
        """
        user = User.objects.get(id=user_id)
        project_feature_permission = user.project_feature_permission
        project = Project.objects.get(id=project_id)
        project_feature_permission = project_feature_permission.filter(project=project)
        serializer = ProjectFeaturePermissionMiniSerializer(project_feature_permission, many=True)
        return Response(serializer.data)

"""
   Api view for User Client Feature
"""
class UserClientFeatureAPIView(APIView):
    permission_classes = [ClientFeaturePermissionClass]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    @staticmethod
    def get(request, user_id, client_id):
        """
        This function will sent a get request and fetch Client Feature Permission
        """
        user = User.objects.get(id=user_id)
        client_feature_permission = user.client_feature_permission
        client = Client.objects.get(id=client_id)
        client_feature_permission = client_feature_permission.filter(client=client)
        serializer = ClientFeaturePermissionMiniSerializer(client_feature_permission, many=True)
        return Response(serializer.data)

"""
   Api view for Current User
"""
class CurrentUserAPIView(APIView):
    permission_classes = [ClientFeaturePermissionClass]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get(self, request):
        """
        This function will sent a get request and fetch the current user
        """
        user = request.user
        data = UserMiniDetailedSerializer(user).data
        return Response(data)


"""
   Api view for Current User Application Role and id
"""
class CurrentUserARIDView(APIView):
    permission_classes = [ClientFeaturePermissionClass]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get(self, request):
        """
        This function will sent a get request and fetch the current user
        """
        user = request.user
        data = UserApplicationRoleSerializer(user).data
        return Response(data)



"""
   Api view for Saving Client Feature Permissions For User
"""
class SaveClientFeaturePermissionsForUserAPIView(APIView):
    permission_classes = [ClientFeaturePermissionClass]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def post(self, request):
        client = request.data.get('client')
        permissions = request.data.get('permission')
        user = request.data.get('user')
        user_object = User.objects.get(id=user)
        final_client_permissions = []
        old_permissions = list(user_object.client_feature_permission.all())
        for feature_code in permissions:
            client_permissions = ClientFeaturePermission.objects.filter(feature__feature_code=feature_code,
                                                                        client=client)
            for permission in permissions[feature_code]:
                if not permissions[feature_code][permission]:
                    client_permissions = client_permissions.exclude(permission=permission)
            final_client_permissions.extend(client_permissions)
        user_object.client_feature_permission.remove(*user_object.client_feature_permission.filter(client=client))
        user_object.client_feature_permission.add(*final_client_permissions)
        new_permissions = list(user_object.client_feature_permission.all())
        UserActivityLog.objects.create(
            object_id=user,
            user=user_object,
            action='user_client_permissions_changed',
            new_value=ClientFeaturePermissionMicroSerializer(new_permissions, many=True).data,
            old_value=ClientFeaturePermissionMicroSerializer(old_permissions, many=True).data,
        )
        return Response(UserDetailedSerializer(user_object).data, status=200)

"""
   Api view for Saving Project Feature Permissions For User
"""
class SaveProjectFeaturePermissionsForUserAPIView(APIView):
    permission_classes = [ClientFeaturePermissionClass]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def post(self, request):
        project = request.data.get('project')
        permissions = request.data.get('permission')
        user = request.data.get('user')
        user_object = User.objects.get(id=user)
        final_project_permissions = []
        old_permissions = list(user_object.project_feature_permission.all())

        for feature_code in permissions:
            project_permissions = ProjectFeaturePermission.objects.filter(feature__feature_code=feature_code,
                                                                          project=project)
            for permission in permissions[feature_code]:
                if not permissions[feature_code][permission]:
                    project_permissions = project_permissions.exclude(permission=permission)
            final_project_permissions.extend(project_permissions)
        user_object.project_feature_permission.remove(*user_object.project_feature_permission.filter(project=project))
        user_object.project_feature_permission.add(*final_project_permissions)
        new_permissions = list(user_object.project_feature_permission.all())

        UserActivityLog.objects.create(
            object_id=user,
            user=user_object,
            action='user_project_permissions_changed',
            new_value=ProjectFeaturePermissionMicroSerializer(new_permissions, many=True).data,
            old_value=ProjectFeaturePermissionMicroSerializer(old_permissions, many=True).data,
        )
        return Response(UserDetailedSerializer(user_object).data, status=200)
