# Generated by Django 2.0.5 on 2020-01-13 08:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('risk_management', '0007_auto_20191227_1556'),
    ]

    operations = [
        migrations.AlterField(
            model_name='riskentry',
            name='comments',
            field=models.TextField(blank=True),
        ),
    ]
