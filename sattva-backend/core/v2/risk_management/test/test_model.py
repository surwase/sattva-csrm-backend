from django.test import TestCase
from .test_setup import TestSetUp
from rest_framework.fields import FileField
from v2.risk_management.models import RiskCategory, Risk,RiskEntry,RiskTemplate,RiskVariables,RiskVariablesClientSettings
from v2.project.models import Project,Milestone
from v2.plant.models import Plant
from unittest.mock import patch

class TestRiskCategory(TestSetUp):

    def setUp(self):
        super().setUp()
        RiskCategory.objects.create(
        name = "risk1"
        )

    def test_values(self):
        risk1=RiskCategory.objects.get(name = "risk1")
        self.assertEqual(risk1.name , "risk1")

    def test__str__(self):
        risk1=RiskCategory.objects.get(name = "risk1")
        self.assertEqual(risk1.__str__() , "risk1")

class TestRisk(TestSetUp):

    def setUp(self):
        super().setUp()
        RiskCategory.objects.create(
        name = "riskcat1"
        )

        rc=RiskCategory.objects.get(name = "riskcat1" )
        Risk.objects.create(
            severity =  0,
            name = "risk1",
            trigger_name = "r1",
            category = rc,
            description = "hi i am risk"
        )

    def test_values(self):
        risk1=Risk.objects.get(name = "risk1")
        self.assertEqual(risk1.name , "risk1")
        self.assertEqual(risk1.trigger_name , "r1")
        self.assertEqual(risk1.category.name , "riskcat1")
        self.assertEqual(risk1.description , "hi i am risk")

    def test__str__(self):
        risk1=Risk.objects.get(name = "risk1")
        self.assertEqual(risk1.__str__() , "risk1")


class TestRiskEntry(TestSetUp):

    @patch('v2.project.tasks.calculate_project_status.apply_async')
    def setUp(self,mock_send):
        super().setUp()
        Plant.objects.create(
                plant_name = "plant1",
                plant_description = "plantdec",
                client = self.client_obj
            )

        plant1=Plant.objects.get( plant_name = "plant1")
        Project.objects.create(
            project_name = "xyz",
            project_description = "hi i am tested",
            client = self.client_obj,
            plant = plant1,
            budget = 1000,
            project_status = 72.22,
            total_disbursement_amount = 8804465,
            planned_disbursement_amount = 8804465,
            total_utilised_amount = 7000000
            )

        project1=Project.objects.get(project_name = "xyz")
        Milestone.objects.create(
                milestone_name = "milestone1",
                milestone_description = "hi i am milestone",
                status = "complete",
                project = project1,
                position = 10,
                sub_focus_area = ['malnutrition'],
                sub_target_segment = ['children'],
                location =['lucknow'],
                tags = ['tag1','tag2'],
                )

        RiskCategory.objects.create(
        name = "riskcat1"
        )

        rc=RiskCategory.objects.get(name = "riskcat1" )
        Risk.objects.create(
            severity = 0,
            name = "risk1",
            trigger_name = "r1",
            category = rc,
            description = "hi i am risk"
        )

        milestone=Milestone.objects.get(milestone_name =  "milestone1")
        risk1=Risk.objects.get(name = "risk1")
        RiskEntry.objects.create(
            name = "riskent1",
            project = project1,
            client = self.client_obj,
            plant = plant1,
            milestone = milestone,
            risk = risk1,
            status = 0,
            severity = 1,
            risk_type = 0,
            category = rc,
            content = "hhhhh",
            comments = "jjjj"
        )

    def test_validation(self):
        with self.assertRaises(ValueError):
            plant1=Plant.objects.get( plant_name = "plant1")
            project1=Project.objects.get(project_name = "xyz")
            rc=RiskCategory.objects.get(name = "riskcat1" )
            milestone=Milestone.objects.get(milestone_name =  "milestone1")
            risk1=Risk.objects.get(name = "risk1")
            RiskEntry.objects.create(
                name = 123,
                project = project1,
                client = self.client_obj,
                plant = plant1,
                milestone = milestone,
                risk = risk1,
                status = 0,
                severity = 1,
                risk_type = 0,
                category = "xxx",
            )

    def test_values(self):
        riskent1=RiskEntry.objects.get(name = "riskent1")
        self.assertEqual(riskent1.name , "riskent1")
        self.assertEqual(riskent1.status , 0)
        self.assertEqual(riskent1.severity , 1)
        self.assertEqual(riskent1.content , "hhhhh")
        self.assertEqual(riskent1.milestone.milestone_name , "milestone1")
        self.assertEqual(riskent1.risk_type , 0)
        self.assertEqual(riskent1.content , "hhhhh")

    def test__str__(self):
        riskent1=RiskEntry.objects.get(name = "riskent1")
        self.assertEqual(riskent1.__str__() , "riskent1")

    def test_is_opened(self):
        riskent1=RiskEntry.objects.get(name = "riskent1")
        self.assertEqual(riskent1.is_opened() , True )

    def test_is_closed(self):
        riskent1=RiskEntry.objects.get(name = "riskent1")
        self.assertEqual(riskent1.is_closed() , False )

    def test_get_project(self):
        riskent1=RiskEntry.objects.get(name = "riskent1")
        self.assertEqual(riskent1.get_project().id,2039 )

    def test_get_client(self):
        riskent1=RiskEntry.objects.get(name = "riskent1")
        self.assertEqual(riskent1.get_client().id,2040 )

    def tes_get_plant(self):
        riskent1=RiskEntry.objects.get(name = "riskent1")
        self.assertEqual(riskent1.get_plant().id, 1 )


class TestRiskTemplate(TestSetUp):

    def setUp(self):
        super().setUp()
        RiskCategory.objects.create(
        name = "riskcat1"
        )

        r1=RiskCategory.objects.get(name = "riskcat1" )
        Risk.objects.create(
            severity =  0,
            name = "risk1",
            trigger_name = "r1",
            category = r1,
            description = "hi i am risk"
        )

        risk1=Risk.objects.get(name = "risk1")
        RiskTemplate.objects.create(
            risk = risk1,
            content = "hi i am risk"
        )

    def test_validation(self):
        with self.assertRaises(ValueError):
            RiskTemplate.objects.create(
            risk = "abc",
            content = "hi i am risk"
        )

    def test_values(self):
        riskt1= RiskTemplate.objects.get(content = "hi i am risk")
        self.assertEqual(riskt1.risk.name , "risk1")
        self.assertEqual(riskt1.content , "hi i am risk")

    def test__str__(self):
        riskt1= RiskTemplate.objects.get(content = "hi i am risk")
        self.assertEqual(riskt1.__str__() , "risk1")


class TestRiskVariables(TestSetUp):

    def setUp(self):
        super().setUp()
        RiskCategory.objects.create(
        name = "riskcat1"
        )

        rc=RiskCategory.objects.get(name = "riskcat1" )
        Risk.objects.create(
            severity =  0,
            name = "risk1",
            trigger_name = "r1",
            category = rc,
            description = "hi i am risk"
        )

        risk1=Risk.objects.get(name = "risk1")
        RiskVariables.objects.create(
            risk = risk1,
            name = "temp1",
            display_name = "name",
            default_value = 9.0,
            variable_type = "var1"
        )

    def test_validation(self):
        with self.assertRaises(ValueError):
            risk1=Risk.objects.get(name = "risk1")
            RiskVariables.objects.create(
                risk = risk1,
                name = 12,
                display_name = "name",
                default_value = "hi",
                variable_type = "var1"
            )

    def test_values(self):
        riskt1= RiskVariables.objects.get(name = "temp1")
        self.assertEqual(riskt1.risk.name , "risk1")
        self.assertEqual(riskt1.name , "temp1")
        self.assertEqual(riskt1.display_name , "name")
        self.assertEqual(riskt1.default_value , 9.0)
        self.assertEqual(riskt1.variable_type , "var1")

    def test__str__(self):
        riskt1= RiskVariables.objects.get(name = "temp1")
        self.assertEqual(riskt1.__str__(), "risk1")


class TestRiskVariablesClientSettings(TestSetUp):

    def setUp(self):
        super().setUp()
        RiskCategory.objects.create(
        name = "riskcat1"
        )

        rc=RiskCategory.objects.get(name = "riskcat1" )
        Risk.objects.create(
            severity =  0,
            name = "risk1",
            trigger_name = "r1",
            category = rc,
            description = "hi i am risk"
        )

        risk1=Risk.objects.get(name = "risk1")
        RiskVariables.objects.create(
            risk = risk1,
            name = "temp1",
            display_name="name",
            default_value = 9.0,
            variable_type = "var1"
        )

        riskv1=RiskVariables.objects.get(risk = risk1)
        RiskVariablesClientSettings.objects.create(
            risk_variable = riskv1,
            client = self.client_obj,
            new_value = "zxc"
        )

    def test_validation(self):
        with self.assertRaises(ValueError):
            risk1=Risk.objects.get(name = "risk1")
            riskv1=RiskVariables.objects.get(risk = risk1)
            RiskVariablesClientSettings.objects.create(
                risk_variable = riskv1,
                client = "abb",
                new_value = "zxc"
            )

    def test_values(self):
        rvcs= RiskVariablesClientSettings.objects.get(new_value = "zxc")
        self.assertEqual(rvcs.risk_variable.risk.name , "risk1")

    def test__str__(self):
        rvcs= RiskVariablesClientSettings.objects.get(new_value = "zxc")
        self.assertEqual(rvcs.__str__() , "risk1")
