import json

from rest_framework import serializers

from v2.client.serializers import ClientMicroSerializer
from v2.plant.serializers import PlantMicroSerializer
from v2.project.serializers import ProjectMicroSerializer
from v2.risk_management.models import RiskEntry, RiskCategory


"""
    This class will serialize RiskEntry model.
"""
class RiskEntrySerializer(serializers.ModelSerializer):
    severity_text = serializers.SerializerMethodField()
    risk_name = serializers.SerializerMethodField()
    risk_type_text = serializers.SerializerMethodField()
    risk_category = serializers.SerializerMethodField()
    status_text = serializers.SerializerMethodField()
    project = serializers.SerializerMethodField()
    client = serializers.SerializerMethodField()
    plant = serializers.SerializerMethodField()
    milestone = serializers.SerializerMethodField()

    def get_severity_text(self, obj):
        return obj.get_severity_display()

    def get_risk_type_text(self, obj):
        return obj.get_risk_type_display()

    def get_risk_name(self, obj):
        return obj.risk.name if not obj.risk is None else ''

    def get_risk_category(self, obj):
        return obj.category.name if not obj.category is None else ''

    def get_status_text(self, obj):
        return obj.get_status_display()

    def get_status_text(self, obj):
        return obj.get_status_display()

    def get_project(self, obj):
        return ProjectMicroSerializer(obj.get_project()).data if obj.get_project() else []

    def get_client(self, obj):
        return ClientMicroSerializer(obj.get_client()).data if obj.get_client() else []

    def get_plant(self, obj):
        return PlantMicroSerializer(obj.get_plant()).data if obj.get_plant() else []

    def get_milestone(self, obj):
        return {'milestone_name': obj.milestone.get_name(),
                'id': obj.milestone.id} if obj.milestone else None

    class Meta:
        model = RiskEntry
        fields = ['id', 'code', 'status', 'status_text', 'severity' ,'severity_text', 'risk_type', 'risk_type_text',
                  'risk_name','risk_category', 'content', "comments", "created_date", "updated_date",
                  "client", "project", "plant","milestone" ]


"""
    This class will serialize RiskEntry model for write operations.
"""
class RiskEntryWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = RiskEntry
        fields = '__all__'


"""
    This class will serialize RiskCategory model.
"""
class RiskCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = RiskCategory
        fields = ['id', 'name']
