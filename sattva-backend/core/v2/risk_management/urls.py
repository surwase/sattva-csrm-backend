from django.conf.urls import url

from v2.risk_management.views import RiskViewSet
from .views import *

app_name = "risk_management"
urlpatterns = [
    url(r'^$', RiskViewSet.as_view(
        {'get': 'list', 'post': 'create'}),
        name='project_risk_management'),
    url(r'^(?P<pk>\d+)/$', RiskViewSet.as_view(
        {'get': 'retrieve', 'put': 'update', 'patch': 'partial_update', 'delete': 'destroy'}),
        name='project_risk_management'),
    url(r'^category/', RiskCategoryViewSet.as_view(
        {'get': 'list'}),
        name='risk_management_category'),
    url(r'^project_risk/download/$', ProjectRiskView.as_view(),
        name='project_risk'),
    url(r'^client_risk/download/$', ClientRiskView.as_view(),
        name='client_risk'),
    url(r'^program_risk/download/$', ProgramRiskView.as_view(),
            name='program_risk'),
]
