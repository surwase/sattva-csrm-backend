from django.urls import reverse
from .test_setup import TestSetUpCsrUser
from rest_framework import status
from v2.governance.models import Governance
from v2.governance.views import GovernanceModelViewSet

class TestGovernanceModelViewSet(TestSetUpCsrUser):

    def setUp(self):
        super().setUp()
        self.gov=Governance.objects.create(
            name = "governance1",
            location = ['lucknow'],
            description = "hi i am a governance",
            client = self.client_obj,
            governance_type =0,
            status = 0,
            comments = "comment1",
            is_milestone_selected = True,
            is_financial_selected = True,
            is_impact_selected = False,
            is_risk_selected = False,
            )
        self.data={
            "name" : "governance2",
            "location" : "['lucknow']",
            "description" : "hi i am a governance",
            "client" : self.client_obj.id,
            "governance_type" :0,
            "status" : 0,
            "comments" : "comment1",
            "is_milestone_selected" : True,
            "is_financial_selected" : True,
            "is_impact_selected" : False,
            "is_risk_selected ": False,
        }
        #urls
        self.governance_url= reverse('v2:governance:governance')

    def test_governance_url_list(self):
        request = self.factory.get(self.governance_url)
        request.user = self.user
        view =  GovernanceModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_governance_create(self):
        request = self.factory.post(self.governance_url, self.data, format='json')
        request.user = self.user
        view =  GovernanceModelViewSet.as_view({'get': 'list', 'post': 'create'})
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['name'], self.data['name'])


