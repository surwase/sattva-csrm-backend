import django_filters
from django_filters import rest_framework as filters
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.views import APIView
from v2.common_functions import create_xlsx_file
from v2.authentication import KeycloakUserUpdateAuthentication
from v2.client.models import ClientUser
from v2.client.permissions import GovernancePermission
from v2.governance.models import Governance
from v2.governance.serializers import GovernanceSerializer, GovernanceWriteSerializer
from v2.plant.models import Plant, PlantDocument, PlantUser
from v2.plant.serializers import PlantSerializer, PlantDocumentSerializer, PlantUserSerializer
from v2.user_management.permissions import ClientFeaturePermission, ProjectFeaturePermission


"""
    Filter for Governance model where we filter client by exact id
"""
class GovernanceFilter(django_filters.FilterSet):
    class Meta:
        model = Governance
        fields = {
            'client': ['exact']
        }

"""
    Viewset for Governance model, allows CRUD operations.
"""
class GovernanceModelViewSet(viewsets.ModelViewSet):
    serializer_class = GovernanceSerializer
    queryset = Governance.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = GovernanceFilter
    permission_classes = [GovernancePermission]
    authentication_classes = [KeycloakUserUpdateAuthentication]

    def get_serializer_class(self):
        """
        This function is used to get serializer class.It will return GovernanceWriteSerializer if method is put or post or patch else it will return GovernanceSerializer
        """
        method = self.request.method
        if method == 'PUT' or method == 'POST' or method == 'PATCH':
            return GovernanceWriteSerializer
        else:
            return GovernanceSerializer

    def create(self, request, *args, **kwargs):
        """
        This function will create the governance and return the response(created)
        """
        data = request.data
        if data.get('assignee'):
            assignee = data.get('assignee')
            client = data.get('client')
            client_users = ClientUser.objects.filter(user__in=assignee, client=client).values_list('id', flat=True)
            data['assignee'] = client_users
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        """
        This function will update the governance
        """
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        data = request.data
        if data.get('assignee'):
            assignee = data.get('assignee')
            client = data.get('client')
            client_users = ClientUser.objects.filter(user__in=assignee, client=client).values_list('id', flat=True)
            data['assignee'] = client_users
        serializer = self.get_serializer(instance, data=data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class GovernanceExcelView(APIView):
    @staticmethod
    def get(request):
        try:
            client_id = request.GET.get('client')
            governance = Governance.objects.filter(client=client_id).values('name', 'description', 'governance_type',
                                                                            'governance_date__date', 'projects__project_name',
                                                                            'start_date__date', 'end_date__date',
                                                                            'location', 'status',
                                                                            'comments').order_by('updated_date',
                                                                                                 'created_date')

            for num, gove in enumerate(governance):
                if gove.get('location').get('area') or gove.get('location').get('city'):
                    loc_list = '; '.join([gove.get('location') .get('area') if gove.get('location').get('area') else gove.get('location').get('city')])
                    governance[num]['location'] = loc_list if loc_list else None
                else:
                    governance[num]['location'] = None
            governance_header = ['Identifier', 'Description', 'Governance Type', 'Governance Date', 'Project',
                                 'Start Date', 'End Date', 'location', 'Status', 'Comments']

            return create_xlsx_file(titles=['Governance'], headers=[governance_header],
                                    objects=[governance], comments=None, members=None)
        except Exception as err:
            return Response({'error': 'Failed to Export file.' + str(err)}, status=status.HTTP_400_BAD_REQUEST)
