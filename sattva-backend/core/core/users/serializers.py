
from rest_framework import serializers

from core.users.models import User, UserActivityLog


class UserSerializer(serializers.ModelSerializer):
  class Meta:
    model = User
    fields = '__all__'


class UserActivityLogSerializer(serializers.ModelSerializer):
  class Meta:
    model = UserActivityLog
    fields = '__all__'