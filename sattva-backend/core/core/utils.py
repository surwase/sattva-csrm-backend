

def get_value_or_default(dict_, key, default=None):
    try:
        ret = dict_[key]
    except:
        ret = default
    return ret

def check_null_and_replace_to_none(str=None):
    if str is not None:
        if len(str) > 0 :
            return None if str.lower() == 'null' else str
        else:
            return None
    else:
        return None
