from django.db import models
from django.conf import settings
from geolibraries.models import AuditFields


def _check_permission_clashing(custom, builtin, ctype):
    pass

"""
   This model will be used to store the Focus Area.
"""
class FocusArea(AuditFields):
    focus_area_id = models.AutoField(primary_key=True)
    focus_area_name = models.CharField(max_length=40, unique=True)

    def __str__(self):
        return self.focus_area_name

    class Meta:
        db_table = 'focus_area'
        # permissions = (
        #     ('view_focusarea', 'Can view focus area'),
        # )

"""
   This model will be used to store the sub Focus Area.
"""
class SubFocusArea(AuditFields):
    sub_focus_area_id = models.AutoField(primary_key=True)
    focus_area_name = models.ForeignKey(FocusArea, to_field='focus_area_name', db_column='focus_area_name',
                                        on_delete=models.CASCADE)
    sub_focus_area_name = models.CharField(max_length=60, unique=True)

    def __str__(self):
        return self.sub_focus_area_name

    class Meta:
        db_table = 'sub_focus_area'
        # permissions = (
        #     ('view_subfocusarea', 'Can view sub focus area'),
        # )

"""
   This model will be used to store the Institution.
"""
class Institution(AuditFields):
    institution_id = models.AutoField(primary_key=True)
    institution_name = models.CharField(max_length=35, unique=True)

    class Meta:
        db_table = 'institution'
        # permissions = (
        #     ('view_institution', 'Can view institution'),
        # )

"""
   This model will be used to store the SDGGoal.
"""
class SDGGoal(AuditFields):
    sdg_goal_id = models.AutoField(primary_key=True)
    sdg_goal = models.CharField(max_length=50, unique=True)

    class Meta:
        db_table = 'sdg_goal'
        # permissions = (
        #     ('view_sdggoal', 'Can view sdg goal'),
        # )

"""
   This model will be used to store the TargetSegment.
"""
class TargetSegment(AuditFields):
    target_segment_id = models.AutoField(primary_key=True)
    target_segment = models.CharField(max_length=35, unique=True)

    def __str__(self):
        return self.target_segment

    class Meta:
        db_table = 'target_segment'
        # permissions = (
        #     ('view_targetsegment', 'Can view target segment'),
        # )

"""
   This model will be used to store the sub TargetSegment.
"""
class SubTargetSegment(AuditFields):
    sub_target_segment_id = models.AutoField(primary_key=True)
    target_segment = models.ForeignKey(TargetSegment, to_field="target_segment", db_column="target_segment",
                                       on_delete=models.CASCADE)
    sub_target_segment = models.CharField(max_length=35, unique=True)

    def __str__(self):
        return self.sub_target_segment

    class Meta:
        db_table = 'sub_target_segment'
        # permissions = (
        #     ('view_subtargetsegment', 'Can view sub target segment'),
        # )

"""
   This model will be used to store the Application Role.
"""
class ApplicationRole(AuditFields):
    role_choices = (
        ('sattva-admin', 'sattva-admin'),
        ('sattva-user', 'sattva-user'),
        ('csr-admin', 'csr-admin'),
        ('csr-user', 'csr-user'),
        ('ngo-user', 'ngo-user'),
        ('data-collector', 'data-collector'),
        ('csr-watcher', 'csr-watcher'),
    )
    # ('csr-approver', 'csr-approver'),
    # ('SuperAdmin', 'SuperAdmin'),
    # ('SattvaAdmin', 'SattvaAdmin'),
    # ('CSRAdmin', 'CSRAdmin'),
    # ('CSRLeadership', 'CSRLeadership'),
    # ('CSRSpoc', 'CSRSpoc'),
    # ('CSRSpocManager', 'CSRSpocManager'),
    # ('CSRVolunteer', 'CSRVolunteer'),
    # ('CSREmployee', 'CSREmployee'),
    # ('CSRProjectApprover', 'CSRProjectApprover'),
    # ('CSRComplianceApprover', 'CSRComplianceApprover'),
    # ('CSRContractApprover', 'CSRContractApprover'),
    # ('NGOUploader', 'NGOUploader'),
    # ('NGOViewer', 'NGOViewer'),
    # ('NGOAdmin', 'NGOAdmin')
    role_id = models.AutoField(primary_key=True)
    role_name = models.CharField(max_length=21, choices=role_choices, unique=True)

    class Meta:
        db_table = 'application_role'
        # permissions = (
        #     ('view_applicationrole', 'Can view application role'),
        # )

"""
   This model will be used to store the Data Type.
"""
class DataType(AuditFields):
    data_type_id = models.AutoField(primary_key=True)
    data_type = models.CharField(max_length=9, choices=settings.GLOBAL_CONSTANTS_FIELD_TYPE_CHOICES, unique=True)

    def __str__(self):
        return self.data_type

    class Meta:
        db_table = 'data_type'
        # permissions = (
        #     ('view_datatype', 'Can view data type'),
        # )

"""
   This model will be used to store the Frequency.
"""
class Frequency(AuditFields):
    frequency_choices = (
        ('daily', 'Daily'),
        ('weekly', 'Weekly'),
        ('fortnightly', 'Fortnightly'),
        ('monthly', 'Monthly'),
        ('bi-monthly', 'Bi-Monthly'),
        ('quarterly', 'Quarterly'),
        ('semiannually', 'SemiAnnually'),
        ('annually', 'Annually'),
    )
    frequency_id = models.AutoField(primary_key=True)
    frequency = models.CharField(max_length=12, choices=frequency_choices, unique=True)

    class Meta:
        db_table = 'frequency'
        # permissions = (
        #     ('view_frequency', 'Can view frequency'),
        # )

"""
   This model will be used to store the EntityType.
"""
class EntityType(AuditFields):
    entity_type_choices = (
        ('Beneficiary', 'Beneficiary'),
        ('Institution', 'Institution'),
        ('Environment', 'Environment'),
    )
    entity_type_id = models.AutoField(primary_key=True)
    entity_type = models.CharField(max_length=11, choices=entity_type_choices, unique=True)

    def __str__(self):
        return self.entity_type

    class Meta:
        db_table = 'entity_type'
        # permissions = (
        #     ('view_entitytype', 'Can view entity type'),
        # )

"""
   This model will be used to store the Entity.
"""
class Entity(AuditFields):
    entity_requirement_choices = (
        ('Mandatory', 'Mandatory'),
        ('Optional', 'Optional'),
    )
    id = models.AutoField(primary_key=True)
    entity_name = models.CharField(max_length=35)
    entity_type = models.ForeignKey(EntityType,
                                    to_field='entity_type', db_column='entity_type', on_delete=models.CASCADE)
    entity_focus_area = models.ForeignKey(FocusArea, to_field='focus_area_name', db_column='focus_area_name',
                                          on_delete=models.CASCADE)
    entity_sub_focus_area = models.ForeignKey(SubFocusArea, to_field='sub_focus_area_name',
                                              db_column='sub_focus_area_name', on_delete=models.CASCADE)
    attribute_name = models.CharField(max_length=150)
    attribute_type = models.ForeignKey(DataType, to_field='data_type', db_column='attribute_type',
                                       on_delete=models.CASCADE)
    attribute_length = models.SmallIntegerField(null=True)
    attribute_order = models.SmallIntegerField(null=False)
    attribute_requirement = models.CharField(max_length=9, choices=entity_requirement_choices,
                                             default='varchar', null=True)
    attribute_grouping = models.CharField(max_length=60)
    attribute_sub_grouping = models.CharField(max_length=60, null=True)
    attribute_value_range = models.CharField(max_length=500, null=True)
    attribute_default_value = models.CharField(max_length=40, null=True)
    attribute_collection_method = models.CharField(max_length=25, null=True)
    attribute_source = models.CharField(max_length=50, null=True)
    attribute_source_link = models.CharField(max_length=100, null=True)
    attribute_lead_indicator_flag = models.BooleanField(default=False)
    attribute_classification_indicator_flag = models.BooleanField(default=False)

    def __str__(self):
        return self.attribute_name

    class Meta:
        db_table = 'entity'
        # permissions = (
        #     ('view_entity', 'Can view entity'),
        # )
        unique_together = (
            ('entity_name', 'entity_type', 'attribute_name'),
            ('entity_name', 'entity_type', 'attribute_order'),
        )

"""
   This model will be used to store the Aggregate Function.
"""
class AggregateFunction(AuditFields):
    agg_func_id = models.AutoField(primary_key=True)
    agg_func = models.CharField(max_length=20, unique=True)

    class Meta:
        db_table = 'aggregate_function'
        # permissions = (
        #     ('view_aggregatefunction', 'Can view aggregate function'),
        # )

"""
   This model will be used to store the DocumentType.
"""
class DocumentType(AuditFields):
    document_type_id = models.AutoField(primary_key=True)
    document_type = models.CharField(max_length=35, unique=True)

    def __str__(self):
        return self.document_type

    class Meta:
        db_table = 'document_type'
        # permissions = (
        #     ('view_documenttype', 'Can view document type'),
        # )

"""
   This model will be used to store the Document class.
"""
class DocumentClass(AuditFields):
    document_list_id = models.AutoField(primary_key=True)
    document_source = models.CharField(max_length=6, choices=settings.DOCUMENT_SOURCE_CHOICES, null=True)
    document_type = models.ForeignKey(DocumentType, to_field='document_type', db_column='document_type',
                                      on_delete=models.CASCADE)
    document_status = models.CharField(max_length=7, choices=settings.DOCUMENT_STATUS_CHOICES, default='valid')
    document_requirement = models.CharField(max_length=9,
                                            choices=settings.DOCUMENT_REQUIREMENT_CHOICES, default='optional')
    document_class_name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.document_class_name

    class Meta:
        db_table = 'document_class'
        # permissions = (
        #     ('view_documentclass', 'Can view document class'),
        # )
        unique_together = ('document_source', 'document_type')

"""
   This model will be used to store the CSR Compliance CheckList.
"""
class CSRComplianceCheckList(AuditFields):
    area_choices = (
        ('revenue', 'Revenue'),
        ('board', 'Board'),
        ('policy', 'Policy'),
        ('spend', 'Spend'),
        ('programs', 'Programs'),
        ('reporting', 'Reporting'),
    )
    csr_compliance_checklist_id = models.AutoField(primary_key=True)
    area = models.CharField(max_length=10, choices=area_choices)
    indicator = models.CharField(max_length=250, unique=True)
    document_required = models.BooleanField(default=False)

    def __str__(self):
        return self.indicator

    class Meta:
        db_table = 'csr_compliance_checklist'
        # permissions = (
        #     ('view_csrcompliancechecklist', 'Can view csr compliance check list'),
        # )

"""
   This model will be used to store the NGO Selection CheckList.
"""
class NGOSelectionCheckList(AuditFields):
    head_choices = (
        ('Legal Document Checklist', 'Legal Document Checklist'),
        ('Key considerations for selection of NGO', 'Key considerations for selection of NGO'),
        ('About the NGO', 'About the NGO'),
    )
    ngo_selection_checklist_id = models.AutoField(primary_key=True)
    head = models.CharField(max_length=40, choices=head_choices)
    detail = models.CharField(max_length=100, unique=True)
    notes = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.detail

    class Meta:
        db_table = 'ngo_selection_checklist'
        # permissions = (
        #     ('view_ngoselectionchecklist', 'Can view ngo selection check list'),
        # )

"""
   This model will be used to store the Risk Impact.
"""
class RiskImpact(AuditFields):
    risk_impact_id = models.AutoField(primary_key=True)
    risk_impact = models.CharField(max_length=6, unique=True)

    def __str__(self):
        return self.risk_impact

    class Meta:
        db_table = 'risk_impact'
        # permissions = (
        #     ('view_riskimpact', 'Can view risk impact'),
        # )

"""
   This model will be used to store the Risk Probability.
"""
class RiskProbability(AuditFields):
    risk_probability_id = models.AutoField(primary_key=True)
    risk_probability = models.CharField(max_length=6, unique=True)

    def __str__(self):
        return self.risk_probability

    class Meta:
        db_table = 'risk_probability'
        # permissions = (
        #     ('view_riskprobability', 'Can view risk probability'),
        # )

"""
   This model will be used to store the Risk status.
"""
class RiskStatus(AuditFields):
    risk_status_id = models.AutoField(primary_key=True)
    risk_status = models.CharField(max_length=6, unique=True)

    def __str__(self):
        return self.risk_status

    class Meta:
        db_table = 'risk_status'
        # permissions = (
        #     ('view_riskstatus', 'Can view risk status'),
        # )

"""
   This model will be used to store the Risk type.
"""
class RiskType(AuditFields):
    risk_type_id = models.AutoField(primary_key=True)
    risk_type = models.CharField(max_length=12, unique=True)

    def __str__(self):
        return self.risk_type

    class Meta:
        db_table = 'risk_type'
        # permissions = (
        #     ('view_risktype', 'Can view risk type'),
        # )

"""
   This model will be used to store the Risk owner.
"""
class RiskOwner(AuditFields):
    risk_owner_id = models.AutoField(primary_key=True)
    risk_owner = models.CharField(max_length=6, unique=True)

    def __str__(self):
        return self.risk_owner

    class Meta:
        db_table = 'risk_owner'
        # permissions = (
        #     ('view_riskowner', 'Can view risk owner'),
        # )

"""
   This model will be used to store the Mitigation Status.
"""
class MitigationStatus(AuditFields):
    mitigation_status_id = models.AutoField(primary_key=True)
    mitigation_status = models.CharField(max_length=30, unique=True)

    def __str__(self):
        return self.mitigation_status

    class Meta:
        db_table = 'mitigation_status'
        # permissions = (
        #     ('view_mitigationstatus', 'Can view mitigation status'),
        # )

"""
   This model will be used to store the Task Category.
"""
class TaskCategory(AuditFields):
    task_category_id = models.AutoField(primary_key=True)
    task_category = models.CharField(max_length=12, unique=True)

    class Meta:
        db_table = 'task_category'
        # permissions = (
        #     ('view_taskcategory', 'Can view task category'),
        # )

"""
   This model will be used to store the ScheduleVII Activity.
"""
class ScheduleVIIActivity(AuditFields):
    activity_description = models.TextField()

    def __str__(self):
        return self.activity_description


"""
   This model will be used to store the MCA Sector.
"""
class MCASector(AuditFields):
    id = models.AutoField(primary_key=True)
    mca_id = models.IntegerField()
    mca_focus_area_name = models.CharField(max_length=300)
    mca_sub_focus_area_name = models.CharField(max_length=300, unique=True)
    status = models.IntegerField()

    def __str__(self):
        return self.mca_focus_area_name

    class Meta:
        db_table = 'mca_sector'
