from django.contrib import admin

# Register your models here.
from domainlibraries.models import ApplicationRole

admin.site.register(ApplicationRole)
