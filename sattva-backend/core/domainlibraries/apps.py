from django.apps import AppConfig


class DomainlibrariesConfig(AppConfig):
    name = 'domainlibraries'
